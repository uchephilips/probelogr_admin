<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model {

    public $username;
    public $password;
    public $rememberMe = true;
    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
// username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params) {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    public function loginContinue($loginToken, $loginUser) {
        $au = $loginUser;

        $this->username = $au->email;

        $usr = $this->getUser();
        if (isset($usr)) {
            if ($usr !== false) {
//return Yii::$app->user->login($usr, $this->rememberMe ? 3600 * 24 * 30 : 0);
                return Yii::$app->user->login($usr, 1) ? "set" : "not set";
//return Yii::$app->user->login($usr, 1);
            } else {
                Yii::$app->session->setFlash('error', "Unknown user");
            }
        } else {
            Yii::$app->session->setFlash('error', "Unknown credential");
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login() {
//$resp = \app\assets\BoldServiceAsset::login($this->username, $this->password);
//$respobj = \app\assets\BoldServiceAsset::getResponseObj($resp);


        
        $au = AppUsers::find()->where('email = :email', ['email' => $this->username])->one();

        if (isset($au)) {
            $encPss = md5($this->password);

            if ($au->password == $encPss) {

                $usr = $this->getUser();

                if (isset($usr)) {
                    if ($usr !== false) {
//return Yii::$app->user->login($usr, $this->rememberMe ? 3600 * 24 * 30 : 0);
                        return Yii::$app->user->login($usr, 3600 * 24 * 30) ? "set" : "not set";
//return Yii::$app->user->login($usr, 1);
                    } else {
                        Yii::$app->session->setFlash('error', "Unknown user");
                    }
                } else {
                    Yii::$app->session->setFlash('error', "Unknown credential");
                }
            } else {
                Yii::$app->session->setFlash('error', "Unable to signing");
            }
        } else {
            Yii::$app->session->setFlash('error', "Unable to access service");
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     * 
     * B > A
     * C > 
     */
//    public function getUser() {
//        if ($this->_user === false) {
//            $resp = \app\assets\BoldServiceAsset::login($this->username, $this->password);
//            $resp = \app\assets\BoldServiceAsset::getResponseObj($resp);
//
//            if (isset($resp)) {
//                \app\assets\SessionAssets::setUserSession($resp);
//                $usr = new User();
//                $usr->id = $resp->id;
//                $usr->accessToken = $resp->accessToken;
//                $usr->firstname = $resp->firstName;
//                $usr->middlename = $resp->middleName;
//                $usr->lastname = $resp->lastName;
//                $usr->phone_number = $resp->phoneNumber;
//                $usr->email = $resp->email;
//                $usr->username = $resp->email;
//                $this->_user = $usr;
//                
//
//            }
//        }
//
//        return $this->_user;
//    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser() {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }

}
