<?php

namespace app\models\plt\query;

/**
 * This is the ActiveQuery class for [[\app\models\plt\Avgpageloadtimeym]].
 *
 * @see \app\models\plt\Avgpageloadtimeym
 */
class AvgpageloadtimeymQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return \app\models\plt\Avgpageloadtimeym[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\plt\Avgpageloadtimeym|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public static function load($avgPtlYm) {

        if (isset($avgPtlYm)) {
            foreach ($avgPtlYm as $value) {

                $pltYm = \app\models\plt\Avgpageloadtimeym::findOne(
                                [
                                    'userId' => $value->userid,
                                    'appId' => $value->appid,
                                    'tag' => $value->tags,
                                    'ym' => $value->ym,
                                    'currentUrl' => $value->currenturl,
                ]);

                if (!isset($pltYm)) {
                    $pltYm = new \app\models\plt\Avgpageloadtimeym();
                    $pltYm->userId = $value->userid;
                    $pltYm->appId = $value->appid;
                    $pltYm->tag = $value->tags;
                    $pltYm->currentUrl = $value->currenturl;
                    $pltYm->ym = $value->ym;
                }


                if ($pltYm->avgloadtime != $value->avgloadtime) {
                    $pltYm->avgloadtime = $value->avgloadtime;
                    if (!$pltYm->save()) {
//                        print_r($pltYm->getErrors());
//                        exit;
                    }
                }
            }
        }
    }

}
