<?php

namespace app\models\plt\query;

/**
 * This is the ActiveQuery class for [[\app\models\plt\Avgpageloadtimeymd]].
 *
 * @see \app\models\plt\Avgpageloadtimeymd
 */
class AvgpageloadtimeymdQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return \app\models\plt\Avgpageloadtimeymd[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\plt\Avgpageloadtimeymd|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public static function load($avgPLTYmd) {

        if (isset($avgPLTYmd)) {
            foreach ($avgPLTYmd as $value) {

                $aptlYmd = \app\models\plt\Avgpageloadtimeymd::findOne(
                                [
                                    'userId' => $value->userid,
                                    'appId' => $value->appid,
                                    'tag' => $value->tags,
                                    'ymd' => $value->ymd,
                                    'currentUrl' => $value->currenturl,
                ]);

                if (!isset($aptlYmd)) {
                    $aptlYmd = new \app\models\plt\Avgpageloadtimeymd();
                    $aptlYmd->userId = $value->userid;
                    $aptlYmd->appId = $value->appid;
                    $aptlYmd->tag = $value->tags;
                    $aptlYmd->currentUrl = $value->currenturl;
                    $aptlYmd->ymd = $value->ymd;
                }


                if ($aptlYmd->avgloadtime != $value->avgloadtime) {
                    $aptlYmd->avgloadtime = $value->avgloadtime;
                    if (!$aptlYmd->save()) {
//                        print_r($aptlYmd->getErrors());
//                        exit;
                    }
                }
            }
        }
    }

}
