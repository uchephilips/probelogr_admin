<?php

namespace app\models\plt;

use Yii;

/**
 * This is the model class for table "avgpageloadtimeym".
 *
 * @property int $id
 * @property int $userId
 * @property int $appId
 * @property string $tag
 * @property string $ym
 * @property string $currentUrl
 * @property int $avgloadtime
 */
class Avgpageloadtimeym extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'avgpageloadtimeym';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userId', 'appId', 'tag', 'ym', 'currentUrl'], 'required'],
            [['userId', 'appId', 'avgloadtime'], 'integer'],
            [['tag'], 'string', 'max' => 50],
            [['ym'], 'string', 'max' => 20],
            [['currentUrl'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'appId' => 'App ID',
            'tag' => 'Tag',
            'ym' => 'Ym',
            'currentUrl' => 'Current Url',
            'avgloadtime' => 'Avgloadtime',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\plt\query\AvgpageloadtimeymQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\plt\query\AvgpageloadtimeymQuery(get_called_class());
    }
}
