<?php

namespace app\models\plt;

use Yii;

/**
 * This is the model class for table "avgpageloadtimeymd".
 *
 * @property int $id
 * @property int $userId
 * @property int $appId
 * @property string $tag
 * @property string $ymd
 * @property string $currentUrl
 * @property int $avgloadtime
 */
class Avgpageloadtimeymd extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'avgpageloadtimeymd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['userId', 'appId', 'tag', 'ymd', 'currentUrl'], 'required'],
            [['userId', 'appId', 'avgloadtime'], 'integer'],
            [['ymd'], 'safe'],
            [['tag'], 'string', 'max' => 50],
            [['currentUrl'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'appId' => 'App ID',
            'tag' => 'Tag',
            'ymd' => 'Ymd',
            'currentUrl' => 'Current Url',
            'avgloadtime' => 'Avgloadtime',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\plt\query\AvgpageloadtimeymdQuery the active query used by this AR class.
     */
    public static function find() {
        return new \app\models\plt\query\AvgpageloadtimeymdQuery(get_called_class());
    }

}
