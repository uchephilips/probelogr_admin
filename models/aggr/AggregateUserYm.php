<?php

namespace app\models\aggr;

use Yii;

/**
 * This is the model class for table "aggregate_user_ym".
 *
 * @property int $id
 * @property int $userId
 * @property int $appId
 * @property string $tag
 * @property string $username
 * @property int $data_count
 * @property string $ym
 */
class AggregateUserYm extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'aggregate_user_ym';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['userId', 'appId', 'tag', 'username', 'ym'], 'required'],
            [['userId', 'appId', 'data_count'], 'integer'],
            [['tag'], 'string', 'max' => 50],
            [['username'], 'string', 'max' => 1000],
            [['ym'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'userId' => 'Userid',
            'appId' => 'App ID',
            'tag' => 'Tag',
            'username' => 'Username',
            'data_count' => 'Data Count',
            'ym' => 'Ym',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\aggr\query\AggregateUserYmQuery the active query used by this AR class.
     */
    public static function find() {
        return new \app\models\aggr\query\AggregateUserYmQuery(get_called_class());
    }

}
