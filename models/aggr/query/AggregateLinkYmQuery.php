<?php

namespace app\models\aggr\query;

/**
 * This is the ActiveQuery class for [[\app\models\aggr\AggregateLinkYm]].
 *
 * @see \app\models\aggr\AggregateLinkYm
 */
class AggregateLinkYmQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return \app\models\aggr\AggregateLinkYm[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\aggr\AggregateLinkYm|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public static function load($agrLinkYm) {

        if (isset($agrLinkYm)) {
            foreach ($agrLinkYm as $value) {

                $agLYm = \app\models\aggr\AggregateLinkYm::findOne(
                                [
                                    'userId' => $value->userid,
                                    'appId' => $value->appid,
                                    'tag' => $value->tags,
                                    'ym' => $value->ym,
                                    'currentUrl' => $value->currenturl,
                ]);

                if (!isset($agLYm)) {
                    $agLYm = new \app\models\aggr\AggregateLinkYm();
                    $agLYm->userId = $value->userid;
                    $agLYm->appId = $value->appid;
                    $agLYm->tag = $value->tags;
                    $agLYm->currentUrl = $value->currenturl;
                    $agLYm->ym = $value->ym;
                }


                if ($agLYm->data_count != $value->data_count) {
                    $agLYm->data_count = $value->data_count;
                    if (!$agLYm->save()) {
//                        print_r($agLYm->getErrors());
//                        exit;
                    }
                }
            }
        }
    }

}
