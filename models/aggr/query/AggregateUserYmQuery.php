<?php

namespace app\models\aggr\query;

/**
 * This is the ActiveQuery class for [[\app\models\aggr\AggregateUserYm]].
 *
 * @see \app\models\aggr\AggregateUserYm
 */
class AggregateUserYmQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return \app\models\aggr\AggregateUserYm[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\aggr\AggregateUserYm|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public static function load($agrUserYm) {

        if (isset($agrUserYm)) {
            foreach ($agrUserYm as $value) {

                $agUYm = \app\models\aggr\AggregateUserYm::findOne(
                                [
                                    'userId' => $value->userid,
                                    'appId' => $value->appid,
                                    'tag' => $value->tags,
                                    'ym' => $value->ym,
                                    'username' => $value->username,
                ]);

                if (!isset($agUYm)) {
                    $agUYm = new \app\models\aggr\AggregateUserYm();
                    $agUYm->userId = $value->userid;
                    $agUYm->appId = $value->appid;
                    $agUYm->tag = $value->tags;
                    $agUYm->username = $value->username;
                    $agUYm->ym = $value->ym;
                }

                if ($agUYm->data_count != $value->data_count) {
                    $agUYm->data_count = $value->data_count;
                    if (!$agUYm->save()) {
//                        print_r($agUYm->getErrors());
//                        exit;
                    }
                }
            }
        }
    }

}
