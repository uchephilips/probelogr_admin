<?php

namespace app\models\aggr\query;

/**
 * This is the ActiveQuery class for [[\app\models\aggr\AggregateUserYmdd]].
 *
 * @see \app\models\aggr\AggregateUserYmdd
 */
class AggregateUserYmdQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return \app\models\aggr\AggregateUserYmdd[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\aggr\AggregateUserYmdd|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public static function load($agrUserYmd) {

        if (isset($agrUserYmd)) {
            foreach ($agrUserYmd as $value) {

                $agUYmd = \app\models\aggr\AggregateUserYmd::findOne(
                                [
                                    'userId' => $value->userid,
                                    'appId' => $value->appid,
                                    'tag' => $value->tags,
                                    'ymd' => $value->ymd,
                                    'username' => $value->username,
                ]);

                if (!isset($agUYmd)) {
                    $agUYmd = new \app\models\aggr\AggregateUserYmd();
                    $agUYmd->userId = $value->userid;
                    $agUYmd->appId = $value->appid;
                    $agUYmd->tag = $value->tags;
                    $agUYmd->username = $value->username;
                    $agUYmd->ymd = $value->ymd;
                }

                if ($agUYmd->data_count != $value->data_count) {
                    $agUYmd->data_count = $value->data_count;
                    if (!$agUYmd->save()) {
//                        print_r($agUYmd->getErrors());
//                        exit;
                    }
                }
            }
        }
    }

}
