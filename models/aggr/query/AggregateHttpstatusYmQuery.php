<?php

namespace app\models\aggr\query;

/**
 * This is the ActiveQuery class for [[\app\models\aggr\AggregateHttpstatusYm]].
 *
 * @see \app\models\aggr\AggregateHttpstatusYm
 */
class AggregateHttpstatusYmQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return \app\models\aggr\AggregateHttpstatusYm[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\aggr\AggregateHttpstatusYm|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public static function load($agrHttpstatusYm) {
        if (isset($agrHttpstatusYm)) {
            foreach ($agrHttpstatusYm as $value) {

                $agHsYm = \app\models\aggr\AggregateHttpstatusYm::findOne(
                                [
                                    'userId' => $value->userid,
                                    'appId' => $value->appid,
                                    'tag' => $value->tags,
                                    'ym' => $value->ym,
                                    'statusCode' => $value->statuscode,
                ]);

                if (!isset($agHsYm)) {
                    $agHsYm = new \app\models\aggr\AggregateHttpstatusYm();
                    $agHsYm->userId = $value->userid;
                    $agHsYm->appId = $value->appid;
                    $agHsYm->tag = $value->tags;
                    $agHsYm->statusCode = $value->statuscode;
                    $agHsYm->statusText = $value->statustext;
                    $agHsYm->ym = $value->ym;
                }


                if ($agHsYm->data_count != $value->data_count) {
                    $agHsYm->data_count = $value->data_count;
                    if (!$agHsYm->save()) {
//                        print_r($agHsYm->getErrors());
//                        exit;
                    }
                }
            }
        }
    }

}
