<?php

namespace app\models\aggr\query;

/**
 * This is the ActiveQuery class for [[\app\models\aggr\AggregateHttpstatusYmd]].
 *
 * @see \app\models\aggr\AggregateHttpstatusYmd
 */
class AggregateHttpstatusYmdQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return \app\models\aggr\AggregateHttpstatusYmd[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\aggr\AggregateHttpstatusYmd|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public static function load($agrHttpstatusYmd) {

        if (isset($agrHttpstatusYmd))
            foreach ($agrHttpstatusYmd as $value) {

                $agHsYmd = \app\models\aggr\AggregateHttpstatusYmd::findOne(
                                [
                                    'userId' => $value->userid,
                                    'appId' => $value->appid,
                                    'tag' => $value->tags,
                                    'ymd' => $value->ymd,
                                    'statusCode' => $value->statuscode,
                ]);

                if (!isset($agHsYmd)) {
                    $agHsYmd = new \app\models\aggr\AggregateHttpstatusYmd();
                    $agHsYmd->userId = $value->userid;
                    $agHsYmd->appId = $value->appid;
                    $agHsYmd->tag = $value->tags;
                    $agHsYmd->statusCode = $value->statuscode;
                    $agHsYmd->statusText = $value->statustext;
                    $agHsYmd->ymd = $value->ymd;
                }

                if ($agHsYmd->data_count != $value->data_count) {
                    $agHsYmd->data_count = $value->data_count;
                    if (!$agHsYmd->save()) {
//                        print_r($agHsYmd->getErrors());
//                        exit;
                    }
                }
            }
    }

}
