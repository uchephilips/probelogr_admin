<?php

namespace app\models\aggr\query;

/**
 * This is the ActiveQuery class for [[\app\models\aggr\AggregateLinkYmdd]].
 *
 * @see \app\models\aggr\AggregateLinkYmdd
 */
class AggregateLinkYmdQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return \app\models\aggr\AggregateLinkYmdd[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\aggr\AggregateLinkYmdd|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public static function load($agrLinkYmd) {

        if (isset($agrLinkYmd)) {
            foreach ($agrLinkYmd as $value) {

                $agLYmd = \app\models\aggr\AggregateLinkYmd::findOne(
                                [
                                    'userId' => $value->userid,
                                    'appId' => $value->appid,
                                    'tag' => $value->tags,
                                    'ymd' => $value->ymd,
                                    'currentUrl' => $value->currenturl,
                ]);

                if (!isset($agLYmd)) {
                    $agLYmd = new \app\models\aggr\AggregateLinkYmd();
                    $agLYmd->userId = $value->userid;
                    $agLYmd->appId = $value->appid;
                    $agLYmd->tag = $value->tags;
                    $agLYmd->currentUrl = $value->currenturl;
                    $agLYmd->ymd = $value->ymd;
                }


                if ($agLYmd->data_count != $value->data_count) {
                    $agLYmd->data_count = $value->data_count;
                    if (!$agLYmd->save()) {
//                        print_r($agLYmd->getErrors());
//                        exit;
                    }
                }
            }
        }
    }

}
