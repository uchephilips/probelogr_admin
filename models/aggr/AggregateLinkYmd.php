<?php

namespace app\models\aggr;

use Yii;

/**
 * This is the model class for table "aggregate_link_ymd".
 *
 * @property int $id
 * @property int $userId
 * @property int $appId
 * @property string $tag
 * @property string $currentUrl
 * @property int $data_count
 * @property string $ymd
 */
class AggregateLinkYmd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aggregate_link_ymd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userId', 'appId', 'tag', 'currentUrl', 'ymd'], 'required'],
            [['userId', 'appId', 'data_count'], 'integer'],
            [['ymd'], 'safe'],
            [['tag'], 'string', 'max' => 50],
            [['currentUrl'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'userId',
            'appId' => 'App ID',
            'tag' => 'Tag',
            'currentUrl' => 'Current Url',
            'data_count' => 'Data Count',
            'ymd' => 'Ymd',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\aggr\query\AggregateLinkYmdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\aggr\query\AggregateLinkYmdQuery(get_called_class());
    }
}
