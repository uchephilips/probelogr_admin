<?php

namespace app\models\aggr;

use Yii;

/**
 * This is the model class for table "aggregate_link_ym".
 *
 * @property int $id
 * @property int $userId
 * @property int $appId
 * @property string $tag
 * @property string $currentUrl
 * @property int $data_count
 * @property string $ym
 */
class AggregateLinkYm extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'aggregate_link_ym';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['userId', 'appId', 'tag', 'currentUrl', 'ym'], 'required'],
            [['userId', 'appId', 'data_count'], 'integer'],
            [['tag'], 'string', 'max' => 50],
            [['currentUrl'], 'string', 'max' => 1000],
            [['ym'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'appId' => 'App ID',
            'tag' => 'Tag',
            'currentUrl' => 'Current Url',
            'data_count' => 'Data Count',
            'ym' => 'Ym',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\aggr\query\AggregateLinkYmQuery the active query used by this AR class.
     */
    public static function find() {
        return new \app\models\aggr\query\AggregateLinkYmQuery(get_called_class());
    }

}
