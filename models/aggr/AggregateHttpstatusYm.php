<?php

namespace app\models\aggr;

use Yii;

/**
 * This is the model class for table "aggregate_httpstatus_ym".
 *
 * @property int $id
 * @property int $userId
 * @property int $appId
 * @property string $tag
 * @property int $statusCode
 * @property string $statusText
 * @property int $data_count
 * @property string $ym
 */
class AggregateHttpstatusYm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aggregate_httpstatus_ym';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userId', 'appId', 'tag', 'statusCode', 'statusText', 'ym'], 'required'],
            [['userId', 'appId', 'statusCode', 'data_count'], 'integer'],
            [['tag', 'statusText'], 'string', 'max' => 50],
            [['ym'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'appId' => 'App ID',
            'tag' => 'Tag',
            'statusCode' => 'Status Code',
            'statusText' => 'Status Text',
            'data_count' => 'Data Count',
            'ym' => 'Ym',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\aggr\query\AggregateHttpstatusYmQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\aggr\query\AggregateHttpstatusYmQuery(get_called_class());
    }
}
