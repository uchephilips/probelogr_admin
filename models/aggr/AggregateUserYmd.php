<?php

namespace app\models\aggr;

use Yii;

/**
 * This is the model class for table "aggregate_user_ymd".
 *
 * @property int $id
 * @property int $userId
 * @property int $appId
 * @property string $tag
 * @property string $username
 * @property int $data_count
 * @property string $ymd
 */
class AggregateUserYmd extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'aggregate_user_ymd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['userId', 'appId', 'tag', 'username', 'ymd'], 'required'],
            [['userId', 'appId', 'data_count'], 'integer'],
            [['ymd'], 'safe'],
            [['tag'], 'string', 'max' => 50],
            [['username'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'userId' => 'Userid',
            'appId' => 'App ID',
            'tag' => 'Tag',
            'username' => 'Username',
            'data_count' => 'Data Count',
            'ymd' => 'Ymd',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\aggr\query\AggregateUserYmdQuery the active query used by this AR class.
     */
    public static function find() {
        return new \app\models\aggr\query\AggregateUserYmdQuery(get_called_class());
    }

}
