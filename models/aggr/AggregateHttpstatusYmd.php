<?php

namespace app\models\aggr;

use Yii;

/**
 * This is the model class for table "aggregate_httpstatus_ymd".
 *
 * @property int $id
 * @property int $userId
 * @property int $appId
 * @property string $tag
 * @property int $statusCode
 * @property string $statusText
 * @property int $data_count
 * @property string $ymd
 */
class AggregateHttpstatusYmd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aggregate_httpstatus_ymd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userId', 'appId', 'tag', 'statusCode', 'statusText', 'ymd'], 'required'],
            [['userId', 'appId', 'statusCode', 'data_count'], 'integer'],
            [['ymd'], 'safe'],
            [['tag', 'statusText'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'appId' => 'App ID',
            'tag' => 'Tag',
            'statusCode' => 'Status Code',
            'statusText' => 'Status Text',
            'data_count' => 'Data Count',
            'ymd' => 'Ymd',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\aggr\query\AggregateHttpstatusYmdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\aggr\query\AggregateHttpstatusYmdQuery(get_called_class());
    }
}
