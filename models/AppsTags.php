<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "apps_tags".
 *
 * @property int $id
 * @property string $tags
 * @property string $description
 * @property string $has_email_trigger
 * @property string $log_type
 * @property int $created_by
 * @property int $app_id
 * @property string $color_code
 * @property string $created_time
 * @property string $update_time
 * @property int $updated_by
 * @property bool $is_active
 * @property bool $is_suspended
 */
class AppsTags extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'apps_tags';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['tags', 'description', 'created_by', 'app_id', 'created_time', 'color_code','log_type','has_email_trigger'], 'required'],
            [['created_by', 'app_id', 'updated_by'], 'integer'],
            [['created_time', 'update_time'], 'safe'],
            [['is_active', 'is_suspended'], 'boolean'],
            [['tags'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'tags' => 'Tags',
            'description' => 'Description',
            'created_by' => 'Created By',
            'app_id' => 'App ID',
            'created_time' => 'Created Time',
            'update_time' => 'Update Time',
            'updated_by' => 'Updated By',
            'is_active' => 'Is Active',
            'is_suspended' => 'Is Suspended',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\AppsTagsQuery the active query used by this AR class.
     */
    public static function find() {
        return new \app\models\queries\AppsTagsQuery(get_called_class());
    }

    public static function getUniqueColor($userId) {
        $color = \app\assets\Misc::rand_color();
        $findOne = AppsTags::findOne(['created_by' => $userId, 'color_code' => $color]);
        if (isset($findOne)) {
            return $this->getUniqueColor($userId);
        }
        return $color;
    }

    public static function getTagsColors($tags, $userId) {
        $res = [];
        if (isset($tags)) {
            foreach ($tags as $tag) {
                $color = self::getUniqueColor($userId);
                $res = array_merge([$tag => $color], $res);
            }
        }
        return $res;
    }

}
