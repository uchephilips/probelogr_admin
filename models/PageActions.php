<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "page_actions".
 *
 * @property int $id
 * @property string $controller_id
 * @property string $controller_name
 * @property string $action_id
 * @property string $action_name
 * @property bool $is_user_defined
 * @property string $ts
 */
class PageActions extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'page_actions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['controller_id', 'controller_name', 'action_id', 'action_name'], 'required'],
            [['is_user_defined'], 'boolean'],
            [['ts'], 'safe'],
            [['controller_id', 'action_id', 'action_name'], 'string', 'max' => 20],
            [['controller_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'controller_id' => 'Controller ID',
            'controller_name' => 'Controller Name',
            'action_id' => 'Action ID',
            'action_name' => 'Action Name',
            'is_user_defined' => 'Is User Defined',
            'ts' => 'Ts',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\PageActionsQuery the active query used by this AR class.
     */
    public static function find() {
        return new \app\models\queries\PageActionsQuery(get_called_class());
    }

    /**
     * @return PageActions[] returns all page_actions with the controller_id
     */
    public function getActions() {
        return PageActions::findAll(['controller_id' => $this->controller_id]);
    }

    /**
     * 
     * @return PageActions[] returns all pages (controllers) grouped by controller_id
     */
    public static function getPages() {
        $controllers = \app\models\PageActions::findBySql("SELECT any_value(controller_name) controller_name,any_value(controller_id) controller_id FROM page_actions pa GROUP BY pa.controller_id")->all();
        return $controllers;
    }

    /**
     * 
     * @return boolean returns true if role has a page_action
     */
    public function hasAction($roleId) {
        $roleAction = \app\models\RoleActions::findAll(['role_id' => $roleId, 'page_action_id' => $this->id]);
        return count($roleAction) > 0;
    }

    public static function getFeatures() {
        return \Yii::$app->db->createCommand("SELECT pa.controller_id `key`,pa.controller_name `name` FROM page_actions pa GROUP BY pa.controller_id;")->queryAll();
    }

}
