<?php

namespace app\models;

class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface {

    public $id;
    public $appUserId;
    public $username;
    public $fullname;
    public $password;
    public $authKey;
    public $accessToken;
    public $firstname;
    public $lastname;
    public $middlename;
    public $phone_number;
    public $email;
    public $last_login;
    public $role_id;
    public $is_active;

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id) {
        // \app\assets\TenancyAsset::setDatabaseName("cuorma");
        $usr = AppUsers::find()->where('access_token = :access_token', ['access_token' => $id])->one();

        if (isset($usr)) {
            $u = User::getU($usr);
            return $u;
        } else {
            return null;
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        $users = AppUsers::find()->where('email = :email', ['email' => $username])->all();

        foreach ($users as $usr) {
            if ($user['accessToken'] === $token) {
                $u = User::getU($usr);
                return new static($u);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username) {
        $usr = AppUsers::find()->where('email = :email', ['email' => $username])->one();

        if (isset($usr)) {
            if (strcasecmp($usr->email, $username) === 0) {
                $u = User::getU($usr);
                return new static($u);
            }
        }


        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId() {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey() {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey) {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password) {
        return $this->password === $password;
    }

    /**
     * 
     * @param AppUsers $usr
     * @return \app\models\User
     */
    public static function getU($usr) {
        $u = new User();
        $u->id = $usr->access_token;
        $u->appUserId = $usr->id;
        $u->username = $usr->email;
        $u->fullname = $usr->first_name . ' ' . $usr->middle_name . ' ' . $usr->last_name;
        $u->phone_number = $usr->phone_number;
        $u->email = $usr->email;
        $u->password = $usr->password;
        $u->firstname = $usr->first_name;
        $u->middlename = $usr->middle_name;
        $u->lastname = $usr->last_name;
        $u->last_login = $usr->last_login;
        $u->accessToken = $usr->access_token;
        $u->role_id = $usr->role_id;
        $u->is_active = $usr->is_active;
        return $u;
    }

}
