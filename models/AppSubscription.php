<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "app_subscription".
 *
 * @property int $id
 * @property string $subscription_name
 * @property string $subscription_key
 * @property int $app_no
 * @property int $tag_no
 * @property int $user_no
 * @property int $storage_no
 * @property string $has_support
 * @property string $has_dedicated_env
 * @property int $created_by
 * @property string $created_time
 * @property string $update_time
 * @property int $updated_by
 * @property bool $is_active
 * @property bool $is_suspended
 */
class AppSubscription extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'app_subscription';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subscription_name', 'subscription_key', 'app_no', 'tag_no', 'user_no', 'storage_no', 'created_by', 'created_time'], 'required'],
            [['app_no', 'tag_no', 'user_no', 'storage_no', 'created_by', 'updated_by'], 'integer'],
            [['created_time', 'update_time'], 'safe'],
            [['is_active', 'is_suspended'], 'boolean'],
            [['subscription_name'], 'string', 'max' => 50],
            [['subscription_key'], 'string', 'max' => 20],
            [['has_support', 'has_dedicated_env'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subscription_name' => 'Subscription Name',
            'subscription_key' => 'Subscription Key',
            'app_no' => 'App No',
            'tag_no' => 'Tag No',
            'user_no' => 'User No',
            'storage_no' => 'Storage No',
            'has_support' => 'Has Support',
            'has_dedicated_env' => 'Has Dedicated Env',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'update_time' => 'Update Time',
            'updated_by' => 'Updated By',
            'is_active' => 'Is Active',
            'is_suspended' => 'Is Suspended',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\AppSubscriptionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\AppSubscriptionQuery(get_called_class());
    }
}
