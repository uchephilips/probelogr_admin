<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "team_members".
 *
 * @property int $id
 * @property int $team_id
 * @property string $email
 * @property int $user_id
 * @property string $role
 * @property string $invite_status PENDING,ACCEPTED,REJECTED
 * @property string $created_time
 * @property string $update_time
 * @property int $updated_by
 * @property int $created_by
 * @property bool $is_active
 * @property bool $is_suspended
 */
class TeamMembers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team_members';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['team_id', 'email', 'role', 'created_time', 'created_by'], 'required'],
            [['team_id', 'user_id', 'updated_by', 'created_by'], 'integer'],
            [['created_time', 'update_time'], 'safe'],
            [['is_active', 'is_suspended'], 'boolean'],
            [['email'], 'string', 'max' => 50],
            [['role', 'invite_status'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'team_id' => 'Team ID',
            'email' => 'Email',
            'user_id' => 'User ID',
            'role' => 'Role',
            'invite_status' => 'Invite Status',
            'created_time' => 'Created Time',
            'update_time' => 'Update Time',
            'updated_by' => 'Updated By',
            'created_by' => 'Created By',
            'is_active' => 'Is Active',
            'is_suspended' => 'Is Suspended',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\TeamMembersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\TeamMembersQuery(get_called_class());
    }
}
