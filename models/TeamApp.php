<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "team_app".
 *
 * @property int $id
 * @property int $app_id
 * @property string $tags
 * @property int $team_id
 * @property int $created_by
 * @property string $created_time
 * @property string $update_time
 * @property int $updated_by
 * @property bool $is_active
 * @property bool $is_suspended
 */
class TeamApp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team_app';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['app_id', 'team_id', 'created_by', 'created_time','tags'], 'required'],
            [['app_id', 'team_id', 'created_by', 'updated_by'], 'integer'],
            [['created_time', 'update_time'], 'safe'],
            [['is_active', 'is_suspended'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'app_id' => 'App ID',
            'team_id' => 'Team ID',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'update_time' => 'Update Time',
            'updated_by' => 'Updated By',
            'is_active' => 'Is Active',
            'is_suspended' => 'Is Suspended',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\TeamAppQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\TeamAppQuery(get_called_class());
    }
}
