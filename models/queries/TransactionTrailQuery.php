<?php

namespace app\models\queries;

/**
 * This is the ActiveQuery class for [[\app\models\TransactionTrail]].
 *
 * @see \app\models\TransactionTrail
 */
class TransactionTrailQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return \app\models\TransactionTrail[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\TransactionTrail|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    private function findStuff($transactionId = null, $transactionType = null, $userId = null, $year = null, $month = null) {

        $query = (new \yii\db\Query())->from(['transaction_trail', 'transaction']);
        $query->andWhere('transaction_trail.transaction_id = transaction.id');

        if (isset($transactionId))
            $query->andWhere(['transaction_trail.transaction_id' => $transactionId]);
        if (isset($transactionType))
            $query->andWhere(['transaction_trail.transaction_type' => $transactionType]);
        if (isset($userId))
            $query->andWhere(['transaction.created_by' => $userId]);
        if (isset($year))
            $query->andWhere(['year(transaction_trail.ts)' => $year]);
        if (isset($month))
            $query->andWhere(['month(transaction_trail.ts)' => $month]);
        return $query;
    }

    /**
     * 
     * @param type $transactionId
     * @param type $orderStatus
     * @param type $transactionMethod
     * @param type $userId
     * @return type
     */
    public function findSum($transactionId = null, $transactionType = null, $userId = null, $year = null, $month = null) {
        $query = $this->findStuff($transactionId, $transactionType, $userId, $year, $month);
        $sum = $query->sum('transaction_trail.amount');
        return $sum;
    }

    public function findCount($transactionId = null, $transactionType = null, $userId = null, $year = null, $month = null) {
        $query = $this->findStuff($transactionId, $transactionType, $userId, $year, $month);
        $sum = $query->count('transaction_trail.id');
        return $sum;
    }

    public function findMonthAmount($transactionId = null, $orderStatus = null, $transactionMethod = null, $userId = null, $year = null, $month = null) {
        $query = $this->findStuff($transactionId, $orderStatus, $transactionMethod, $userId, $year, $month);

        $result = $query->select('sum(transaction_trail.amount) amount, monthname(transaction_trail.created_time) month,year(transaction_trail.created_time) year')
                ->groupBy('month(transaction_trail.created_time)')
                ->all();
        return $result;
    }

    public function findYearReport($transactionId = null, $transactionType = null, $userId = null, $year = null, $month = null) {
        $whereQ = "";
        $bindParam;
        if (isset($transactionId)){
            $whereQ .= ' and tt.transaction_id = :transaction_id';
            $bindParam[':transaction_id'] = $transactionId;
        }
        if (isset($transactionType)){
            $whereQ .= ' and tt.transaction_type = :transaction_type';
            $bindParam[':transaction_type'] = $transactionType;
        }
        if (isset($userId)){
            $whereQ .= ' and tt.app_user_id = :created_by';
            $bindParam[':created_by'] = $userId;
        }
        if (isset($year)){
            $whereQ .= ' and year(tt.ts) = :yearTs';
            $bindParam[':yearTs'] = $year;
        }
        if (isset($month)){
            $whereQ .= ' and month(tt.ts) = :monthTs';
            $bindParam[':monthTs'] = $month;
        }
        
        $query = "SELECT ifnull(SUM(tt.amount),0) amount_sum FROM months m
	LEFT join transaction_trail tt 
		ON  MONTH(tt.ts) = m.id $whereQ
	GROUP BY m.id";
        \Yii::error($query);
        \Yii::error($bindParam);
        return \Yii::$app->getDb()
                ->createCommand($query)
                ->bindValues($bindParam)->queryAll();
    }
    
}
