<?php

namespace app\models\queries;

/**
 * This is the ActiveQuery class for [[\app\models\TransactionServiceData]].
 *
 * @see \app\models\TransactionServiceData
 */
class TransactionServiceDataQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\models\TransactionServiceData[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\TransactionServiceData|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
