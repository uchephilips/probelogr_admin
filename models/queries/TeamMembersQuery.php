<?php

namespace app\models\queries;

use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the ActiveQuery class for [[\app\models\TeamMembers]].
 *
 * @see \app\models\TeamMembers
 */
class TeamMembersQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return \app\models\TeamMembers[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\TeamMembers|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    /**
     * Finds the Team model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \app\models\TeamMembers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModelTeamMember($id) {
        if (($model = \app\models\TeamMembers::findOne(['id' => $id, 'created_by' => Yii::$app->user->identity->appUserId])) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function hasDuplicate($teamId, $email) {
        $count = $this->findCountTeamMembers('email', $email,$teamId);
        return $count > 0;
    }

    /**
     * Finds the Team model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \app\models\TeamMembers[] the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModelTeamMembers($id) {
        if (($model = \app\models\TeamMembers::findAll(['team_id' => $id, 'created_by' => Yii::$app->user->identity->appUserId])) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * 
     * @param type $col
     * @param type $id
     * @return int
     */
    public function findCountTeamMembers($col, $value, $teamId) {
        $count = \app\models\TeamMembers::find()->andOnCondition(['team_id' => $teamId, $col => $value, 'created_by' => Yii::$app->user->identity->appUserId])->count();
        return $count;
    }

    /**
     * 
     * @param type $col
     * @param type $id
     * @return int
     */
    public function findSharedAppCount() {
        $count = \app\models\TeamMembers::find()->andOnCondition(['user_id' => Yii::$app->user->identity->appUserId])->count();
        return $count;
    }

}
