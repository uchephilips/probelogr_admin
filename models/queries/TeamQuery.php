<?php

namespace app\models\queries;

/**
 * This is the ActiveQuery class for [[\app\models\Team]].
 *
 * @see \app\models\Team
 */
class TeamQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return \app\models\Team[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\Team|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    /**
     * 
     * @param type $name
     * @param type $userId
     * @return boolean
     */
    public function isUniqueTeamName($name, $userId) {
        $proj = \app\models\Team::findOne(['team_name' => $name, 'created_by' => $userId]);
        return !isset($proj);
    }

}
