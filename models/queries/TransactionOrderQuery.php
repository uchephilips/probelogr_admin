<?php

namespace app\models\queries;

/**
 * This is the ActiveQuery class for [[\app\models\TransactionOrder]].
 *
 * @see \app\models\TransactionOrder
 */
class TransactionOrderQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return \app\models\TransactionOrder[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\TransactionOrder|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function countByTransactionId($transactionId, $orderStatus) {
        $queryResult = \Yii::$app->db->createCommand('select count(*) c from transaction_order where transaction_id = :transaction_id and order_status = :order_status')
                ->bindParam(":transaction_id", $transactionId)
                ->bindParam(":order_status", $orderStatus)
                ->queryOne();
        return ($queryResult['c']);
    }

    /**
     * 
     * @param type $transactionId
     * @param type $orderStatus
     * @param type $transactionMethod
     * @param type $userId
     * @param type $year
     * @param type $month
     * @return type
     */
    private function findStuff($transactionId = null, $orderStatus = null, $transactionMethod = null, $userId = null, $year = null, $month = null) {

        $query = (new \yii\db\Query())->from(['transaction_order', 'transaction']);
        $query->andWhere('transaction_order.transaction_id = transaction.id');

        if (isset($transactionId))
            $query->andWhere(['transaction_order.transaction_id' => $transactionId]);
        if (isset($orderStatus))
            $query->andWhere(['transaction_order.order_status' => $orderStatus]);
        if (isset($transactionMethod))
            $query->andWhere(['transaction_order.transaction_method' => $transactionMethod]);
        if (isset($userId))
            $query->andWhere(['transaction.created_by' => $userId]);
        if (isset($year))
            $query->andWhere(['year(transaction_order.created_time)' => $year]);
        if (isset($month))
            $query->andWhere(['month(transaction_order.created_time)' => $month]);

        return $query;
    }

    /**
     * 
     * @param type $transactionId
     * @param type $orderStatus
     * @param type $transactionMethod
     * @param type $userId
     * @return type
     */
    public function findTransOrderSumAmount($transactionId = null, $orderStatus = null, $transactionMethod = null, $userId = null, $year = null, $month = null) {
        $query = $this->findStuff($transactionId, $orderStatus, $transactionMethod, $userId, $year, $month);
        $sum = $query->sum('transaction_order.amount');
        return isset($sum)?$sum:0;
    }

    /**
     * 
     * @param type $transactionId
     * @param type $orderStatus
     * @param type $transactionMethod
     * @param type $userId
     * @return type
     */
    public function findTransOrderCount($transactionId = null, $orderStatus = null, $transactionMethod = null,$userId = null, $year = null, $month = null) {
        $query = $this->findStuff($transactionId, $orderStatus, $transactionMethod, $userId, $year, $month);
        $sum = $query->count('transaction_order.id');
        return $sum;
    }

    /**
     * 
     * @param type $transactionId
     * @param type $orderStatus
     * @param type $transactionMethod
     * @param type $userId
     * @return type
     */
    public function findTransOrderMonthAmount($transactionId = null, $orderStatus = null, $transactionMethod = null, $userId = null, $year = null, $month = null) {
        $query = $this->findStuff($transactionId, $orderStatus, $transactionMethod, $userId, $year, $month);

        $result = $query->select('sum(transaction_order.amount) amount, monthname(transaction_order.created_time) month,year(transaction_order.created_time) year')
                ->groupBy('month(transaction_order.created_time)')
                ->all();
        return $result;
    }

}
