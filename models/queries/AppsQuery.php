<?php

namespace app\models\queries;

use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the ActiveQuery class for [[\app\models\Apps]].
 *
 * @see \app\models\Apps
 */
class AppsQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return \app\models\Apps[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\Apps|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public static function getTotal($createdBy) {
        $count = \app\models\Apps::find()->where(['created_by' => $createdBy, 'is_active' => 1])->count();
        if (isset($count)) {
            return $count;
        } else {
            return 0;
        }
    }

    public function findShareAppCount($inviteStatus) {
        $r = \Yii::$app->db->createCommand("SELECT COUNT(*) app_count FROM team_members tm,team_app ta
	WHERE tm.team_id = ta.team_id
		and tm.user_id = :user_id AND tm.invite_status = :inviteStatus;")
                ->bindParam(':inviteStatus', $inviteStatus)
                ->bindParam(':user_id', \Yii::$app->user->identity->appUserId)
                ->queryOne();
        return json_decode(json_encode($r));
    }

    public function findSharedAppCount() {
        $r = \Yii::$app->db->createCommand("SELECT COUNT(*) app_count FROM team_members tm,team_app ta
	WHERE tm.team_id = ta.team_id
		and tm.user_id = :user_id;")
                ->bindParam(':user_id', \Yii::$app->user->identity->appUserId)
                ->queryOne();
        return json_decode(json_encode($r));
    }

    /**
     * Finds the Apps model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Apps the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id) {
        $model = \app\models\Apps::find()->addSelect('apps.*')
                        ->addSelect(['project_name' => '(SELECT p.name FROM project p WHERE apps.project_id = p.id)'])
                        ->andOnCondition(['id' => $id, 'created_by' => \Yii::$app->user->identity->appUserId])->one();
        if ($model !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested app does not exist.');
    }

    /**
     * Finds the Apps model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Apps the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModelByIdAndUserId($id, $createdBy) {
        $model = \app\models\Apps::find()->addSelect('apps.*')
                        ->addSelect(['project_name' => '(SELECT p.name FROM project p WHERE apps.project_id = p.id)'])
                        ->andOnCondition(['id' => $id, 'created_by' => $createdBy])->one();
        if ($model !== null) {
            return $model;
        }
        throw new NotFoundHttpException("The requested app[$id] and user[$createdBy] do not exist.");
    }

    /**
     * 
     * @param type $id the id of the app.
     * @param type $createdBy the owner of the app
     * @return \app\models\Apps
     * @throws NotFoundHttpException
     */
    public function findModelShared($id, $createdBy) {
        //$createdBy = \app\models\AppUsers::find()->findTeamCreatorIdByAppId($id);

        $model = \app\models\Apps::find()->addSelect('apps.*')
                        ->addSelect(['project_name' => '(SELECT p.name FROM project p WHERE apps.project_id = p.id)'])
                        ->andOnCondition(['id' => $id, 'created_by' => $createdBy])->one();
        if ($model !== null) {
            return $model;
        }

//        $searchModel = new \app\models\search\AppsSearch();
//        $searchModel->id = $id;
//        $dataProvider = $searchModel->searchShare([]);
//        $rc = count($dataProvider->getModels());
//        if ($rc == 1) {
//            return $dataProvider->getModels()[0];
//        } else {
//            
//        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
