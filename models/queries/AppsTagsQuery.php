<?php

namespace app\models\queries;


use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the ActiveQuery class for [[\app\models\AppsTags]].
 *
 * @see \app\models\AppsTags
 */
class AppsTagsQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return \app\models\AppsTags[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\AppsTags|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public static function getTotal($createdBy, $appid = 0) {
        $ar = ['created_by' => $createdBy, 'is_active' => 1];
        if ($appid != 0) {
            $ar = array_merge($ar, ['app_id' => $appid]);
        }
        $count = \app\models\AppsTags::find()->where($ar)->count();
        if (isset($count)) {
            return $count;
        } else {
            return 0;
        }
    }
    
    

    /**
     * 
     * @param type $id
     * @return \app\models\AppsTags
     */
    public function findAppTagModel($id) {
        if (($model = \app\models\AppsTags::findOne(['id' => $id, 'created_by' => Yii::$app->user->identity->appUserId])) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function findAppTagModelShared($id,$createdBy) {
        if (($model = \app\models\AppsTags::findOne(['id' => $id, 'created_by' =>$createdBy])) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function findAppTagsModels($id) {
        if (($model = \app\models\AppsTags::findAll(['app_id' => $id, 'created_by' => Yii::$app->user->identity->appUserId])) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function findAppTagsModelsShared($id,$createdBy) {
        if (($model = \app\models\AppsTags::findAll(['app_id' => $id, 'created_by' => $createdBy])) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
