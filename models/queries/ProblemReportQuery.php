<?php

namespace app\models\queries;

/**
 * This is the ActiveQuery class for [[\app\models\ProblemReport]].
 *
 * @see \app\models\ProblemReport
 */
class ProblemReportQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\models\ProblemReport[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\ProblemReport|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
