<?php

namespace app\models\queries;

/**
 * This is the ActiveQuery class for [[\app\models\BankDetailsTransaction]].
 *
 * @see \app\models\BankDetailsTransaction
 */
class BankDetailsTransactionQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return \app\models\BankDetailsTransaction[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\BankDetailsTransaction|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function listBankAccounts() {
        return \app\models\BankDetailsTransaction::find()->where(['created_by' => \Yii::$app->user->identity->appUserId])->orderBy('bank_name', SORT_ASC)->all();
    }

    public function countBankAccounts() {
        return \app\models\BankDetailsTransaction::find()->where(['created_by' => \Yii::$app->user->identity->appUserId])->count();
    }

}
