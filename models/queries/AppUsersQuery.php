<?php

namespace app\models\queries;

use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the ActiveQuery class for [[\app\models\AppUsers]].
 *
 * @see \app\models\AppUsers
 */
class AppUsersQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return \app\models\AppUsers[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\AppUsers|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public static function findIdByEmail($email) {
        $findOne = \app\models\AppUsers::findOne(['email' => $email]);
        if (isset($findOne)) {
            return $findOne->id;
        }
        return 0;
    }

    public function findTeamCreatorIdByAppId($appId) {
        $r = \Yii::$app->db->createCommand("SELECT t.created_by FROM team_members tm,team_app ta,team t
	WHERE tm.team_id = ta.team_id and t.id = tm.team_id
		and tm.user_id = :createdBy and ta.app_id = :appId LIMIT 1;
		")
                ->bindParam(':createdBy', Yii::$app->user->identity->appUserId)
                ->bindParam(':appId', $appId)
                ->queryOne();
        $r = (json_decode(json_encode($r)));

        if (!empty($r)) {
            return $r->created_by;
        }
       
        throw new NotFoundHttpException("Could not find team creator by app id");
    }

    /**
     * 
     * @param type $email
     * @return type
     */
    public function addMemberToTeams($email) {
        $result = \Yii::$app->db->createCommand("UPDATE team_members SET user_id = :userId, invite_status = :inviteStatus  WHERE email = :email AND  user_id = 0;",
                        ['userId' => \Yii::$app->user->identity->appUserId, 'email' => $email, 'inviteStatus' => \app\assets\AppAsset::INVITE_STATUS_JOINED()])->execute();

        return $result > 0;
    }

}
