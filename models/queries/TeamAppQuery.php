<?php

namespace app\models\queries;

use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the ActiveQuery class for [[\app\models\TeamApp]].
 *
 * @see \app\models\TeamApp
 */
class TeamAppQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return \app\models\TeamApp[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\TeamApp|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    /**
     * Finds the Team model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \app\models\TeamMembers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModelTeamApp($id) {
        if (($model = \app\models\TeamApp::findOne(['id' => $id, 'created_by' => Yii::$app->user->identity->appUserId])) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function hasDuplicate($teamId, $appId) {
        $count = \app\models\TeamApp::find()->andOnCondition(['team_id' => $teamId, 'app_id' => $appId, 'created_by' => Yii::$app->user->identity->appUserId])->count();
        if ($count > 0) {
            return true;
        }
        return false;
    }

    /**
     * Finds the Team model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \app\models\TeamMembers[] the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModelTeamApps($teamId) {
        if (($model = \app\models\TeamApp::findAll(['team_id' => $teamId, 'created_by' => Yii::$app->user->identity->appUserId])) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function findListByTeam($teamId) {
        $r = Yii::$app->db->createCommand("SELECT t.id team_id,ta.id,a.name,t.team_name,ta.tags FROM team_app ta,team t, apps a
	WHERE a.id = ta.app_id AND t.id = ta.team_id and t.created_by = :createdBy and t.id = :team")
                ->bindParam(':createdBy', Yii::$app->user->identity->appUserId)
                ->bindParam(':team', $teamId)
                ->queryAll();
        return json_decode(json_encode($r));
    }

    public function findListByAppId($appId) {
        $r = Yii::$app->db->createCommand("SELECT t.team_name,(SELECT COUNT(*) FROM team_members tm WHERE tm.team_id = t.id) team_members_count FROM team t, team_app ta
	WHERE t.id = ta.team_id AND ta.app_id = :appId and
        t.created_by = :createdBy and
        ta.created_by = :createdBy")
                ->bindParam(':createdBy', Yii::$app->user->identity->appUserId)
                ->bindParam(':appId', $appId)
                ->queryAll();
        return json_decode(json_encode($r));
    }

    public function findListByAppIdShared($appId, $createdBy) {
        $r = Yii::$app->db->createCommand("SELECT t.team_name,(SELECT COUNT(*) FROM team_members tm WHERE tm.team_id = t.id) team_members_count FROM team t, team_app ta
	WHERE t.id = ta.team_id AND ta.app_id = :appId and
        t.created_by = :createdBy and
        ta.created_by = :createdBy")
                ->bindParam(':createdBy', $createdBy)
                ->bindParam(':appId', $appId)
                ->queryAll();
        return json_decode(json_encode($r));
    }

}
