<?php

namespace app\models\queries;

/**
 * This is the ActiveQuery class for [[\app\models\Transaction]].
 *
 * @see \app\models\Transaction
 */
class TransactionQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return \app\models\Transaction[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\Transaction|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    private function findStuff($transactionId = null, $transactionType = null, $userId = null, $year = null, $month = null) {

        
        $whereQ = "";
        $bindParam;
        if (isset($transactionId)){
            $whereQ .= ' and transaction_trail.transaction_id = :transaction_id';
            $bindParam[':transaction_id'] = $transactionId;
        }
        if (isset($transactionType)){
            $whereQ .= ' and transaction_trail.transaction_type = :transaction_type';
            $bindParam[':transaction_type'] = $transactionType;
        }
        if (isset($userId)){
            $whereQ .= ' and transaction.created_by = :created_by';
            $bindParam[':created_by'] = $userId;
        }
        if (isset($year)){
            $whereQ .= ' and year(transaction_trail.ts) = :yearTs';
            $bindParam[':yearTs'] = $year;
        }
        if (isset($month)){
            $whereQ .= ' and month(transaction_trail.ts) = :monthTs';
            $bindParam[':monthTs'] = $month;
        }
        
        $query = 'SELECT * FROM (SELECT t.btn,t.amount,t.description,SUM(tt.amount) amount_sum
                     FROM transaction_trail tt,`transaction` t WHERE t.id = tt.transaction_id '+$whereQ+'
                     GROUP BY t.id) subtable ORDER BY amount_sum DESC limit 10';
        
        return \Yii::$app->getDb()
                ->createCommand($query)
                ->bindValues($bindParam);
    }

    /**
     * 
     * @param type $transactionId
     * @param type $orderStatus
     * @param type $transactionType
     * @param type $userId
     * @param type $year
     * @param type $month
     * @return type
     */
    public function findTopTenTrans($transactionId = null, $transactionType = null, $userId = null, $year = null, $month = null) {
        $whereQ = "";
        $bindParam;
        if (isset($transactionId)){
            $whereQ .= ' and transaction_trail.transaction_id = :transaction_id';
            $bindParam[':transaction_id'] = $transactionId;
        }
        if (isset($transactionType)){
            $whereQ .= ' and transaction_trail.transaction_type = :transaction_type';
            $bindParam[':transaction_type'] = $transactionType;
        }
        if (isset($userId)){
            $whereQ .= ' and transaction.created_by = :created_by';
            $bindParam[':created_by'] = $userId;
        }
        if (isset($year)){
            $whereQ .= ' and year(transaction_trail.ts) = :yearTs';
            $bindParam[':yearTs'] = $year;
        }
        if (isset($month)){
            $whereQ .= ' and month(transaction_trail.ts) = :monthTs';
            $bindParam[':monthTs'] = $month;
        }
        
        $query = "SELECT * FROM (SELECT transaction.btn,transaction.amount,transaction.description,SUM(transaction_trail.amount) amount_sum
                     FROM transaction_trail,`transaction`  WHERE transaction.id = transaction_trail.transaction_id $whereQ
                     GROUP BY transaction.id) subtable ORDER BY amount_sum DESC limit 10";
        \Yii::error($query);
        \Yii::error($bindParam);
        return \Yii::$app->getDb()
                ->createCommand($query)
                ->bindValues($bindParam)->queryAll();
    }
    
    public function findYearTrans($transactionId = null, $transactionType = null, $userId = null, $year = null, $month = null) {
        $whereQ = "";
        $bindParam;
        if (isset($transactionId)){
            $whereQ .= ' and transaction_trail.transaction_id = :transaction_id';
            $bindParam[':transaction_id'] = $transactionId;
        }
        if (isset($transactionType)){
            $whereQ .= ' and transaction_trail.transaction_type = :transaction_type';
            $bindParam[':transaction_type'] = $transactionType;
        }
        if (isset($userId)){
            $whereQ .= ' and transaction_trail.app_user_id = :created_by';
            $bindParam[':created_by'] = $userId;
        }
        if (isset($year)){
            $whereQ .= ' and year(transaction_trail.ts) = :yearTs';
            $bindParam[':yearTs'] = $year;
        }
        if (isset($month)){
            $whereQ .= ' and month(transaction_trail.ts) = :monthTs';
            $bindParam[':monthTs'] = $month;
        }
        
        $query = "SELECT ifnull(SUM(tt.amount),0) amount_sum, COUNT(tt.id),m.name FROM months m
	LEFT join transaction_trail tt 
		ON  MONTH(tt.ts) = m.id $whereQ
	GROUP BY m.id";
        
        return \Yii::$app->getDb()
                ->createCommand($query)
                ->bindValues($bindParam)->queryAll();
    }

}
