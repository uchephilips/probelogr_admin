<?php

namespace app\models\queries;


use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the ActiveQuery class for [[\app\models\AppsEndpoints]].
 *
 * @see \app\models\AppsEndpoints
 */
class AppsEndpointsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\models\AppsEndpoints[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\AppsEndpoints|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
    
    

    /**
     * 
     * @param type $id
     * @return \app\models\AppsEndpoints
     */
    public function findAppEndpointModel($id) {
        $endpoint = \app\models\AppsEndpoints::findOne(['app_id' => $id, 'created_by' => Yii::$app->user->identity->appUserId]);
        if (isset($endpoint)) {
            return $endpoint;
        }
        return new \app\models\AppsEndpoints();
    }

    /**
     * 
     * @param type $id
     * @return \app\models\AppsEndpoints
     */
    public function findAppEndpointModelShared($id,$createdBy) {
        $endpoint = \app\models\AppsEndpoints::findOne(['app_id' => $id, 'created_by' => $createdBy]);
        if (isset($endpoint)) {
            return $endpoint;
        }
        return new \app\models\AppsEndpoints();
    }
    
}
