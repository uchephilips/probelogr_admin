<?php

namespace app\models\queries;

/**
 * This is the ActiveQuery class for [[\app\models\SettingsCharges]].
 *
 * @see \app\models\SettingsCharges
 */
class SettingsChargesQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return \app\models\SettingsCharges[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\SettingsCharges|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function calCharges($amount, $applyTo) {
        $scs = \app\models\SettingsCharges::findAll(['is_deductable' => true, 'apply_to' => $applyTo]);
        $newAmount = $amount;
        foreach ($scs as $sc) {
            $flatFee = $sc->flat_fee;
            $percent = $sc->percent;
            $cap = $sc->capped_at;

            $perc = ($amount / 100) * $percent;
            $newAmount += ($perc > $cap) ? $cap : $perc;
            $newAmount += $flatFee;
        }
        return $newAmount;
    }
    public function calChargesDiff($amount, $applyTo) {
        $scs = \app\models\SettingsCharges::findAll(['is_deductable' => true, 'apply_to' => $applyTo]);
        $newAmount = $amount;
        foreach ($scs as $sc) {
            $flatFee = $sc->flat_fee;
            $percent = $sc->percent;
            $cap = $sc->capped_at;

            $perc = ($amount / 100) * $percent;
            $newAmount += ($perc > $cap) ? $cap : $perc;
            $newAmount += $flatFee;
        }
        return $newAmount-$amount;
    }

}
