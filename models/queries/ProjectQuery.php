<?php

namespace app\models\queries;

/**
 * This is the ActiveQuery class for [[\app\models\Project]].
 *
 * @see \app\models\Project
 */
class ProjectQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return \app\models\Project[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\Project|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    /**
     * 
     * @param type $name
     * @param type $userId
     * @return boolean
     */
    public function isUniqueProjectName($name, $userId) {
        $proj = \app\models\Project::findOne(['name' => $name, 'created_by' => $userId]);
        return !isset($proj);
    }

    public static function get($id, $createdBy) {
        $proj = \app\models\Project::findOne(['id' => $id, 'created_by' => $createdBy]);
        if (isset($proj)) {
            return $proj;
        } else {
            return new \app\models\Project();
        }
    }

    public static function getTotal($createdBy) {
        $count = \app\models\Project::find()->where(['created_by' => $createdBy, 'is_active' => 1])->count();
        if (isset($count)) {
            return $count;
        } else {
            return 0;
        }
    }

    /**
     * 
     * @param type $id
     * @return \app\models\Project
     * @throws NotFoundHttpException
     */
    public function findProjectModel($id) {
        if (($model = \app\models\Project::findOne(['id' => $id, 'created_by' => \Yii::$app->user->identity->appUserId])) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
