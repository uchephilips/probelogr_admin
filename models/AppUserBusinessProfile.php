<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "app_user_business_profile".
 *
 * @property int $id
 * @property string $business_name
 * @property string $about_business
 * @property string $business_logo
 * @property string $street
 * @property string $area
 * @property int $lga_id
 * @property int $state_id
 * @property string $id_type
 * @property string $id_number
 * @property string $id_photo
 * @property int $created_by
 * @property string $created_time
 * @property int $updated_by
 * @property string $updated_time
 * @property bool $verified
 * @property bool $payment_id
 */
class AppUserBusinessProfile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'app_user_business_profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['business_name', 'about_business', 'created_by', 'created_time'], 'required'],
            [['lga_id', 'state_id', 'created_by', 'updated_by'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['verified'], 'boolean'],
            [['business_name', 'business_logo', 'street', 'id_photo'], 'string', 'max' => 100],
            [['about_business'], 'string', 'max' => 500],
            [['area'], 'string', 'max' => 50],
            [['id_type', 'id_number'], 'string', 'max' => 20],
            [['business_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'business_name' => 'Business Name',
            'about_business' => 'About Business',
            'business_logo' => 'Business Logo',
            'street' => 'Street',
            'area' => 'Area',
            'lga_id' => 'Lga',
            'state_id' => 'State',
            'id_type' => 'Id Type',
            'id_number' => 'Id Number',
            'id_photo' => 'Id Photo',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'updated_by' => 'Updated By',
            'updated_time' => 'Updated Time',
            'verified' => 'Verified',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\AppUserBusinessProfileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\AppUserBusinessProfileQuery(get_called_class());
    }
}
