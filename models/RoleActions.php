<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "role_actions".
 *
 * @property int $id
 * @property int $role_id
 * @property int $page_action_id
 * @property int $created_by
 * @property string $created_time
 * @property string $update_time
 * @property int $updated_by
 * @property $tenant_id
 * @property bool $is_active
 */
class RoleActions extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'role_actions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['role_id', 'page_action_id', 'created_by', 'created_time'], 'required'],
            [['role_id', 'page_action_id', 'created_by', 'updated_by'], 'integer'],
            [['created_time', 'update_time'], 'safe'],
            [['is_active'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'role_id' => 'Role ID',
            'page_action_id' => 'Page Action ID',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'update_time' => 'Update Time',
            'updated_by' => 'Updated By',
            'tenant_id' => 'Tenant ID',
            'is_active' => 'Is Active',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\RoleActionsQuery the active query used by this AR class.
     */
    public static function find() {
        return new \app\models\queries\RoleActionsQuery(get_called_class());
    }

    /**
     * @return PageActions returns page_action with the page_action_id
     */
    public function getPageActions() {
        return PageActions::findOne(['id' => $this->page_action_id]);
    }

}
