<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "apps_endpoints".
 *
 * @property int $id
 * @property string $access_key
 * @property int $app_id
 * @property int $created_by
 * @property string $created_time
 * @property string $update_time
 * @property int $updated_by
 * @property bool $is_active
 * @property bool $is_suspended
 */
class AppsEndpoints extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'apps_endpoints';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['access_key', 'app_id', 'created_by', 'created_time'], 'required'],
            [['app_id', 'created_by', 'updated_by'], 'integer'],
            [['created_time', 'update_time'], 'safe'],
            [['is_active', 'is_suspended'], 'boolean'],
            [['access_key'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'access_key' => 'Access Key',
            'app_id' => 'App ID',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'update_time' => 'Update Time',
            'updated_by' => 'Updated By',
            'is_active' => 'Is Active',
            'is_suspended' => 'Is Suspended',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\AppsEndpointsQuery the active query used by this AR class.
     */
    public static function find() {
        return new \app\models\queries\AppsEndpointsQuery(get_called_class());
    }

    public static function genAccessKey() {
        $unq1 = uniqid("", true) . uniqid("", true);
        $unq2 = str_replace('.', '-', $unq1);

        $appsE = AppsEndpoints::findOne(['access_key' => $unq2, 'created_by' => Yii::$app->user->identity->appUserId]);
        if (isset($appsE)) {
            return self::genAccessKey();
        } else {
            return $unq2;
        }
    }

    public static function validataAccessKey($accessKey) {
        $appsE = AppsEndpoints::findOne(['access_key' => $accessKey, 'created_by' => Yii::$app->user->identity->appUserId]);
        return !isset($appsE);
    }

}
