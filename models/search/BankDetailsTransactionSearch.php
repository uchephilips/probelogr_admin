<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BankDetailsTransaction;

/**
 * BankDetailsTransactionSearch represents the model behind the search form of `app\models\BankDetailsTransaction`.
 */
class BankDetailsTransactionSearch extends BankDetailsTransaction {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'bank_account_number', 'created_by', 'updated_by'], 'integer'],
            [['bank_name', 'bank_code', 'created_time', 'ts'], 'safe'],
            [['is_active'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = BankDetailsTransaction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        //add app user id to the query condition
        if (!\app\assets\RoleManagement::checkIfUserHasPrivilege('search-record', 'search-other-user')) {
            $this->created_by = \Yii::$app->user->identity->appUserId;
        }


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'bank_account_number' => $this->bank_account_number,
            'created_by' => $this->created_by,
            'created_time' => $this->created_time,
            'updated_by' => $this->updated_by,
            'is_active' => 1,
            'ts' => $this->ts,
        ]);

        $query->orderBy(['id' => SORT_DESC]);

        $query->andFilterWhere(['like', 'bank_name', $this->bank_name])
                ->andFilterWhere(['like', 'bank_code', $this->bank_code]);

        return $dataProvider;
    }

}
