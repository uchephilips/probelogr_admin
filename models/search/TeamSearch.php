<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Team;

/**
 * TeamSearch represents the model behind the search form of `app\models\Team`.
 */
class TeamSearch extends Team
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by'], 'integer'],
            [['team_name', 'description', 'created_time', 'update_time'], 'safe'],
            [['is_active', 'is_suspended'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Team::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->addSelect('team.*')
                ->addSelect(['member_count' => '(SELECT count(*) FROM team_members p WHERE team.id = p.team_id)'])
                ;
        
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => \Yii::$app->user->identity->appUserId,
            'created_time' => $this->created_time,
            'update_time' => $this->update_time,
            'updated_by' => $this->updated_by,
            'is_active' => $this->is_active,
            'is_suspended' => $this->is_suspended,
        ]);

        $query->orderBy(['id' => SORT_DESC]);
        
        $query->andFilterWhere(['like', 'team_name', $this->team_name])
            ->andFilterWhere(['like', 'description', $this->description]);

        
        
        return $dataProvider;
    }
}
