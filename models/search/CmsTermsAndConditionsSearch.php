<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CmsTermsAndConditions;

/**
 * CmsTermsAndConditionsSearch represents the model behind the search form of `app\models\CmsTermsAndConditions`.
 */
class CmsTermsAndConditionsSearch extends CmsTermsAndConditions {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'parent_id', 'created_by', 'updated_by'], 'integer'],
            [['txt', 'title', 'created_time', 'updated_time'], 'safe'],
            [['is_draft', 'update_original'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = CmsTermsAndConditions::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_draft' => $this->is_draft,
            'parent_id' => $this->parent_id,
            'update_original' => $this->update_original,
            'created_by' => $this->created_by,
            'created_time' => $this->created_time,
            'updated_by' => $this->updated_by,
            'updated_time' => $this->updated_time,
        ]);

        $query->orderBy(['id' => SORT_DESC]);
        
        $query->andFilterWhere(['like', 'txt', $this->txt])
                ->andFilterWhere(['like', 'title', $this->title]);
        $query->select('id,is_draft,update_original,created_time,created_by,updated_time,updated_by,title');
        return $dataProvider;
    }

}
