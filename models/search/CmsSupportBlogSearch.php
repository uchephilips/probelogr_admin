<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CmsSupportBlog;

/**
 * CmsSupportBlogSearch represents the model behind the search form of `app\models\CmsSupportBlog`.
 */
class CmsSupportBlogSearch extends CmsSupportBlog
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by'], 'integer'],
            [['txt', 'title', 'tag', 'tag_line', 'category', 'author_name', 'summary', 'publish_date', 'created_time', 'updated_time'], 'safe'],
            [['publish'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CmsSupportBlog::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->addSelect('cms_support_blog.*')
                ->addSelect(['createdBy' => "(select concat( r.first_name,' ',r.last_name) from app_users r where r.id = cms_support_blog.created_by)"
                    ,'updatedBy'=>"(select concat( r.first_name,' ',r.last_name) from app_users r where r.id = cms_support_blog.updated_by)"]
                        );

        
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'publish' => $this->publish,
            'publish_date' => $this->publish_date,
            'created_by' => $this->created_by,
            'created_time' => $this->created_time,
            'updated_by' => $this->updated_by,
            'updated_time' => $this->updated_time,
        ]);

        $query->orderBy(['id' => SORT_DESC]);
        
        $query->andFilterWhere(['like', 'txt', $this->txt])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'tag', $this->tag])
            ->andFilterWhere(['like', 'tag_line', $this->tag_line])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'author_name', $this->author_name])
            ->andFilterWhere(['like', 'summary', $this->summary]);

        return $dataProvider;
    }
}
