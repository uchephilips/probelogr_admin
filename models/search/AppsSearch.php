<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Apps;

/**
 * AppsSearch represents the model behind the search form of `app\models\Apps`.
 */
class AppsSearch extends Apps {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'project_id', 'created_by', 'updated_by'], 'integer'],
            [['name', 'description', 'created_time', 'update_time'], 'safe'],
            [['is_active', 'is_suspended'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Apps::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->addSelect('apps.*')
                ->addSelect(['project_name' => '(SELECT p.name FROM project p WHERE apps.project_id = p.id)'])
                ->addSelect(['has_settings' => '(SELECT ifnull(ae.id,0) FROM apps_endpoints ae WHERE apps.id = ae.app_id)'])
                ->addSelect(['has_tags' => '(SELECT ifnull(ae.id,0) FROM apps_tags ae WHERE apps.id = ae.app_id limit 1)'])
        ;

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'project_id' => $this->project_id,
            'created_by' => \Yii::$app->user->identity->appUserId,
            'created_time' => $this->created_time,
            'update_time' => $this->update_time,
            'updated_by' => $this->updated_by,
            'is_active' => $this->is_active,
            'is_suspended' => $this->is_suspended,
        ]);

        $query->orderBy(['id' => SORT_DESC]);

        $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchShare($params) {
        $query = Apps::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->addSelect('apps.*, t.team_name,tm.invite_status,tm.role')
                ->addSelect(['project_name' => '(SELECT p.name FROM project p WHERE apps.project_id = p.id)'])
                ->addSelect(['has_settings' => '(SELECT ifnull(ae.id,0) FROM apps_endpoints ae WHERE apps.id = ae.app_id)'])
                ->addSelect(['has_tags' => '(SELECT ifnull(ae.id,0) FROM apps_tags ae WHERE apps.id = ae.app_id limit 1)']) //no idea what is does
                ->addSelect(['shared_by' => '(SELECT ae.email FROM app_users ae WHERE ae.id = tm.created_by limit 1)']);

        $query->from(['apps', 'tm' => 'team_members', 't' => 'team', 'ta' => 'team_app']);

        $query->where('apps.id = ta.app_id and tm.team_id = ta.team_id and t.id = tm.team_id');

        // grid filtering conditions
        $query->andFilterWhere([
            'apps.id' => $this->id,
            'apps.project_id' => $this->project_id,
            'apps.created_time' => $this->created_time,
            'apps.update_time' => $this->update_time,
            'apps.updated_by' => $this->updated_by,
            'apps.is_active' => $this->is_active,
            'apps.is_suspended' => $this->is_suspended,
            'tm.user_id' => \Yii::$app->user->identity->appUserId,
        ]);

        $query->orderBy(['id' => SORT_DESC]);

        $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }

    /**
     * 
     * @return Product[]
     */
    public static function searchAppAsSelectOption($search) {
        $result = (isset($search) && strlen($search)) > 0 ? (\app\models\Apps::find()
                        ->andFilterWhere([
                            'like', 'concat(name," ",description)', $search,
                            'is_active' => 1,
                            'is_suspended' => 0])
                        ->andFilterWhere(['created_by' => \Yii::$app->user->identity->appUserId])
                        ->select(['id', 'name text'])
                        ->asArray()
                        ->all()) : (\app\models\Apps::find()
                        ->andFilterWhere([
                            'is_active' => 1,
                            'is_suspended' => 0])
                        ->andFilterWhere(['created_by' => \Yii::$app->user->identity->appUserId])
                        ->select(['id', 'name text'])
                        ->asArray()
                        ->all());

        return ['results' => $result];
    }

    public static function getTeamLeadId($appId) {
        
        $re = \Yii::$app->db->createCommand('SELECT t.id,t.created_by FROM apps a, team_app ta,team_members tm,team t
	WHERE ta.app_id = a.id AND tm.team_id = ta.team_id AND t.id = tm.team_id
	AND tm.user_id = :userid AND ta.app_id = :appid', [':userid' => \Yii::$app->user->identity->appUserId, 'appid' => $appId])->queryOne();

        if (!empty($re)) {
            return $re['created_by'];
        } else {
            return \Yii::$app->user->identity->appUserId;
        }
    }

    public static function getTeamLeadIdByUser() {
        $re = \Yii::$app->db->createCommand('SELECT t.id,t.created_by FROM apps a, team_app ta,team_members tm,team t
	WHERE ta.app_id = a.id AND tm.team_id = ta.team_id AND t.id = tm.team_id
	AND tm.user_id = :userid', [':userid' => \Yii::$app->user->identity->appUserId])->queryAll();
     return $re;   
    }

}
