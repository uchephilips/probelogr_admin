<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CmsHeaderSlider;

/**
 * CmsHeaderSliderSearch represents the model behind the search form of `app\models\CmsHeaderSlider`.
 */
class CmsHeaderSliderSearch extends CmsHeaderSlider
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by'], 'integer'],
            [['headline_txt', 'summary_txt', 'link', 'link_txt', 'img_link', 'publish_date', 'created_time', 'updated_time'], 'safe'],
            [['publish'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CmsHeaderSlider::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'publish' => $this->publish,
            'publish_date' => $this->publish_date,
            'created_by' => $this->created_by,
            'created_time' => $this->created_time,
            'updated_by' => $this->updated_by,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'headline_txt', $this->headline_txt])
            ->andFilterWhere(['like', 'summary_txt', $this->summary_txt])
            ->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'link_txt', $this->link_txt])
            ->andFilterWhere(['like', 'img_link', $this->img_link]);

        return $dataProvider;
    }
}
