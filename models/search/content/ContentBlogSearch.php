<?php

namespace app\models\search\content;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\content\ContentBlog;

/**
 * ContentBlogSearch represents the model behind the search form of `app\models\content\ContentBlog`.
 */
class ContentBlogSearch extends ContentBlog
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by'], 'integer'],
            [['header_image_url', 'content_title', 'blog_content', 'tags', 'tagline', 'created_time', 'update_time', 'is_published'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContentBlog::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'created_time' => $this->created_time,
            'update_time' => $this->update_time,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'header_image_url', $this->header_image_url])
            ->andFilterWhere(['like', 'content_title', $this->content_title])
            ->andFilterWhere(['like', 'blog_content', $this->blog_content])
            ->andFilterWhere(['like', 'tags', $this->tags])
            ->andFilterWhere(['like', 'tagline', $this->tagline])
            ->andFilterWhere(['like', 'is_published', $this->is_published]);

        return $dataProvider;
    }
}
