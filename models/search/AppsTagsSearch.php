<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AppsTags;

/**
 * AppsTagsSearch represents the model behind the search form of `app\models\AppsTags`.
 */
class AppsTagsSearch extends AppsTags {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'created_by', 'app_id', 'updated_by'], 'integer'],
            [['tags', 'description', 'color_code', 'has_email_trigger', 'log_type', 'created_time', 'update_time'], 'safe'],
            [['is_active', 'is_suspended'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = AppsTags::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'app_id' => $this->app_id,
            'created_time' => $this->created_time,
            'update_time' => $this->update_time,
            'updated_by' => $this->updated_by,
            'is_active' => $this->is_active,
            'is_suspended' => $this->is_suspended,
        ]);

        $query->andFilterWhere(['like', 'tags', $this->tags])
                ->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['like', 'color_code', $this->color_code])
                ->andFilterWhere(['like', 'has_email_trigger', $this->has_email_trigger])
                ->andFilterWhere(['like', 'log_type', $this->log_type]);

        return $dataProvider;
    }

    
    /**
     * 
     * @return Product[]
     */
    public static function searchAppTagsAsSelectOption($search) {
        $result = (isset($search) && strlen($search)) > 0 ? (\app\models\AppsTags::find()
                        ->andFilterWhere([
                            'app_id' => $search,
                            'is_active' => 1,
                            'is_suspended' => 0])
                        ->andFilterWhere(['created_by' => \Yii::$app->user->identity->appUserId])
                        ->select(['tags id', 'tags text'])
                        ->asArray()
                        ->all()) : [];

        return ['results' => $result];
    }
    
}
