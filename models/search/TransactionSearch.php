<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Transaction;

/**
 * TransactionSearch represents the model behind the search form of `app\models\Transaction`.
 */
class TransactionSearch extends Transaction {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'btn', 'transaction_bank_detail_id', 'payer_bank_details_id', 'created_by'], 'integer'],
            [['token', 'description', 'status', 'expiry_date', 'created_time', 'ts'], 'safe'],
            [['amount'], 'number'],
            [['is_active'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Transaction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        //add app user id to the query condition
        if (!\app\assets\RoleManagement::checkIfUserHasPrivilege('search-record', 'search-other-user')) {
            $this->created_by = \Yii::$app->user->identity->appUserId;
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->addSelect('transaction.*')
                ->addSelect(['order_count' => '(SELECT COUNT(*) FROM transaction_order tor WHERE tor.transaction_id = transaction.id AND tor.order_status = "PAID")'])
                ->addSelect(['amount_received' => '(SELECT ifnull(SUM(tor.amount),0) FROM transaction_order tor WHERE tor.transaction_id = transaction.id AND tor.order_status = "PAID")'])
                ->addSelect(['amount_paidout' => '(SELECT ifnull(SUM(tor.amount),0) FROM transaction_order tor WHERE tor.transaction_id = transaction.id AND tor.order_status = "SETTLED")']);


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'btn' => $this->btn,
            'amount' => $this->amount,
            'transaction_bank_detail_id' => $this->transaction_bank_detail_id,
            'payer_bank_details_id' => $this->payer_bank_details_id,
            'expiry_date' => $this->expiry_date,
            'created_time' => $this->created_time,
            'created_by' => $this->created_by,
            'is_active' => $this->is_active,
            'ts' => $this->ts,
        ]);

        $query->orderBy(['id' => SORT_DESC]);


        $query->andFilterWhere(['like', 'token', $this->token])
                ->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }

}
