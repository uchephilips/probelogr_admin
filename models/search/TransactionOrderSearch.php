<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TransactionOrder;

/**
 * TransactionOrderSearch represents the model behind the search form of `app\models\TransactionOrder`.
 */
class TransactionOrderSearch extends TransactionOrder
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'transaction_id', 'transaction_count', 'amount'], 'integer'],
            [['customer_info', 'created_time', 'order_status', 'token'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TransactionOrder::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'transaction_id' => $this->transaction_id,
            'transaction_count' => $this->transaction_count,
            'amount' => $this->amount,
            'created_time' => $this->created_time,
        ]);
        $query->orderBy(['id' => SORT_DESC]);

        $query->andFilterWhere(['like', 'customer_info', $this->customer_info])
            ->andFilterWhere(['like', 'order_status', $this->order_status])
            ->andFilterWhere(['like', 'token', $this->token]);

        return $dataProvider;
    }
}
