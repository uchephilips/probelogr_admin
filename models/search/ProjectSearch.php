<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Project;

/**
 * ProjectSearch represents the model behind the search form of `app\models\Project`.
 */
class ProjectSearch extends Project {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'created_by', 'updated_by'], 'integer'],
            [['name', 'description', 'created_time', 'update_time'], 'safe'],
            [['is_active', 'is_suspended'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Project::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => \Yii::$app->user->identity->appUserId,
            'created_time' => $this->created_time,
            'update_time' => $this->update_time,
            'updated_by' => $this->updated_by,
            'is_active' => $this->is_active,
            'is_suspended' => $this->is_suspended,
        ]);

        $query->orderBy(['id' => SORT_DESC]);
        
        $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }

    /**
     * 
     * @return Product[]
     */
    public static function searchProjectAsSelectOption($search) {
        $result = (isset($search) && strlen($search)) > 0 ? (\app\models\Project::find()
                        ->andFilterWhere([
                            'like', 'concat(name," ",description)', $search,
                            'is_active' => 1,
                            'is_suspended' => 0])
                        ->andFilterWhere(['created_by' => \Yii::$app->user->identity->appUserId])
                        ->select(['id', 'name text'])
                        ->asArray()
                        ->all()) : (\app\models\Project::find()
                        ->andFilterWhere([
                            'is_active' => 1,
                            'is_suspended' => 0])
                        ->andFilterWhere(['created_by' => \Yii::$app->user->identity->appUserId])
                        ->select(['id', 'name text'])
                        ->asArray()
                        ->all());

        return ['results' => $result];
    }

}
