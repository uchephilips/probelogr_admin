<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AppSubscription;

/**
 * AppSubscriptionSearch represents the model behind the search form of `app\models\AppSubscription`.
 */
class AppSubscriptionSearch extends AppSubscription
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'app_no', 'tag_no', 'user_no', 'storage_no', 'created_by', 'updated_by'], 'integer'],
            [['subscription_name', 'subscription_key', 'has_support', 'has_dedicated_env', 'created_time', 'update_time'], 'safe'],
            [['is_active', 'is_suspended'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AppSubscription::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'app_no' => $this->app_no,
            'tag_no' => $this->tag_no,
            'user_no' => $this->user_no,
            'storage_no' => $this->storage_no,
            'created_by' => $this->created_by,
            'created_time' => $this->created_time,
            'update_time' => $this->update_time,
            'updated_by' => $this->updated_by,
            'is_active' => $this->is_active,
            'is_suspended' => $this->is_suspended,
        ]);

        $query->andFilterWhere(['like', 'subscription_name', $this->subscription_name])
            ->andFilterWhere(['like', 'subscription_key', $this->subscription_key])
            ->andFilterWhere(['like', 'has_support', $this->has_support])
            ->andFilterWhere(['like', 'has_dedicated_env', $this->has_dedicated_env]);

        return $dataProvider;
    }
}
