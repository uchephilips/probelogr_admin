<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AppUsers;

/**
 * AppUsersSearch represents the model behind the search form of `app\models\AppUsers`.
 */
class AppUsersSearch extends AppUsers {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
                [['id'], 'integer'],
                [['first_name', 'last_name', 'phone_number', 'email', 'create_time', 'last_update', 'password', 'auth_key', 'role_id', 'access_token', 'merchant_id'], 'safe'],
                [['is_active'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = AppUsers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->addSelect('app_users.*')
                ->addSelect(['role_name' => '(select r.role_name from roles r where r.id = app_users.role_id)',
                    'company_name'=>'(SELECT ap.company_name FROM app_user_company_profile ap where ap.created_by = app_users.id)',
                    ]);


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'create_time' => $this->create_time,
            'last_update' => $this->last_update,
            'is_active'=>$this->is_active,
            'role_id'=>$this->role_id,
            'created_by' => \app\assets\RoleManagement::checkSelfEntryCreatedById($this->created_by),
        ]);

        $query->orderBy(['id' => SORT_DESC]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
                ->andFilterWhere(['like', 'last_name', $this->last_name])
                ->andFilterWhere(['like', 'phone_number', $this->phone_number])
                ->andFilterWhere(['like', 'email', $this->email])
                ->andFilterWhere(['like', 'password', $this->password])
                ->andFilterWhere(['like', 'access_token', $this->access_token]);

        return $dataProvider;
    }

}
