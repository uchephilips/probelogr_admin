<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProblemReport;

/**
 * ProblemReportSearch represents the model behind the search form of `app\models\ProblemReport`.
 */
class ProblemReportSearch extends ProblemReport
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by'], 'integer'],
            [['tag', 'description', 'report_status', 'created_time', 'ts'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProblemReport::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        //add app user id to the query condition
        $this->created_by = \Yii::$app->user->identity->appUserId;
        if (!\app\assets\RoleManagement::checkIfUserHasPrivilege('transaction', 'view_other_user_transaction')) {
            $this->created_by = \Yii::$app->user->identity->appUserId;
        }

        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'created_time' => $this->created_time,
            'updated_by' => $this->updated_by,
            'ts' => $this->ts,
        ]);

        $query->orderBy(['id' => SORT_DESC]);
        
        $query->andFilterWhere(['like', 'tag', $this->tag])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'report_status', $this->report_status]);

        return $dataProvider;
    }
}
