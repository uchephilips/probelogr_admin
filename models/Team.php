<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "team".
 *
 * @property int $id
 * @property string $team_name
 * @property string $description
 * @property string $token
 * @property int $created_by
 * @property string $created_time
 * @property string $update_time
 * @property int $updated_by
 * @property bool $is_active
 * @property bool $is_suspended
 */
class Team extends \yii\db\ActiveRecord {

    public $member_count;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'team';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['team_name', 'created_by', 'created_time','token'], 'required'],
            [['created_by', 'updated_by'], 'integer'],
            [['created_time', 'update_time'], 'safe'],
            [['is_active', 'is_suspended'], 'boolean'],
            [['team_name'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'team_name' => 'Team Name',
            'description' => 'Description',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'update_time' => 'Update Time',
            'updated_by' => 'Updated By',
            'is_active' => 'Is Active',
            'is_suspended' => 'Is Suspended',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\TeamQuery the active query used by this AR class.
     */
    public static function find() {
        return new \app\models\queries\TeamQuery(get_called_class());
    }

    
    
    public static function getUniqueToken() {
        $token = \app\assets\Misc::unique_id(20);
        $findOne = Team::findOne(['token' => $token]);
        if (isset($findOne)) {
            return $this->getUniqueToken();
        }
        return $token;
    }
}
