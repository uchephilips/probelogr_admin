<?php

namespace app\models\content\query;

/**
 * This is the ActiveQuery class for [[\app\models\content\ContentBlog]].
 *
 * @see \app\models\content\ContentBlog
 */
class ContentBlogQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\models\content\ContentBlog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\content\ContentBlog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
