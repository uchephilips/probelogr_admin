<?php

namespace app\models\content\query;

/**
 * This is the ActiveQuery class for [[\app\models\content\ContentVideo]].
 *
 * @see \app\models\content\ContentVideo
 */
class ContentVideoQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      return $this->andWhere('[[status]]=1');
      } */

    /**
     * {@inheritdoc}
     * @return \app\models\content\ContentVideo[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\content\ContentVideo|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    /**
     * 
     * @param type $name
     * @param type $userId
     * @return boolean
     */
    public function isUnique($name) {
        $proj = \app\models\content\ContentVideo::findOne(['video_title' => $name]);
        return !isset($proj);
    }

}
