<?php

namespace app\models\content;

use Yii;

/**
 * This is the model class for table "content_video".
 *
 * @property int $id
 * @property string $video_title
 * @property string $video_url
 * @property string $video_description
 * @property string $tags
 * @property int $created_by
 * @property string $created_time
 * @property string $update_time
 * @property int $updated_by
 * @property string $is_published
 */
class ContentVideo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'content_video';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['video_title', 'video_url', 'video_description', 'tags', 'created_by', 'created_time'], 'required'],
             [['video_title'], 'unique', 'targetAttribute' => ['video_title']],
            [['created_by', 'updated_by'], 'integer'],
            [['created_time', 'update_time'], 'safe'],
            [['video_title', 'video_url'], 'string', 'max' => 500],
            [['video_description'], 'string', 'max' => 1000],
            [['tags'], 'string', 'max' => 200],
            [['is_published'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'video_title' => 'Video Title',
            'video_url' => 'YouTube Video ID',
            'video_description' => 'Video Description',
            'tags' => 'Tags',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'update_time' => 'Update Time',
            'updated_by' => 'Updated By',
            'is_published' => 'Is Published',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\content\query\ContentVideoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\content\query\ContentVideoQuery(get_called_class());
    }
}
