<?php

namespace app\models\content;

use Yii;

/**
 * This is the model class for table "content_blog".
 *
 * @property int $id
 * @property string $header_image_url
 * @property string $content_title
 * @property string $author
 * @property string $blog_description
 * @property string $blog_content
 * @property string $tags
 * @property string $tagline
 * @property int $created_by
 * @property string $created_time
 * @property string $update_time
 * @property int $updated_by
 * @property string $is_published
 */
class ContentBlog extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'content_blog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['header_image_url', 'author', 'blog_description', 'content_title', 'blog_content', 'tags', 'tagline', 'created_by', 'created_time'], 'required'],
            [['content_title'], 'unique', 'targetAttribute' => ['content_title']],
            [['tagline'], 'unique', 'targetAttribute' => ['tagline']],
            [['blog_content'], 'string'],
            [['created_by', 'updated_by'], 'integer'],
            [['created_time', 'update_time'], 'safe'],
            [['header_image_url', 'blog_description'], 'string', 'max' => 500],
            [['author'], 'string', 'max' => 50],
            [['content_title', 'tags'], 'string', 'max' => 200],
            [['tagline'], 'string', 'max' => 100],
            [['is_published'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'header_image_url' => 'Header Image Url',
            'content_title' => 'Content Title',
            'blog_content' => 'Blog Content',
            'blog_description' => 'Description',
            'author' => 'Author',
            'tags' => 'Tags',
            'tagline' => 'Tagline',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'update_time' => 'Update Time',
            'updated_by' => 'Updated By',
            'is_published' => 'Is Published',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\content\query\ContentBlogQuery the active query used by this AR class.
     */
    public static function find() {
        return new \app\models\content\query\ContentBlogQuery(get_called_class());
    }

}
