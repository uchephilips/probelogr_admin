<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logsym".
 *
 * @property int $id
 * @property string $userid
 * @property string $projectid
 * @property string $appid
 * @property string $tags
 * @property string $ym
 * @property int $log_count
 * @property string $last_synced
 */
class Logsym extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logsym';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userid', 'projectid', 'appid', 'tags', 'ym', 'log_count', 'last_synced'], 'required'],
            [['log_count'], 'integer'],
            [['userid', 'projectid', 'appid','last_synced'], 'safe'],
            [[ 'tags', 'ym'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userid' => 'Userid',
            'projectid' => 'Projectid',
            'appid' => 'Appid',
            'tags' => 'Tags',
            'ym' => 'Ym',
            'log_count' => 'Log Count',
            'last_synced' => 'Last Synced',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\LogsymQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\LogsymQuery(get_called_class());
    }
}
