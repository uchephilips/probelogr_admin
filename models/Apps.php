<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "apps".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $color_code
 * @property int $project_id
 * @property int $created_by
 * @property string $created_time
 * @property string $update_time
 * @property int $updated_by
 * @property bool $is_active
 * @property bool $is_suspended
 */
class Apps extends \yii\db\ActiveRecord {

    public $project_name;
    public $has_settings;
    public $has_tags;
    
    public $shared_by;
    public $team_name;
    public $invite_status;
    public $role;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'apps';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['name', 'created_by', 'created_time', 'color_code'], 'required'],
            [['project_id', 'created_by', 'updated_by'], 'integer'],
            [['created_time', 'update_time'], 'safe'],
            [['is_active', 'is_suspended'], 'boolean'],
            [['name'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'project_id' => 'Project',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'update_time' => 'Update Time',
            'updated_by' => 'Updated By',
            'is_active' => 'Is Active',
            'is_suspended' => 'Is Suspended',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\AppsQuery the active query used by this AR class.
     */
    public static function find() {
        return new \app\models\queries\AppsQuery(get_called_class());
    }

    public static function getUniqueColor($userId) {
        $color = \app\assets\Misc::rand_color();
        $findOne = Apps::findOne(['created_by' => $userId, 'color_code' => $color]);
        if (isset($findOne)) {
            return $this->getUniqueColor($userId);
        }
        return $color;
    }



}
