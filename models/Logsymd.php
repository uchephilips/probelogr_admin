<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logsymd".
 *
 * @property int $id
 * @property string $userid
 * @property string $projectid
 * @property string $appid
 * @property string $tags
 * @property string $ym
 * @property string $ymd
 * @property int $log_count
 * @property string $last_synced
 */
class Logsymd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logsymd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userid', 'projectid', 'appid', 'tags', 'ym', 'ymd'], 'required'],
            [['ymd', 'last_synced'], 'safe'],
            [['userid', 'projectid', 'appid','log_count'], 'integer'],
            [[ 'tags', 'ym'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userid' => 'Userid',
            'projectid' => 'Projectid',
            'appid' => 'Appid',
            'tags' => 'Tags',
            'ym' => 'Ym',
            'ymd' => 'Ymd',
            'log_count' => 'Log Count',
            'last_synced' => 'Last Synced',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\LogsymdQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\LogsymdQuery(get_called_class());
    }
}
