<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "roles".
 *
 * @property int $id
 * @property string $role_name
 * @property int $created_by
 * @property string $created_time
 * @property string $update_time
 * @property int $updated_by
 * @property  $tenant_id
 * @property bool $is_active
 * @property bool $is_editable
 */
class Roles extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'roles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['role_name', 'created_by', 'created_time'], 'required'],
            [['created_by', 'updated_by'], 'integer'],
            [['created_time', 'update_time'], 'safe'],
            [['is_active'], 'boolean'],
            [['role_name'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'role_name' => 'Role Name',
            'created_by' => 'Created By',
            'created_time' => 'Created Time',
            'update_time' => 'Update Time',
            'updated_by' => 'Updated By',
            'tenant_id' => 'Tenant ID',
            'is_active' => 'Is Active',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\queries\RolesQuery the active query used by this AR class.
     */
    public static function find() {
        return new \app\models\queries\RolesQuery(get_called_class());
    }

    public function getCreatorName() {
        $u = AppUsers::findOne(['id' => $this->created_by]);

        if (isset($u)) {
            return $u->first_name . ' ' . $u->last_name;
        }
        return "not set";
    }

    /**
     * @return PageActions[] returns page_actions with the role_id
     */
    public function getPageActions() {
        return PageActions::findOne(['role_id' => $this->id]);
    }
    
    public static function searchRolesAsSelectOption($search) {
        return (\app\models\Roles::find()
                        ->andFilterWhere(['like', 'role_name', $search])->select(['id','role_name text'])->asArray()->all());
    }

}
