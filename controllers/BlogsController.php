<?php

namespace app\controllers;

use Yii;
use app\models\content\ContentBlog;
use app\models\search\content\ContentBlogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class BlogsController extends \yii\web\Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        $this->layout = 'anada_layout';
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new ContentBlogSearch();
        $searchModel->is_published = "YES";
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'models' => $dataProvider->getModels(),
                    'pages' => $dataProvider->pagination,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($tagline) {
        return $this->render('view', [
                    'model' => $this->findModelByTagline($tagline),
        ]);
    }

    
    /**
     * Finds the ContentBlog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ContentBlog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelByTagline($tagline) {
        if (($model = ContentBlog::findOne(['tagline' => $tagline])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
}
