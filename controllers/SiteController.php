<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['admin'],
                'rules' => [
                    [
                        'actions' => ['admin'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \app\assets\RoleManagement::hasPrivilege($action);
                            //return true;
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'login_layout'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        $this->layout = 'anada_layout';


        $contentVideos = \app\models\content\ContentVideo::findBySql("select * from content_video order by id desc limit 3")->all();
        $contentBlogs = \app\models\content\ContentBlog::findBySql("select * from content_blog order by id desc limit 3")->all();


        return $this->render('index', [
                    'contentVideos' => $contentVideos,
                    'contentBlogs' => $contentBlogs
        ]);
    }

    public function actionAdmin() {


        return $this->render('admin');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        $this->layout = 'login_layout';
        if (!Yii::$app->user->isGuest) {
            //return $this->goHome();
            return $this->redirect(['dashboard/index']);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {

            if ($model->login()) {
                return $this->redirect(['dashboard/index']);
            }
        }

        $model->password = '';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }


    public function actionRegister() {
        $this->layout = 'login_layout';
        $model = new \app\models\AppUsers();

        if ($model->load(Yii::$app->request->post())) {
            $model->confirm_password = $_POST['AppUsers']['confirm_password'];

            if ($model->confirm_password != $model->password) {
                Yii::$app->session->setFlash('error', "Password doesn't match");
                return $this->render('register', [
                            'model' => $model,
                ]);
            }

            $unEncPacc = $model->password;
            $model->password = md5($model->password);
            $acce = uniqid();
            $model->access_token = md5($acce);
            $model->created_by = 0;
            $model->role_id = 2;
            $model->is_active = 1;
            $model->create_time = date("Y-m-d H:i:s");
            if ($model->save()) {

                //\app\assets\EmailAssets::createAccount($model->email);
                $lf = new LoginForm();
                $lf->username = $model->email;
                $lf->password = $unEncPacc;
                if ($lf->login()) {
                    Yii::$app->session->setFlash('info', "Welcome to Probelogr");

                    $result = \app\models\AppUsers::find()->addMemberToTeams($lf->username);

                    if ($result) {
                        \Yii::$app->session->addFlash("success", "You have a newly shared apps");
                    }

                    \app\assets\EmailAssets::createAccount($model->first_name . ' ' . $model->last_name, $model->email);
                    return $this->redirect(['project/index']);
                }
            } else {
                Yii::$app->session->setFlash('error', \app\assets\Misc::readableError($model->getErrorSummary(TRUE)));
                Yii::error($model->getErrorSummary(true));
            }
        }

        return $this->render('register', [
                    'model' => $model,
        ]);
    }

    public function actionContinueToAccount($login_token) {
        $this->layout = 'login_layout';
        $model = $this->findUserByLoginToken($login_token);

        if (isset($model)) {
            $model->login_token = "";
            $model->save();

            $lf = new LoginForm();

            if ($lf->loginContinue($login_token, $model)) {
                Yii::$app->session->setFlash('info', "Welcome to Probelogr");

                return $this->redirect(['dashboard/index']);
            }
        }

        return $this->render('reset_password', [
                    'model' => $model,
        ]);
    }

    public function actionChangePassword($login_token) {
        $this->layout = 'login_layout';
        $model = $this->findUserByLoginToken($login_token);
        //exit;
        if (isset($model)) {

            if ($model->load(Yii::$app->request->post())) {
                $model->password = md5($model->password);
                $model->login_token = "";
                $model->save();

                $lf = new LoginForm();

                if ($lf->loginContinue($login_token, $model)) {
                    Yii::$app->session->setFlash('info', "You password has been updated");

                    return $this->redirect(['dashboard/index']);
                }
            }
        }

        $model->email = '';
        return $this->render('change_password', [
                    'model' => $model,
        ]);
    }

    public function actionResetPassword() {

        $this->layout = 'login_layout';
        $model = new \app\models\AppUsers();

        if ($model->load(Yii::$app->request->post())) {

            $model = $this->findUser($model->email);

            $model->forget_password = 1;
            $model->login_token = \app\models\AppUsers::getUniqueLoginToken();
            $model->access_token = \app\models\AppUsers::getUniqueAccessToken();

            if (!$model->save()) {
                Yii::$app->session->setFlash('error', \app\assets\Misc::readableError($model->getErrorSummary(TRUE)));
                Yii::error($model->getErrorSummary(true));
            } else {
                Yii::$app->session->setFlash('success', "An email has been sent to your email to aid in accessing your account");
                \app\assets\EmailAssets::createResetPassword($model->email, $model->login_token);
                return $this->redirect(['login']);
            }
        }

        return $this->render('reset_password', [
                    'model' => $model,
        ]);
    }

    public function actionUpdatePassword() {
        $this->layout = 'login_layout';
        $model = new \app\models\AppUsers();
        $resetkey = \Yii::$app->request->get('resetkey');

        if ($model->load(Yii::$app->request->post())) {
            $model->confirm_password = $_POST['AppUsers']['confirm_password'];
            $resp = \app\assets\BoldServiceAsset::updatePassword($model, $resetkey);
            if (\app\assets\BoldServiceAsset::isSuccessfull($resp)) {

                $lf = new LoginForm();
                $lf->username = $model->email;
                $lf->password = $model->password;
                if ($lf->login()) {
                    return $this->goHome();
                }
            } else {
                $desc = \app\assets\BoldServiceAsset::getResponseDesc($resp) . '<br>';
                $desc .= \app\assets\BoldServiceAsset::getValidationErrorMsg($resp);
                Yii::$app->session->setFlash('error', $desc);
            }
        }

        return $this->render('update_password', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        $this->layout = 'main_frontend';

        $about = \app\models\CmsSupportBlog::findOne(['category' => 'ABOUT-BT']);


        return $this->render('about', ['about' => $about]);
    }

    public function beforeAction($action) {
        if (parent::beforeAction($action)) {
            // change layout for error action
            if ($action->id == 'error')
                $this->layout = 'main_frontend';
            return true;
        } else {
            return false;
        }
    }

    public function actionLgaSelect() {
        $stateId = \Yii::$app->request->get('stateId');
        $res = \app\models\Lga::find()->select(['id', 'name'])->where(['state_id' => $stateId])->asArray(true)->all();
        echo json_encode($res);
    }

    public function actionSetTheme($theme) {
        Yii::$app->session->set('quixlab_theme', $theme);
    }

    /**
     * 
     * @param type $id
     * @return \app\models\AppUsers
     */
    protected function findUser($email) {
        $model = \app\models\AppUsers::findOne(['email' => $email]);
        if (isset($model)) {
            return $model;
        }
        throw new NotFoundHttpException('Unknown user.');
    }

    /**
     * 
     * @param type $id
     * @return \app\models\AppUsers
     */
    protected function findUserByLoginToken($loginToken) {
        $model = \app\models\AppUsers::findOne(['login_token' => $loginToken]);

        if (isset($model)) {
            return $model;
        }
        throw new NotFoundHttpException('Unknown user.');
    }

}
