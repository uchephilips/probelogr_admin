<?php

namespace app\controllers;

use Yii;

class OpenFileController extends \yii\web\Controller {

    public function actionIndex($img, $dir) {
        $filename = $img;

        if (isset($filename)) {
            $file = '';
            if ($dir == 'logo') {
                $file = Yii::$app->params['boldt.file.businesslogo.dir'] . $filename;
            } else if ($dir == 'cms_imgs') {
                $file = Yii::$app->params['cms.imgs'] . $filename;
            }

            if (file_exists($file)) {
                $filename = basename($file);

                $mime_type = mime_content_type($file);

                header("Content-Type:$mime_type");
                echo readfile($file);
                exit;
            } else {
                echo "No attachment could be found";
            }
        } else {
            $ar = array('retn' => '76', 'desc' => 'attachement file did not load');
            echo json_encode($ar);
        }
        exit;
    }

    public function actionGetBase() {
        $filename = Yii::$app->request->get("img");
        $dir = Yii::$app->request->get("dir");
        
        header("Access-Control-Allow-Origin: *");
        if (isset($filename)) {
            $file = '';
            if ($dir == 'logo') {
                $file = Yii::$app->params['boldt.file.businesslogo.dir'] . $filename;
            } else if ($dir == 'cms_imgs') {
                $file = Yii::$app->params['cms.imgs'] . $filename;
            } else {
                $file = Yii::$app->params['boldt.file.idfile.dir'] . $filename;
            }

            if (file_exists($file) && !empty(trim($filename))) {
                $filename = basename($file);
                $mime_type = mime_content_type($file);
                



                $fileContent = file_get_contents($file);
                $imgDat = "data:$mime_type;base64," . base64_encode($fileContent);
                //echo "<img src='$imgDat'>";
                echo $imgDat;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
        exit;
    }

    public function actionGetImage() {
        $filename = Yii::$app->request->get("img");
        $dir = Yii::$app->request->get("dir");
        $attrib = Yii::$app->request->get("attrib");

        if (isset($filename)) {
            $file = '';
            if ($dir == 'logo') {
                $file = Yii::$app->params['boldt.file.businesslogo.dir'] . $filename;
            } else if ($dir == 'cms_imgs') {
                $file = Yii::$app->params['cms.imgs'] . $filename;
            } else {
                $file = Yii::$app->params['boldt.file.idfile.dir'] . $filename;
            }

            if (file_exists($file) && !empty(trim($filename))) {
                $filename = basename($file);
                $mime_type = mime_content_type($file);
                //header('Content-Type: ' . $mime_type);


                $fileContent = file_get_contents($file);
                $imgDat = "data:$mime_type;base64," . base64_encode($fileContent);
                echo "<img src='$imgDat' $attribs>";
                //echo $imgDat;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
        exit;
    }

}
