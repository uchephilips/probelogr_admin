<?php

namespace app\controllers;

use Yii;
use app\models\CmsSupportBlog;
use app\models\search\CmsSupportBlogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class FaqController extends \yii\web\Controller {

    public function actionIndex() {
        $this->layout = 'main_frontend';

        $searchModel = new CmsSupportBlogSearch();
        $searchModel->category = 'FAQ';
        $searchModel->publish = true;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'articles' => $dataProvider->getModels(),
                    'pages' => $dataProvider->pagination,
        ]);
    }

}
