<?php

namespace app\controllers;

use Yii;
use app\models\Apps;
use app\models\Project;
use app\models\search\AppsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AppsController implements the CRUD actions for Apps model.
 */
class AppsController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        $this->layout = 'main_frontend';
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'delete', 'view', 'update', 'logs', 'app-settings', 'appinfo', 'accessendpoint', 'applicationtags'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'delete', 'view', 'update', 'logs', 'app-settings', 'appinfo', 'accessendpoint', 'applicationtags'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \app\assets\RoleManagement::hasPrivilege($action);
                            //return true;
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delte' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Apps models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new AppsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $sharedAppCount = Apps::find()->findSharedAppCount(\app\assets\AppAsset::INVITE_STATUS_APPROVED());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'models' => $dataProvider->getModels(),
                    'pages' => $dataProvider->pagination,
                    'sharedAppCount' => $sharedAppCount,
        ]);
    }

    /**
     * Lists all Apps models.
     * @return mixed
     */
    public function actionSharedapps() {
        $searchModel = new AppsSearch();
        $dataProvider = $searchModel->searchShare(Yii::$app->request->queryParams);

        $sharedAppCount = Apps::find()->findSharedAppCount(\app\assets\AppAsset::INVITE_STATUS_APPROVED());

        return $this->render('shared_apps', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'models' => $dataProvider->getModels(),
                    'pages' => $dataProvider->pagination,
                    'sharedAppCount' => $sharedAppCount,
        ]);
    }

    /**
     * Displays a single Apps model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => Apps::find()->findModel($id),
        ]);
    }

    /**
     * Creates a new Apps model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Apps();
        $userId = Yii::$app->user->identity->appUserId;
        if ($model->load(Yii::$app->request->post())) {


            $model->created_by = $userId;
            $model->created_time = date("Y-m-d H:i:s");
            $model->color_code = \app\models\Apps::getUniqueColor($userId);
            if ($model->save()) {
                if ($_POST['form_type'] == '<< Save Back') {
                    Yii::$app->session->setFlash('success', "App Added");
                    return $this->redirect(['index']);
                } else {
                    Yii::$app->session->setFlash('success', "App Added, You need to set the Access Key");
                    return $this->redirect(['accessendpoint?app=' . $model->id]);
                }
            } else {
                Yii::$app->session->setFlash('error', \app\assets\Misc::readableError($model->getErrorSummary(TRUE)));
                Yii::error($model->getErrorSummary(true));
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    public function actionAccessendpoint($app) {
        $model = Apps::find()->findModel($app);
        $endpoint = \app\models\AppsEndpoints::find()->findAppEndpointModel($app);

        if (\Yii::$app->request->isPost) {
            $accessKey = \Yii::$app->request->post('access_key');

            $endpoint->app_id = $app;
            $endpoint->access_key = $accessKey;
            if ($endpoint->isNewRecord) {
                $endpoint->created_time = date("Y-m-d H:i:s");
                $endpoint->created_by = Yii::$app->user->identity->appUserId;
            } else {
                $endpoint->update_time = date("Y-m-d H:i:s");
                $endpoint->updated_by = Yii::$app->user->identity->appUserId;
            }

            if ($endpoint->save()) {

                if ($_POST['form_type'] == '<< Save Back') {
                    Yii::$app->session->setFlash('success', "App Added");
                    return $this->redirect(['index']);
                } else {
                    Yii::$app->session->setFlash('success', "App Added, You need to set the Access Key");
                    return $this->redirect(['applicationtags?app=' . $model->id]);
                }
            } else {
                Yii::$app->session->setFlash('error', \app\assets\Misc::readableError($endpoint->getErrorSummary(TRUE)));
                Yii::error($model->getErrorSummary(true));
            }
        }

        return $this->render('accessendpoint', [
                    'model' => $model,
                    'endpoint' => $endpoint,
        ]);
    }

    public function actionApplicationtags($app) {
        $model = Apps::find()->findModel($app);
        $endpoint = \app\models\AppsEndpoints::find()->findAppEndpointModel($app);


        if (\Yii::$app->request->isGet) {
            $deleteId = \Yii::$app->request->get('delete_id');
            if (isset($deleteId)) {
                $del = \app\models\AppsTags::find()->findAppTagModel($deleteId);
                $anyLog = \app\models\Logsym::findOne(['appid' => $deleteId]);
                if (!isset($anyLog)) {
                    Yii::$app->session->setFlash('success', "You have deleted tag (" . $del->tags . ")");
                    $del->delete();
                } else {
                    Yii::$app->session->setFlash('info', "You can't delete a tag that already has logs in it");
                }
            }
        }

        if (\Yii::$app->request->isPost) {

            if ($_POST['form_type'] == 'Add Tag') {
                $log_type = \Yii::$app->request->post('log_type');
                $has_email_trigger = \Yii::$app->request->post('has_email_trigger');
                $tags = \Yii::$app->request->post('tags');
                $tags = preg_replace('/\s+/', ' ', $tags);
                $tags = strtoupper($tags);

                $appTag = new \app\models\AppsTags();

                $appTag->app_id = $app;
                $appTag->log_type = $log_type;
                $appTag->description = $tags;
                $appTag->color_code = \app\models\AppsTags::getUniqueColor(Yii::$app->user->identity->appUserId);
                $appTag->has_email_trigger = $has_email_trigger;
                $appTag->tags = $tags;
                $appTag->created_time = date("Y-m-d H:i:s");
                $appTag->created_by = Yii::$app->user->identity->appUserId;
                $appTag->is_active = 1;
                $appTag->is_suspended = 0;
                if (!$appTag->save()) {
                    Yii::$app->session->setFlash('error', \app\assets\Misc::readableError($appTag->getErrorSummary(TRUE)));
                    Yii::error($appTag->getErrorSummary(true));
                }
            } else if ($_POST['form_type'] == '<< Save Back') {
                return $this->redirect(['accessendpoint?app=' . $model->id]);
            } else {
                $del = \app\models\AppsTags::find()->findAppTagsModels($app);
                if (count($del) > 0) {

                    \app\assets\AppSettingAsset::pushConfig($endpoint->access_key);

                    Yii::$app->session->setFlash('success', "App setup completed");
                } else {
                    Yii::$app->session->setFlash('info', "App Setup is not completed");
                }
                return $this->redirect(['index']);
            }
        }

        $tagsModels = \app\models\AppsTags::find()->findAppTagsModels($app);

        return $this->render('applicationtags', [
                    'model' => $model,
                    'tagsModels' => $tagsModels,
        ]);
    }

    public function actionAppinfo($app) {

        if (isset($_GET['shared'])) {
            $createdBy = \app\models\AppUsers::find()->findTeamCreatorIdByAppId($app);


            $model = Apps::find()->findModelShared($app, $createdBy);
            
            
            
            $tagsModels = \app\models\AppsTags::find()->findAppTagsModelsShared($app, $createdBy);
            $endpoint = \app\models\AppsEndpoints::find()->findAppEndpointModelShared($app, $createdBy);
        } else {
            $model = Apps::find()->findModel($app);
            $tagsModels = \app\models\AppsTags::find()->findAppTagsModels($app);
            $endpoint = \app\models\AppsEndpoints::find()->findAppEndpointModel($app);
            $teams = \app\models\TeamApp::find()->findListByAppId($app);
        }



        return $this->render('appinfo', [
                    'tagsModels' => $tagsModels,
                    'endpoint' => $endpoint,
                    'model' => $model,
                    'shared' => isset($_GET['shared']),
                    'teams' => isset($teams) ? $teams : null,
        ]);
    }

    public function actionShare($app) {
        $model = Apps::find()->findModel($app);
        $endpoint = \app\models\AppsEndpoints::find()->findAppEndpointModel($app);

        $isPost = \Yii::$app->request->isPost;
        $shareEmail = \Yii::$app->request->post('share_email', '');
        $shareEmail = trim($shareEmail);
        if (isset($isPost)) {
            if ($shareEmail != '') {
                \app\assets\AppSettingAsset::shareConfig($endpoint->access_key, $shareEmail);
                Yii::$app->session->setFlash('info', "App settings has been shared");
            } else {
                Yii::$app->session->setFlash('error', "Email is needed");
            }
        }

        return $this->redirect(['appinfo?app=' . $model->id]);
    }

    /**
     * Updates an existing Apps model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = Apps::find()->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->updated_by = Yii::$app->user->identity->appUserId;
            $model->update_time = date("Y-m-d H:i:s");
            if ($model->save()) {
                if ($_POST['form_type'] == '<< Save & Go Back') {
                    Yii::$app->session->setFlash('success', "App Updated");
                    return $this->redirect(['index']);
                } else {
                    Yii::$app->session->setFlash('success', "App Added, You need to set the Access Key");
                    return $this->redirect(['accessendpoint?app=' . $model->id]);
                }
            } else {
                Yii::$app->session->setFlash('error', \app\assets\Misc::readableError($model->getErrorSummary(TRUE)));
                Yii::error($model->getErrorSummary(true));
            }
        }

        $project = Project::find()->findProjectModel($model->project_id);

        return $this->render('update', [
                    'model' => $model,
                    'project' => $project,
        ]);
    }

    public function actionLogs($id) {


        if (isset($_GET['shared'])) {
            $createdBy = \app\models\AppUsers::find()->findTeamCreatorIdByAppId($id);
            $model = Apps::find()->findModelShared($id, $createdBy);
            $endpoint = \app\models\AppsEndpoints::find()->findAppEndpointModelShared($id, $createdBy);
        } else {
            $model = Apps::find()->findModel($id);
            $endpoint = \app\models\AppsEndpoints::find()->findAppEndpointModel($id);
        }

        if ($endpoint->isNewRecord) {
            Yii::$app->session->setFlash('info', "unable to access app settings");
            return $this->redirect(['index']);
        } else {
            return $this->render('logs', [
                        'endpoint' => $endpoint,
                        'model' => $model,
                        'shared' => isset($_GET['shared']),
            ]);
        }
        Yii::$app->session->setFlash('info', "Unable to view log at this moment");
        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing Apps model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        Apps::find()->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionGenAccessKey() {
        $result = \app\models\AppsEndpoints::genAccessKey();
        echo $result;
        exit;
    }

    /**
     * Finds the Apps model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Apps the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        $model = Apps::find()->addSelect('apps.*')
                        ->addSelect(['project_name' => '(SELECT p.name FROM project p WHERE apps.project_id = p.id)'])
                        ->andOnCondition(['id' => $id, 'created_by' => Yii::$app->user->identity->appUserId])->one();
        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * 
     * @param type $id
     * @return \app\models\AppsEndpoints
     */
    protected function findAppEndpointModel($id) {
        $endpoint = \app\models\AppsEndpoints::findOne(['app_id' => $id, 'created_by' => Yii::$app->user->identity->appUserId]);
        if (isset($endpoint)) {
            return $endpoint;
        }
        return new \app\models\AppsEndpoints();
    }

    public function actionAppsAsSelectOption() {
        $term = \Yii::$app->request->get('term');
        $result = \app\models\search\AppsSearch::searchAppAsSelectOption($term);
        echo json_encode($result, false);
        header('Content-type: application/json');
        exit;
    }

    public function actionAppTagsAsSelectOption() {
        $term = \Yii::$app->request->get('term');
        $result = \app\models\search\AppsTagsSearch::searchAppTagsAsSelectOption($term);
        echo json_encode($result, false);
        header('Content-type: application/json');
        exit;
    }

}
