<?php

namespace app\controllers;

use Yii;
use app\models\Apps;
use app\models\search\AppsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class DashboardController extends \yii\web\Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        $this->layout = 'main_frontend';
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'shared-dashboard'],
                'rules' => [
                    [
                        'actions' => ['index', 'shared-dashboard'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \app\assets\RoleManagement::hasPrivilege($action);
                            //return true;
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delte' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex() {


        \app\assets\LoadStatAsset::loadUnsyncedLogs();


        $userId = Yii::$app->user->identity->appUserId;
        $totalProjects = \app\models\Project::find()->getTotal($userId);
        $totalApps = \app\models\Apps::find()->getTotal($userId);
        $totalTags = \app\models\AppsTags::find()->getTotal($userId);

        $searchModel = new AppsSearch();
        $searchModel->is_active = 1;
        $searchModel->is_suspended = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        $sharedAppCount = Apps::find()->findSharedAppCount(\app\assets\AppAsset::INVITE_STATUS_APPROVED());

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'models' => $dataProvider->getModels(),
                    'pages' => $dataProvider->pagination,
                    'totalApps' => $totalApps,
                    'totalProjects' => $totalProjects,
                    'sharedAppCount' => $sharedAppCount,
                    'totalTags' => $totalTags]);
    }

    public function actionSharedDashboard() {

        \app\assets\LoadStatAsset::loadTeamUnsyncedLogs();


        $searchModel = new AppsSearch();
        $searchModel->is_active = 1;
        $searchModel->is_suspended = 0;
        $dataProvider = $searchModel->searchShare(Yii::$app->request->queryParams);



        return $this->render('index_shared', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'models' => $dataProvider->getModels(),
                    'pages' => $dataProvider->pagination]);
    }

    public function actionAppMonthlyReport($appId) {

        $y = date('Y');
        $response = Yii::$app->db->createCommand('SELECT SUM(l.log_count) data,l.ym label FROM logsym l
	WHERE l.appid = :appid AND l.userid = :userid and l.ym like "' . $y . '%"
	GROUP BY	l.ym', [':userid' => AppsSearch::getTeamLeadId($appId), 'appid' => $appId])->queryAll();
        echo json_encode($response, false);
        header('Content-type: application/json');
        exit;
    }

    public function actionAppTagMonthlyReport($appId) {
        $month = date('Y') . '-' . date('m');
        $response = Yii::$app->db->createCommand('SELECT SUM(l.log_count) data,l.tags label FROM logsym l
	WHERE l.appid = :appid AND l.userid = :userid and l.ym = :ym
	GROUP BY	l.tags', [':userid' => AppsSearch::getTeamLeadId($appId), 'appid' => $appId, 'ym' => $month])->queryAll();
        echo json_encode($response, false);
        header('Content-type: application/json');
        exit;
    }

    public function actionAppTagCount($appId) {
        $ym = date('Y-m');
        $response = $this->tagsCount($ym, $appId);
        echo json_encode($response, false);
        header('Content-type: application/json');
        exit;
    }

    public function actionAppStatInfo($appId) {
//        SELECT sum(l.log_count)/5,l.ym label FROM logsym l
//	WHERE l.appid = 4 AND l.userid = 8 AND l.ym LIKE "2020-%";

        $y = date('Y');
        $m = date('m');


        $month = date('Y') . '-' . date('m');
        $lastMonth = date("Y-m", strtotime("first day of previous month"));


        $lastMonthSum = $this->getMon("ifnull(SUM(l.log_count),0)", $lastMonth, $appId);

        $lastMonthcount = $this->getMon("count(*)", $lastMonth, $appId);

        $monthSum = $this->getMon("ifnull(SUM(l.log_count),0)", $month, $appId);

        $monthcount = $this->getMon("count(*)", $month, $appId);

        $lastMonAvg = ($lastMonthSum != 0) ? $lastMonthSum / $lastMonthcount : 0;
        $monAvg = round(($monthSum != 0) ? $monthSum / $monthcount : 0);

        $avgDiff = round(($lastMonAvg - $monAvg));
        //7 - 10 = -3;
        $trend = ($avgDiff > 0) ? "-" . $avgDiff : ($avgDiff * -1);


        $highestTags = $this->topTags('ifnull(l.log_count,0) ma', $month, $appId, 'desc');
        $lowestTags = $this->topTags('ifnull(l.log_count,0) mi', $month, $appId, 'asc');

        $response = [
            'y' => $y,
            'lastMonthSum' => $lastMonthSum,
            'lastMonthcount' => $lastMonthcount,
            'monthSum' => $monthSum,
            'monthcount' => $monthcount,
            'monAvg' => $monAvg,
            'lastMonAvg' => $lastMonAvg,
            'highestTags' => $highestTags,
            'lowestTags' => $lowestTags,
            'trend' => $trend,
        ];


        echo json_encode($response, false);
        header('Content-type: application/json');
        exit;
    }

    public function getMon($select, $month, $appId) {
        $res = Yii::$app->db->createCommand('SELECT ' . $select . ' data FROM logsymd l WHERE l.appid = :appid AND l.userid = :userid and l.ymd like "' . $month . '%"',
                        [':userid' => AppsSearch::getTeamLeadId($appId), 'appid' => $appId])->queryScalar();
        return $res;
    }

    public function topTags($select, $ym, $appId, $order) {
        $res = Yii::$app->db->createCommand('select ' . $select . ',ifnull(l.tags,"not activity") tags FROM logsym l
	WHERE l.appid = :appid AND l.userid = :userid and l.ym = "' . $ym . '"
	ORDER BY l.log_count ' . $order . ' LIMIT 1', [':userid' => AppsSearch::getTeamLeadId($appId), ':appid' => $appId])->queryOne();
        return $res;
    }

    public function tagsCount($ym, $appId) {

        $res = Yii::$app->db->createCommand("SELECT l.tags label,l.log_count data,l.ym FROM"
                        . " logsym l WHERE l.ym = :ym and l.appid = :appid AND l.userid = :userid",
                        [':userid' => AppsSearch::getTeamLeadId($appId),
                            ':appid' => $appId, ':ym' => $ym])->queryAll();
        return $res;
    }

}
