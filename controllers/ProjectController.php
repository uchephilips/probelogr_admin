<?php

namespace app\controllers;

use Yii;
use app\models\Project;
use app\models\search\ProjectSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProjectController implements the CRUD actions for Project model.
 */
class ProjectController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        $this->layout = 'main_frontend';
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'delete', 'view', 'update', 'projects-as-aelect-option'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'delete', 'update', 'view', 'projects-as-aelect-option'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \app\assets\RoleManagement::hasPrivilege($action);
                            //return true;
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delte' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex() {
        
      
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'models' => $dataProvider->getModels(),
                    'pages' => $dataProvider->pagination,
        ]);
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Project();

        if ($model->load(Yii::$app->request->post())) {

            $model->created_by = Yii::$app->user->identity->appUserId;
            $model->created_time = date("Y-m-d H:i:s");

            $isUnique = Project::find()->isUniqueProjectName($model->name, $model->created_by);

            if ($isUnique) {
                if ($model->save()) {
                    return $this->redirect(['index']);
                } else {
                    Yii::$app->session->setFlash('error', \app\assets\Misc::readableError($model->getErrorSummary(TRUE)));
                    Yii::error($model->getErrorSummary(true));
                }
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionSetup() {
        $projectId = \Yii::$app->request->post('project_id', 0);
        $project = Project::find()->get($projectId, 1);

        $currentForm = \Yii::$app->request->post('current_form', 'project');

        if (\Yii::$app->request->isPost) {


            if ($currentForm == 'project') {

                if ($project->load(Yii::$app->request->post())) {

                    $project->created_by = Yii::$app->user->identity->appUserId;
                    $project->created_time = date("Y-m-d H:i:s");

                    $isUnique = Project::find()->isUniqueProjectName($project->name, 1);

                    if ($isUnique) {
                        if ($project->save()) {
                            Yii::$app->session->setFlash('success', "Project Added");
                            $currentForm = "app";
                        } else {
                            Yii::$app->session->setFlash('error', \app\assets\Misc::readableError($project->getErrorSummary(TRUE)));
                            Yii::error($project->getErrorSummary(true));
                        }
                    } else {
                        Yii::$app->session->setFlash('error', "Project name already exists");
                    }
                }
            } else if ($currentForm == 'app') {
                
            }
        }
        echo $currentForm;
        exit;
        return $this->render('setup', [
                    'project' => $project,
                    'currentForm' => $currentForm,
        ]);
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->updated_by = Yii::$app->user->identity->appUserId;
            $model->update_time = date("Y-m-d H:i:s");
            if ($model->save()) {
                Yii::$app->session->setFlash('success', "Project Updated");
                return $this->redirect(['index']);
            } else {
                Yii::$app->session->setFlash('error', \app\assets\Misc::readableError($model->getErrorSummary(TRUE)));
                Yii::error($model->getErrorSummary(true));
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {

        $app = \app\models\Apps::findOne(['project_id' => $id]);
        if (!isset($app)) {
            $this->findModel($id)->delete();
            Yii::$app->session->setFlash('success', "Project Deleted");
        } else {
            Yii::$app->session->setFlash('error', "You need to delete apps for the project first");
        }


        return $this->redirect(['index']);
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.s
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Project::findOne(['id' => $id, 'created_by' => Yii::$app->user->identity->appUserId])) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionProjectsAsSelectOption() {
        $term = \Yii::$app->request->get('term');
        $result = ProjectSearch::searchProjectAsSelectOption($term);
        echo json_encode($result, false);
        header('Content-type: application/json');
        exit;
    }

}
