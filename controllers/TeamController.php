<?php

namespace app\controllers;

use Yii;
use app\models\Team;
use app\models\search\TeamSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TeamController implements the CRUD actions for Team model.
 */
class TeamController extends Controller {

    public function behaviors() {
        $this->layout = 'main_frontend';
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'delete', 'update', 'view', 'teammembers', 'teamapps'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \app\assets\RoleManagement::hasPrivilege($action);
                            //return true;
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delte' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Team models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new TeamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'models' => $dataProvider->getModels(),
                    'pages' => $dataProvider->pagination,
        ]);
    }

    /**
     * Displays a single Team model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Team model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Team();

        if ($model->load(Yii::$app->request->post())) {

            $model->created_by = Yii::$app->user->identity->appUserId;
            $model->created_time = date("Y-m-d H:i:s");
            $model->token = Team::getUniqueToken();

            $isUnique = Team::find()->isUniqueTeamName($model->team_name, $model->created_by);

            if ($isUnique) {
                if ($model->save()) {
                    if ($_POST['form_type'] == '<< Save Back') {
                        Yii::$app->session->setFlash('success', "Team Created");
                        return $this->redirect(['index']);
                    } else {
                        Yii::$app->session->setFlash('success', "App Created, You can now add members");
                        return $this->redirect(['teammembers?team=' . $model->id]);
                    }
                } else {
                    Yii::$app->session->setFlash('error', \app\assets\Misc::readableError($model->getErrorSummary(TRUE)));
                    Yii::error($model->getErrorSummary(true));
                }
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    public function actionTeammembers($team) {
        $model = $this->findModel($team);


        if (\Yii::$app->request->isGet) {
            $deleteId = \Yii::$app->request->get('delete_id');
            if (isset($deleteId)) {
                $member = \app\models\TeamMembers::find()->findModelTeamMember($deleteId);
                if (isset($member)) {
                    Yii::$app->session->setFlash('success', "You have removed  (" . $member->email . ") as a member");
                    $member->delete();
                    return $this->redirect(['teammembers?team=' . $model->id]);
                }
            }
        }

        if (\Yii::$app->request->isPost) {

            if ($_POST['form_type'] == 'Add Member') {
                $email = \Yii::$app->request->post('email');
                $role = \Yii::$app->request->post('role');
                $hasDup = \app\models\TeamMembers::find()->hasDuplicate($team, $email);
                if (!$hasDup) {

                    $teamMember = new \app\models\TeamMembers();

                    $teamMember->team_id = $team;
                    $teamMember->email = $email;
                    $teamMember->user_id = \app\models\AppUsers::find()->findIdByEmail($email);
                    $teamMember->invite_status = $teamMember->user_id > 0 ? \app\assets\AppAsset::INVITE_STATUS_JOINED() : \app\assets\AppAsset::INVITE_STATUS_PENDING();
                    $teamMember->role = $role;
                    $teamMember->created_time = date("Y-m-d H:i:s");
                    $teamMember->created_by = Yii::$app->user->identity->appUserId;
                    $teamMember->is_active = 1;
                    $teamMember->is_suspended = 0;
                    if (!$teamMember->save()) {
                        Yii::$app->session->setFlash('error', \app\assets\Misc::readableError($teamMember->getErrorSummary(TRUE)));
                        Yii::error($teamMember->getErrorSummary(true));
                    } else {
                        \app\assets\EmailAssets::sendTeamInvite($email, $model->token);
                    }
                } else {
                    Yii::$app->session->setFlash('error', "Email already exists in team");
                }
            } else if ($_POST['form_type'] == '<< Save Back') {
                return $this->redirect(['update?id=' . $model->id]);
            } else if ($_POST['form_type'] == 'Add Apps >>') {

                $del = \app\models\TeamMembers::find()->findModelTeamMembers($team);
                if (count($del) > 0) {
                    Yii::$app->session->setFlash('success', "Team Members Added");
                }
                return $this->redirect(['teamapps?team=' . $team]);
            } else {
                
            }
        }

        $memberModels = \app\models\TeamMembers::find()->findModelTeamMembers($team);

        return $this->render('teammembers', [
                    'model' => $model,
                    'memberModels' => $memberModels,
        ]);
    }

    public function actionTeamapps($team) {
        $model = $this->findModel($team);


        if (\Yii::$app->request->isGet) {
            $deleteId = \Yii::$app->request->get('delete_id');
            if (isset($deleteId)) {
                $teamApp = \app\models\TeamApp::find()->findModelTeamApp($deleteId);
                if (isset($teamApp)) {
                    Yii::$app->session->setFlash('success', "You have removed app from team");
                    $teamApp->delete();
                    return $this->redirect(['teamapps?team=' . $model->id]);
                }
            }
        }

        if (\Yii::$app->request->isPost) {

            if ($_POST['form_type'] == 'Add App') {

                $appId = \Yii::$app->request->post('app_id');
                $tgs = \Yii::$app->request->post('app_tags');


                $hasDuplicate = \app\models\TeamApp::find()->hasDuplicate($team, $appId);

                if (!$hasDuplicate) {

                    $teamApp = new \app\models\TeamApp();

                    $teamApp->team_id = $team;
                    $teamApp->app_id = $appId;
                    $teamApp->tags = implode(',',$tgs);
                    $teamApp->created_time = date("Y-m-d H:i:s");
                    $teamApp->created_by = Yii::$app->user->identity->appUserId;
                    $teamApp->is_active = 1;
                    $teamApp->is_suspended = 0;
                    if (!$teamApp->save()) {
                        Yii::$app->session->setFlash('error', \app\assets\Misc::readableError($teamApp->getErrorSummary(TRUE)));
                        Yii::error($teamApp->getErrorSummary(true));
                    }
                    Yii::$app->session->setFlash('success', "App Addedd");
                } else {
                    Yii::$app->session->setFlash('error', "App already exists in team");
                }
            } else if ($_POST['form_type'] == '<< Back') {
                return $this->redirect(['teammembers?team=' . $model->id]);
            } else if ($_POST['form_type'] == 'Complete') {

                $del = \app\models\TeamApp::find()->findModelTeamApps($team);
                if (count($del) > 0) {
                    Yii::$app->session->setFlash('success', "Team setup is completed");
                } else {
                    Yii::$app->session->setFlash('info', "Team setup is not completed");
                }
                return $this->redirect(['index']);
            } else {
                
            }
        }

        $teamAppModels = \app\models\TeamApp::find()->findListByTeam($team);

        return $this->render('teamapps', [
                    'model' => $model,
                    'teamAppModels' => $teamAppModels,
        ]);
    }

    /**
     * Updates an existing Team model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->updated_by = Yii::$app->user->identity->appUserId;
            $model->update_time = date("Y-m-d H:i:s");
            if ($model->save()) {
                if ($_POST['form_type'] == '<< Save & Go Back') {
                    Yii::$app->session->setFlash('success', "Team Updated");
                    return $this->redirect(['index']);
                } else {
                    Yii::$app->session->setFlash('success', "Team Updated, You need to add Members");
                    return $this->redirect(['teammembers?team=' . $model->id]);
                }
            } else {
                Yii::$app->session->setFlash('error', \app\assets\Misc::readableError($model->getErrorSummary(TRUE)));
                Yii::error($model->getErrorSummary(true));
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Team model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);

        $mems = $this->findModelTeamMembers($id);
        if (isset($mems)) {
            Yii::$app->session->setFlash('error', "You can't delete a team that contains members");
        } else {
            Yii::$app->session->setFlash('success', "Team Deleted");
            $model->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Team model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Team the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Team::findOne(['id' => $id, 'created_by' => Yii::$app->user->identity->appUserId])) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
