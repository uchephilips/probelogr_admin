<?php

namespace app\controllers;

use Yii;
use app\models\Apps;
use app\models\plt\Avgpageloadtimeym;
use app\models\search\AppsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class AppDashboardController extends \yii\web\Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        $this->layout = 'main_frontend';
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \app\assets\RoleManagement::hasPrivilege($action);
                            //return true;
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delte' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex() {

        $app = Yii::$app->request->get('app', '0');

        if (isset($_GET['shared'])) {
            $createdBy = \app\models\AppUsers::find()->findTeamCreatorIdByAppId($app);
            
            $model = Apps::find()->findModelShared($app, $userId);
        } else {
            $userId = \Yii::$app->user->identity->appUserId;
            $model = Apps::find()->findModelShared($app, $userId);
        }



        // \app\assets\PageLoadStatAsset::loadUnsyncedLogs($model);
        //\app\assets\LoadArrgStatAsset::loadUnsyncedLogs($model);

        $ymNow = date('Y-m');
        $ym = Yii::$app->request->get('ym', $ymNow);


        $thisMonthTotalLog = $this->totalLogsPerMonth($ym, $app);
        $thisMonthTotalWeb = $this->totalWebPerMonth($ym, $app);
        $pageVisit = $this->pageVisit($ym, $app, 10);
        $pageVisitSum = $this->pageVisitSum($ym, $app, 10);
        $pageLoadTime = $this->pageLoadTime($ym, $app, 10);

        $httpSuccess = $this->httpsStatusYmPercent($ym, $app, 200, 299);
        $httpClient = $this->httpsStatusYmPercent($ym, $app, 400, 499);
        $httpServer = $this->httpsStatusYmPercent($ym, $app, 500, 599);


        $logGeneral = $this->logsYmPercent($ym, $app, 'GENERAL');
        $logError = $this->logsYmPercent($ym, $app, 'ERROR');
        $logSuccess = $this->logsYmPercent($ym, $app, 'SUCCESS');

        $yms = \app\assets\LogAsset::getYms($app);

        return $this->render('index',
                        [
                            'model' => $model,
                            'yms' => $yms,
                            'thisMonthTotalLog' => $thisMonthTotalLog,
                            'thisMonthTotalWeb' => $thisMonthTotalWeb,
                            'pageVisit' => $pageVisit,
                            'pageScoreSum' => $pageVisitSum,
                            'pageLoadTime' => $pageLoadTime,
                            'httpSuccess' => $httpSuccess,
                            'httpClient' => $httpClient,
                            'httpServer' => $httpServer,
                            'logGeneral' => $logGeneral,
                            'logError' => $logError,
                            'logSuccess' => $logSuccess,
                        ]
        );
    }

    public function actionPageloadtime() {

        $app = Yii::$app->request->get('app', '0');

        if (isset($_GET['shared'])) {
            $userId = \app\models\AppUsers::find()->findTeamCreatorIdByAppId($app);
            $model = Apps::find()->findModelShared($app, $userId);
        } else {
            $userId = \Yii::$app->user->identity->appUserId;
            $model = Apps::find()->findModelShared($app, $userId);
        }

        $ymNow = date('Y-m');
        $ym = Yii::$app->request->get('ym', $ymNow);
        $yms = \app\assets\LogAsset::getYms($app);

        $app = Yii::$app->request->get('app', '0');
        $pageLoadTime = Avgpageloadtimeym::find()->all();

        return $this->render('pageloadtime',
                        [
                            'model' => $model,
                            'yms' => $yms,
                            'pageLoadTime' => $pageLoadTime,
                        ]
        );
    }

    public function totalLogsPerMonth($ym, $appId) {
        $res = Yii::$app->db->createCommand('SELECT ifnull(SUM(l.log_count),0) log_count FROM logsym l
	WHERE l.appId = :appId AND l.ym = :ym;', [':ym' => $ym, ':appId' => $appId])->queryScalar();
        $res = self::arrToObj($res);
        return $res;
    }

    public function totalWebPerMonth($ym, $appId) {
        $res = Yii::$app->db->createCommand('SELECT ifnull(SUM(l.data_count),0) log_count FROM aggregate_httpstatus_ym l
	WHERE l.appId = :appId AND l.ym = :ym;', [':ym' => $ym, ':appId' => $appId])->queryScalar();
        $res = self::arrToObj($res);
        return $res;
    }

    public function pageVisit($ym, $appId, $limit = 5) {
        $res = Yii::$app->db->createCommand('SELECT ar.currentUrl label ,ar.data_count data FROM aggregate_link_ym ar
	WHERE ar.appId = :appId AND ar.ym = :ym order by ar.data_count desc limit :limit', [':ym' => $ym, ':appId' => $appId, ':limit' => $limit])->queryAll();
        $res = self::arrToObj($res);
        return $res;
    }

    public function pageVisitSum($ym, $appId) {
        $res = Yii::$app->db->createCommand('SELECT sum(ar.data_count) sum_data FROM aggregate_link_ym ar
	WHERE ar.appId = :appId AND ar.ym = :ym', [':ym' => $ym, ':appId' => $appId])->queryScalar();
        $res = self::arrToObj($res);
        return $res;
    }

    public function pageLoadTime($ym, $appId, $limit = 5) {
        $res = Yii::$app->db->createCommand('SELECT ar.*, ar.currentUrl label ,ar.avgloadtime data FROM avgpageloadtimeym ar
	WHERE ar.appId = :appId AND ar.ym = :ym order by ar.avgloadtime desc limit :limit', [':ym' => $ym, ':appId' => $appId, ':limit' => $limit])->queryAll();
        $res = self::arrToObj($res);
        return $res;
    }

    public function httpsStatus($ym, $appId) {
        $res = Yii::$app->db->createCommand('SELECT hsym.data_count data,hsym.statusCode,'
                        . ' CONCAT(hsym.statusCode," (",hsym.statusText,")") label FROM aggregate_httpstatus_ym hsym'
                        . ' WHERE hsym.appId = :appId AND hsym.ym = :ym order by hsym.data_count desc',
                        [':ym' => $ym, ':appId' => $appId])->queryAll();
        $res = self::arrToObj($res);
        return $res;
    }

    public function httpsStatusYmPercent($ym, $app, $startRange, $endRange) {
        $res = Yii::$app->db->createCommand('SELECT ifnull(ROUND((stat*100)/ total,2),0) statPerc,stat FROM 
        (SELECT 
	ifnull(sum(IF((hs.statusCode >= :startRange AND hs.statusCode <= :endRange), hs.data_count,0)),0) stat,
	ifnull(SUM(hs.data_count),0) total
	 FROM aggregate_httpstatus_ym hs
          WHERE hs.appId = :appId AND hs.ym = :ym 
        ) u',
                        [':ym' => $ym, ':appId' => $app, ':startRange' => $startRange, ':endRange' => $endRange])->queryOne();
        $res = self::arrToObj($res);
        return $res;
    }

    public function logsYmPercent($ym, $app, $logType) {
        $res = Yii::$app->db->createCommand('SELECT ifnull(ROUND((stat*100)/ total,2),0) statPerc,stat  FROM (SELECT 
        ifnull(SUM(if((t.log_type = :logType),l.log_count,0)),0) stat,
        ifnull(sum(l.log_count),0) total FROM logsym l, apps_tags t
	WHERE l.tags = t.tags and
         l.appid = :appId AND l.ym = :ym 
	) d',
                        [':ym' => $ym, ':appId' => $app, ':logType' => $logType])->queryOne();
        $res = self::arrToObj($res);
        return $res;
    }

    public function logsym($ym, $appId) {
        $res = Yii::$app->db->createCommand('SELECT ifnull(sum(lym.log_count),0) data,lym.tags label,atg.color_code,atg.log_type FROM logsym lym, apps_tags atg
	WHERE lym.tags = atg.tags
             and lym.ym = :ym and lym.appId = :appId
             GROUP BY atg.tags',
                        [':ym' => $ym, ':appId' => $appId])->queryAll();
        $res = self::arrToObj($res);
        return $res;
    }

    public function actionPageScoreChart($app) {

        $ym = date('Y-m');
        $response = $this->pageVisit($ym, $app);
        echo json_encode($response, false);
        header('Content-type: application/json');
        return null;
    }

    public function actionHttpStatusChart($app) {

        $ym = date('Y-m');
        $response = $this->httpsStatus($ym, $app);
        echo json_encode($response, false);
        header('Content-type: application/json');
        return null;
    }

    public function actionLogsYmChart($app) {

        $ym = date('Y-m');
        $response = $this->logsym($ym, $app);
        echo json_encode($response, false);
        header('Content-type: application/json');
        return null;
    }

    public static function arrToObj($arr) {
        $arr = json_encode($arr);
        $arr = json_decode($arr);
        return $arr;
    }

}
