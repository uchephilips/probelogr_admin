<?php

namespace app\controllers;

use Yii;
use app\models\AppUsers;
use app\models\search\AppUsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AppUsersController implements the CRUD actions for AppUsers model.
 */
class AppUsersController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        $this->layout = 'main_frontend';
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'delete', 'view', 'update', 'suspend', 'reset-password'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'delete', 'view', 'update', 'suspend', 'reset-password'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \app\assets\RoleManagement::hasPrivilege($action);
                            //return true;
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AppUsers models.
     * @return mixed
     */
    public function actionIndex() {

        $searchModel = new AppUsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'models' => $dataProvider->getModels(),
                    'pages' => $dataProvider->pagination,
                    'model' => $searchModel
        ]);
    }

    /**
     * Displays a single AppUsers model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        $businessProfile = \app\models\AppUserBusinessProfile::findOne(['created_by' => $id]);
        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'businessProfile' => $businessProfile
        ]);
    }

    /**
     * Updates an existing AppUsers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate() {
        $id = Yii::$app->request->get("id");
        $model = $this->findModel($id);



        if ($model->load(Yii::$app->request->post())) {

            $model->role_id = Yii::$app->request->post('AppUsers')["role_id"];

            $model->last_update = date("Y-m-d H:i:s");
            $model->updated_by = Yii::$app->user->id;




            if ($model->save()) {
                Yii::$app->session->setFlash('success', "User Updated");
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    public function actionSuspend() {
        $id = Yii::$app->request->get("id");
        $model = $this->findModel($id);

        if ($model->is_active == 1) {
            $model->is_active = 0;
        } else {
            $model->is_active = 1;
        }
        if ($model->save()) {
            Yii::$app->session->setFlash('success', ($model->is_active == 0) ? "User suspended" : "User unsuspended");
        } else {
            Yii::$app->session->setFlash('success', ($model->is_active == 0) ? "Couldn't suspend user" : "Couldn't unsuspended user");
        }
        return $this->redirect(['index']);
    }

    public function actionResetPassword() {
        $id = Yii::$app->request->get("id");
        $model = $this->findModel($id);

//      $model->forget_password = 1;
        $password = strtoupper(hash('crc32', uniqid() . rand(1, 99)));
        $model->password = md5($password);

        if ($model->save()) {
//            \app\assets\MailerAssets::resetPassword($model, $password);
            $cmd = str_replace("{arg}", $model->id . '-' . $password, Yii::$app->params['cmd_account_reset']);
            exec($cmd);

            Yii::$app->session->setFlash('success', "A new password has been sent to user");
        } else {
            Yii::$app->session->setFlash('success', "Couldn't forget password");
        }
        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing AppUsers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();



        return $this->redirect(['index']);
    }

    /**
     * Finds the AppUsers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AppUsers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        $model = AppUsers::find()->addSelect('app_users.*')
                        ->addSelect(['role_name' => '(select r.role_name from roles r where r.id = app_users.role_id)'])->andFilterWhere([
                    'id' => $id])->one();
        if (isset($model)) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
