<?php

namespace app\controllers;

use Yii;
use app\models\plt\Avgpageloadtimeymd;
use app\models\Apps;
use app\models\plt\search\AvgpageloadtimeymdSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LinkLogsController implements the CRUD actions for Avgpageloadtimeymd model.
 */
class LinkLogsController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        $this->layout = 'main_frontend';
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Avgpageloadtimeymd models.
     * @return mixed
     */
    public function actionPageLoadTime($app) {

        $app = Yii::$app->request->get('app', '0');

        if (isset($_GET['shared'])) {
            $createdBy = \app\models\AppUsers::find()->findTeamCreatorIdByAppId($app);
            $model = Apps::find()->findModelShared($app, $userId);
        } else {
            $userId = \Yii::$app->user->identity->appUserId;
            $model = Apps::find()->findModelShared($app, $userId);
        }

        $searchModel = new AvgpageloadtimeymdSearch();
        $searchModel->userId = $userId;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $ymds = \app\assets\LogAsset::getYmds($app);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model' => $model,
                    'ymds' => $ymds,
                    'models' => $dataProvider->getModels(),
                    'pagination' => $dataProvider->getPagination(),
        ]);
    }

    /**
     * Displays a single Avgpageloadtimeymd model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Avgpageloadtimeymd model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Avgpageloadtimeymd();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Avgpageloadtimeymd model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Avgpageloadtimeymd model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Avgpageloadtimeymd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Avgpageloadtimeymd the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Avgpageloadtimeymd::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
