<?php

namespace app\controllers;

use Yii;
use app\models\Apps;
use app\models\Project;
use app\models\search\AppsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AppsController implements the CRUD actions for Apps model.
 */
class AppLogsController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        $this->layout = 'main_frontend';
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['real-time', 'past','apps-as-select-option','app-tags-as-select-option','search-logs'],
                'rules' => [
                    [
                        'actions' => ['real-time', 'past','apps-as-select-option','app-tags-as-select-option','search-logs'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \app\assets\RoleManagement::hasPrivilege($action);
                            //return true;
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delte' => ['POST'],
                ],
            ],
        ];
    }

    public function actionRealTime($id) {

        if (isset($_GET['shared'])) {
            $createdBy = \app\models\AppUsers::find()->findTeamCreatorIdByAppId($id);
            $model = Apps::find()->findModelShared($id, $createdBy);
            $endpoint = \app\models\AppsEndpoints::find()->findAppEndpointModelShared($id, $createdBy);
        } else {
            $model = Apps::find()->findModel($id);
            $endpoint = \app\models\AppsEndpoints::find()->findAppEndpointModel($id);
        }

        if ($endpoint->isNewRecord) {
            Yii::$app->session->setFlash('info', "unable to access app settings");
            return $this->redirect(['apps']);
        } else {
            return $this->render('real-time', [
                        'endpoint' => $endpoint,
                        'model' => $model,
                        'shared' => isset($_GET['shared']),
            ]);
        }
        Yii::$app->session->setFlash('info', "Unable to view log at this moment");
        return $this->redirect(['apps']);
    }

    public function actionPast($id) {

        \app\assets\LoadStatAsset::loadUnsyncedLogs();

        if (isset($_GET['shared'])) {
            $createdBy = \app\models\AppUsers::find()->findTeamCreatorIdByAppId($id);

            $model = Apps::find()->findModelShared($id, $createdBy);
            $endpoint = \app\models\AppsEndpoints::find()->findAppEndpointModelShared($id, $createdBy);
        } else {
            $model = Apps::find()->findModel($id);
            $endpoint = \app\models\AppsEndpoints::find()->findAppEndpointModel($id);
        }

        $ymds = \app\assets\LogAsset::getYmds($id);

        $tags = \app\models\AppsTags::findAll(['app_id' => $id]);

        return $this->render('past', [
                    'endpoint' => $endpoint,
                    'model' => $model,
                    'ymds' => $ymds,
                    'tags' => $tags,
                    'shared' => isset($_GET['shared']),
        ]);
    }

    public function actionSearchLogs() {
        $id = Yii::$app->request->get("id");
        $ymd = Yii::$app->request->get("ymd");
        $tag = Yii::$app->request->get("tag");
        $page = Yii::$app->request->get("page");
        $size = Yii::$app->request->get("size");
        header('Content-type: application/json');
        $response = \app\assets\LogAsset::searchLog($id, $ymd, $tag, $page, $size, isset($_GET['shared']));

        echo json_encode($response, false);

        exit;
    }

    /**
     * Finds the Apps model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Apps the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        $model = Apps::find()->addSelect('apps.*')
                        ->addSelect(['project_name' => '(SELECT p.name FROM project p WHERE apps.project_id = p.id)'])
                        ->andOnCondition(['id' => $id, 'created_by' => Yii::$app->user->identity->appUserId])->one();
        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * 
     * @param type $id
     * @return \app\models\AppsEndpoints
     */
    protected function findAppEndpointModel($id) {
        $endpoint = \app\models\AppsEndpoints::findOne(['app_id' => $id, 'created_by' => Yii::$app->user->identity->appUserId]);
        if (isset($endpoint)) {
            return $endpoint;
        }
        return new \app\models\AppsEndpoints();
    }

    public function actionAppsAsSelectOption() {
        $term = \Yii::$app->request->get('term');
        $result = \app\models\search\AppsSearch::searchAppAsSelectOption($term);
        echo json_encode($result, false);
        header('Content-type: application/json');
        exit;
    }

    public function actionAppTagsAsSelectOption() {
        $term = \Yii::$app->request->get('term');
        $result = \app\models\search\AppsTagsSearch::searchAppTagsAsSelectOption($term);
        echo json_encode($result, false);
        header('Content-type: application/json');
        exit;
    }

}
