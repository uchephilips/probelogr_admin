<?php

namespace app\controllers;

use Yii;
use app\models\content\ContentBlog;
use app\models\search\content\ContentBlogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ContentBlogController implements the CRUD actions for ContentBlog model.
 */
class ContentBlogController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        $this->layout = 'main_frontend';
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'delete', 'view', 'update'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'delete', 'update', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \app\assets\RoleManagement::hasPrivilege($action);
                            //return true;
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delte' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ContentBlog models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ContentBlogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'models' => $dataProvider->getModels(),
                    'pages' => $dataProvider->pagination,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ContentBlog model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($tagline) {
        return $this->render('view', [
                    'model' => $this->findModelByTagline($tagline),
        ]);
    }

    /**
     * Creates a new ContentBlog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new ContentBlog();

        if ($model->load(Yii::$app->request->post())) {

            $model->created_by = Yii::$app->user->identity->appUserId;
            $model->created_time = date("Y-m-d H:i:s");


            if ($model->save()) {
                Yii::$app->session->setFlash('success', "Successful");
                return $this->redirect(['index']);
            } else {
                Yii::$app->session->setFlash('error', \app\assets\Misc::readableError($model->getErrorSummary(TRUE)));
                Yii::error($model->getErrorSummary(true));
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing ContentBlog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->updated_by = Yii::$app->user->identity->appUserId;
            $model->update_time = date("Y-m-d H:i:s");
            if ($model->save()) {
                Yii::$app->session->setFlash('success', "Successful");
                return $this->redirect(['index']);
            } else {
                Yii::$app->session->setFlash('error', \app\assets\Misc::readableError($model->getErrorSummary(TRUE)));
                Yii::error($model->getErrorSummary(true));
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ContentBlog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ContentBlog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ContentBlog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = ContentBlog::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the ContentBlog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ContentBlog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelByTagline($tagline) {
        if (($model = ContentBlog::findOne(['tagline' => $tagline])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
