<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\assets;

use Yii;

/**
 * Description of MiscTracksend
 *
 * @author uchep
 */
class Misc {

    public static function curlPost($arr, $url, $type, $accessKey = null) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($arr),
            CURLOPT_HTTPHEADER => Misc::headerType($type, $accessKey),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        Yii::error($url);
        Yii::error($arr);
        Yii::error($response);
        Yii::error("-------------");
        Yii::error("-------------");
        Yii::error("-------------");

        Yii::error(json_encode($arr));
        if ($err) {
            Yii::error("cURL payload #:" . $url);
            Yii::error("cURL payload #:");
            Yii::error($response);
            Yii::error("cURL Error #:" . $err);
            return null;
        } else {
            Yii::error("cURL payload #:" . $url);
            Yii::error("cURL response #:" . $response);
            return json_decode($response);
        }
    }

    public static function curlGet($url, $type, $accessKey = null) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => Misc::headerType($type, $accessKey),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        Yii::error($url);
        Yii::error(Misc::headerType($type, $accessKey));
        Yii::error(json_decode($response));
        if ($err) {
            Yii::error($url);
            Yii::error($err);
            return null;
        } else {
            Yii::error("cURL payload #:" . $url);
            return json_decode($response);
        }
    }

    private static function headerType($type, $accessToken = null) {
        $header = array();
        \Yii::error("type" . $type);
        \Yii::error("accessToken" . $accessToken);
        switch ($type) {
            case 'settings': {
                    $header = array(
                        "content-type: application/json",
                        'sourceToken: ' . \Yii::$app->params['SOURCE_TOKEN']);
                    break;
                }
            case 'load_stat': {
                    $header = array("content-type: application/json",
                        'accessToken: ' . $accessToken,
                        'sourceToken: ' . \Yii::$app->params['SOURCE_TOKEN']);
                    break;
                }
            case 'user_access': {
                    $header = array("content-type: application/json",
                        'accessToken: ' . \Yii::$app->user->identity->accessToken,
                        'sourceToken: ' . \Yii::$app->params['SOURCE_TOKEN']);
                    break;
                }
            default : {
                    $header = array("content-type: application/json", 'sourceToken: ' . \Yii::$app->params['SOURCE_TOKEN']);
                    break;
                }
        }
        return $header;
    }

    public static function unique_id($l = 8) {
        return substr(md5(uniqid(mt_rand(), true)), 0, $l);
    }

    public static function getMoney($number, $dec = 2) {
        return isset($number) ? number_format($number, $dec) : 0;
    }

    public static function time_elapsed_string($datetime, $full = false, $msgFuture = "to go", $msgPresent = " just now", $msgPast = " ago") {
        if (isset($datetime)) {
            $now = new \DateTime;
            $date = new \DateTime($datetime);
            $diff = $now->diff($date);
            $datePosition = " ago";
            if ($now < $date) {
                $datePosition = $msgFuture;
            }

            $diff->w = floor($diff->d / 7);
            $diff->d -= $diff->w * 7;

            $string = array(
                'y' => 'year',
                'm' => 'month',
                'w' => 'week',
                'd' => 'day',
                'h' => 'hour',
                'i' => 'minute',
                's' => 'second',
            );
            foreach ($string as $k => &$v) {
                if ($diff->$k) {
                    $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                } else {
                    unset($string[$k]);
                }
            }

            if (!$full)
                $string = array_slice($string, 0, 1);
            return $string ? implode(', ', $string) . $datePosition : 'just now';
        } else {
            return "not set";
        }
    }

    public static function isPast($dateStr) {
        if (isset($dateStr)) {
            $date = new \DateTime($dateStr);
            $now = new \DateTime();
            return $date < $now;
        }
        return false;
    }

    public static function randomErrMsg() {
        $msgs = ["oops", "sorry", "oh no", "whoops"];
        return $msgs[array_rand($msgs)];
    }

    public static function readableError($error) {
        $str = "";
        if (isset($error)) {
            foreach ($error as $value) {
                $str .= "<br>" . $value;
            }
        }
        return $str;
    }

    public static function createClause($queryValue, $query) {
        $clause = "";
        if (isset($queryValue)) {
            $clause = str_replace('{val}', $queryValue, $query);
        }
        return $clause;
    }

    public static function getNum($param) {
        return is_numeric($param) ? $param : 0;
    }

    public static function ellipsis($str, $start = 0, $end = 20, $trimMarker = '...') {
        //return \mb_strimwidth($str, $start, $end, $trimMarker);
        return $str;
    }

    public static function rand_color() {
        return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
    }

    public static function formatMilliseconds($milliseconds) {
        $seconds = floor($milliseconds / 1000);
        $minutes = floor($seconds / 60);
        $hours = floor($minutes / 60);
        $milliseconds = $milliseconds % 1000;
        $seconds = $seconds % 60;
        $minutes = $minutes % 60;

        //$format = '%u:%02u:%02u sec, %03u ms';
        $format = '%02u min, %02u sec, %03u ms';
        $time = sprintf($format, $minutes, $seconds, $milliseconds);
        return rtrim($time);
    }

    public static function millisecPercent($milliseconds, $oneMin = 60000) {
        return round((($milliseconds * 100) / $oneMin), 2);
    }

}
