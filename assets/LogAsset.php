<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\assets;

use Yii;
use app\models\Apps;

/**
 * Description of LoadStatAsset
 *
 * @author uchephilz
 */
class LogAsset {

    public static function searchLog($id, $ymd, $tag, $page = 1, $size = 5, $shared) {

        $apps = null;
        if ($shared) {
            $createdBy = \app\models\AppUsers::find()->findTeamCreatorIdByAppId($id);
            $apps = Apps::find()->findModelByIdAndUserId($id,$createdBy);
        } else {
            $apps = Apps::find()->findModel($id);
        }

        if (isset($apps)) {
            $endpoint = \app\models\AppsEndpoints::findOne(['app_id' => $apps->id]);
            $ymd = isset($ymd) ? $ymd : date('Y-m-d');
            $ym = explode('-', $ymd)[0] . '-' . explode('-', $ymd)[1];
            $serchRequest = ['userid' => $apps->created_by,
                'projectid' => $apps->project_id,
                'appid' => $apps->id,
                'ym' => $ym,
                'tags' => $tag,
                'ymd' => $ymd,
                'page' => $page, 'size' => $size];
            $resp = \app\assets\Misc::curlPost($serchRequest, \Yii::$app->params['SEARCH_LOG'], 'load_stat', $endpoint->access_key);

            $successfull = \app\assets\ServiceAsset::isSuccessfull($resp);
            if ($successfull) {
                $obj = \app\assets\ServiceAsset::getResponseObj($resp);
                if (isset($obj)) {
                    return $obj;
                }
            }
        }

        return $resp;
    }

    public static function getYms($appId) {
        $res = Yii::$app->db->createCommand('SELECT l.ym FROM logsym l  where l.appId = :appId GROUP BY l.ym', [':appId' => $appId])->queryAll();
        $res = self::arrToObj($res);
        return $res;
    }

    public static function getYmds($appId) {
        $res = Yii::$app->db->createCommand('SELECT l.ymd FROM logsymd l  where l.appId = :appId GROUP BY l.ymd', [':appId' => $appId])->queryAll();
        $res = self::arrToObj($res);
        return $res;
    }

    public static function arrToObj($arr) {
        $arr = json_encode($arr);
        $arr = json_decode($arr);
        return $arr;
    }

}
