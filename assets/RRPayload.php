<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\assets;

/**
 * Description of RRPayload
 *
 * @author uchephilz
 */
class RRPayload {

    /**
     * 
     * @param integer $retn
     * @param string $desc
     * @param type $obj
     * @return type
     */
    const CODE_SUCCESS = 0;
    const CODE_SIMPLE_FEEDBACK = 100;

    public static function response_object($rtn, $desc, $obj = null) {
        $arr = ['retn' => $rtn, 'desc' => $desc, 'obj' => $obj];
        return json_encode($arr);
    }

    public static function successful($obj) {
        return self::response_object(self::CODE_SUCCESS, "Successful", $obj);
        
    }

    public static function errorSimpleFeedbck($desc) {
        return self::response_object(self::CODE_SIMPLE_FEEDBACK, $desc, null);
    }

}
