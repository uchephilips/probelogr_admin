<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public static function NGN_SIGN() {
        return '&#8358;';
    }

    private static function INVITE_STATUS() {
        return [
            'J' => 'JOINED',
            'A' => 'APPROVED'
            , 'P' => 'PENDING'
            , 'R' => 'REJECTED'
        ];
    }

    public static function GET_YES_NO() {
        return [
            'NO' => 'NO',
            'YES' => 'YES'
        ];
    }

    public static function INVITE_STATUS_JOINED() {
        return self::INVITE_STATUS()['J'];
    }

    public static function INVITE_STATUS_APPROVED() {
        return self::INVITE_STATUS()['A'];
    }

    public static function INVITE_STATUS_REJECTED() {
        return self::INVITE_STATUS()['R'];
    }

    public static function INVITE_STATUS_PENDING() {
        return self::INVITE_STATUS()['P'];
    }

}
