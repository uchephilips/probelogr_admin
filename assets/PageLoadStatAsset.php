<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\assets;

/**
 * Description of PageLoadStatAsset
 *
 * @author uchephilz
 */
class PageLoadStatAsset {

//    public static function loadTeamUnsyncedLogs() {
//
//        $us = \app\models\search\AppsSearch::getTeamLeadIdByUser();
//        if (isset($us)) {
//            foreach ($us as $value) {
//                self::loadUnsyncedLogs($value['created_by']);
//            }
//        }
//    }

    public static function loadUnsyncedLogs($apps, $user = 0) {

        if ($user == 0) {
            $user = \Yii::$app->user->identity->appUserId;
        }

        $endpoint = \app\models\AppsEndpoints::findOne(['app_id' => $apps->id]);

        if (isset($endpoint)) {
            self::loadAggrYm($user, $endpoint);
            self::loadAggrYmd($user, $endpoint);
        }
    }

    private static function loadAggrYm($user, $endpoint) {
        $lastYm = \Yii::$app->db->createCommand("SELECT ym"
                        . " FROM aggregate_httpstatus_ym l WHERE l.userid = :userId ORDER BY l.ym LIMIT 1;", [':userId' => $user])->queryOne();
        $lastYm = self::arrToObj($lastYm);
        $resp = null;
        if (!empty($lastSyncYmLogs->ym)) {
            $resp = \app\assets\Misc::curlGet(\Yii::$app->params['GET_PLT_YM'] . '?=ym' . $lastYm->ym, 'load_stat', $endpoint->access_key);
        } else {
            $resp = \app\assets\Misc::curlGet(\Yii::$app->params['GET_PLT_YM'], 'load_stat', $endpoint->access_key);
        }

        $successfull = \app\assets\ServiceAsset::isSuccessfull($resp);

        if ($successfull) {
            $obj = \app\assets\ServiceAsset::getResponseObj($resp);
            if (isset($obj)) {
                \app\models\plt\query\AvgpageloadtimeymQuery::load($obj->pltym);
            }
        }
    }

    private static function loadAggrYmd($user, $endpoint) {
        $lastYmd = \Yii::$app->db->createCommand("SELECT ymd"
                        . " FROM aggregate_httpstatus_ymd l WHERE l.userId = :userId ORDER BY l.ymd LIMIT 1;", [':userId' => $user])->queryOne();
        $lastYmd = self::arrToObj($lastYmd);
        $resp = null;
        if (!empty($lastYmd->ymd)) {
            $resp = \app\assets\Misc::curlGet(\Yii::$app->params['GET_PLT_YMD'] . '?=ymd' . $lastYmd->ymd, 'load_stat', $endpoint->access_key);
        } else {
            $resp = \app\assets\Misc::curlGet(\Yii::$app->params['GET_PLT_YMD'], 'load_stat', $endpoint->access_key);
        }

        $successfull = \app\assets\ServiceAsset::isSuccessfull($resp);

        if ($successfull) {
            $obj = \app\assets\ServiceAsset::getResponseObj($resp);
            if (isset($obj)) {
                \app\models\plt\query\AvgpageloadtimeymdQuery::load($obj->pltymd);
            }
        }
    }

    public static function arrToObj($arr) {
        $arr = json_encode($arr);
        $arr = json_decode($arr);
        return $arr;
    }

}
