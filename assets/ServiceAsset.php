<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\assets;

/**
 * Description of BolServiceAsset
 *
 * @author uchephilz
 */
class ServiceAsset {

    
    public static function getResponseObj($resp) {
        if (isset($resp)) {
            if (property_exists($resp, 'retn')) {
                $retn = $resp->retn;
                if ($retn == 0) {
                    return $resp->obj;
                }
            }
        }
        return null;
    }

    public static function isSuccessfull($resp) {

        if (isset($resp)) {
            if (property_exists($resp, 'retn')) {
                return $resp->retn == 0;
            }
        }

        return false;
    }

    public static function getResponseDesc($resp) {
        if (isset($resp)) {
            if (property_exists($resp, 'desc')) {
                return $resp->desc;
            }
        }
        return 'Unable to reach service';
    }

    public static function getValidationErrorMsg($resp) {
        $errMsg = '';
        if (isset($resp)) {
            if (property_exists($resp, 'retn')) {
                if ($resp->retn != 0) {
                    $obj = $resp->obj;

                    if (is_array($obj)) {
                        foreach ($obj as $value) {

                            $errMsg .= ($resp->retn == 260) ? "$value->name: $value->message<br>" : "$value->message<br>";
                        }
                    } else if (is_object($obj)) {
                        $errMsg .= "$obj->message<br>";
                    }
                }
            }
        }
        return $errMsg;
    }

    private static function getData($imageData) {
        if (isset($imageData)) {
            $explode = \explode(',', $imageData);
            if (key_exists(1, $explode)) {
                return $explode[1];
            }
        }
        return null;
    }

}
