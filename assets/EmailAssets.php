<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\assets;

/**
 * Description of EmailAssets
 *
 * @author uchephilz
 */
class EmailAssets {

    public static function resetPassword($email, $token) {


        $message = file_get_contents(\Yii::$app->params['resetpassword_email_template']);
        $message = str_replace('{bt.link}', \Yii::$app->params['bt.link'], $message);
        $message = str_replace('{bt.logo}', \Yii::$app->params['bt.logo'], $message);
        $message = str_replace('{bt.password.reset.link}', \Yii::$app->params['bt.password.reset.link'] . $token, $message);
        $message = str_replace('{bt.password.reset.howto}', \Yii::$app->params['bt.password.reset.howto'], $message);
        $message = str_replace('{bt.help.link}', \Yii::$app->params['bt.help.link'], $message);
        $message = str_replace('{bt.contactus.link}', \Yii::$app->params['bt.contactus.link'], $message);

        return self::emailSend($email, "Reset Password | Bold Transaction", $message);
    }

    public static function weeklyReport() {

        $string_date = date("Y-m-d");
        echo $day_of_week = date('N', strtotime($string_date));
        echo '<br>';
        echo $week_first_day = date('Y-m-d', strtotime($string_date . " - " . ($day_of_week - 1) . " days"));
        echo '<br>';
        echo $week_last_day = date('Y-m-d', strtotime($string_date . " + " . (7 - $day_of_week) . " days"));

        echo '<br>';
    }

    /**
     * 
     * @param type $fullname
     * @param type $email
     * @return type
     */
    public static function createAccount($fullname, $email) {
        $message = file_get_contents(\Yii::$app->params['email.content.registered']);
        $message = str_replace('{fullname}', $fullname, $message);
        $message = str_replace('{link.user.app}', \Yii::$app->params['link.user.app'], $message);
        return self::emailSend($email, "You are Registered at Probelogr", $message);
    }

    public static function createResetPassword($email, $login_token) {
        $message = file_get_contents(\Yii::$app->params['email.content.resetpassword']);
        $message = str_replace('{link.change.password}', \Yii::$app->params['link.change.password'] . "?login_token=$login_token", $message);
        $message = str_replace('{link.access.account}', \Yii::$app->params['link.access.account'] . "?login_token=$login_token", $message);

        return self::emailSend($email, "Regain access to your Probelogr Account", $message);
    }

    public static function emailSend($email, $subject, $message) {
        $arr = ['to' => $email, 'subject' => $subject, 'body' => $message];
        $resp = \app\assets\Misc::curlPost($arr, \Yii::$app->params['PROBELOGR_EMAILER'], '');
        return $resp;
    }

    public static function sendTeamInvite($email, $token) {
        $arr = ['email' => $email, 'token' => $token];
        $resp = \app\assets\Misc::curlPost($arr, \Yii::$app->params['PROBELOGR_EMAILER_TEAM_INVITE'], 'user_access','');
        return $resp;
    }

}
