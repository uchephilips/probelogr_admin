<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\assets;

/**
 * Description of LoadStatAsset
 *
 * @author uchephilz
 */
class LoadStatAsset {

    public static function loadTeamUnsyncedLogs() {

        $us = \app\models\search\AppsSearch::getTeamLeadIdByUser();
        foreach ($us as $value) {
            self::loadUnsyncedLogs($value['created_by']);
        }
    }

    public static function loadUnsyncedLogs($user = 0) {

        if ($user == 0) {
            $user = \Yii::$app->user->identity->appUserId;
        }

        $endpoint = \app\models\AppsEndpoints::findOne(['created_by' => $user]);
        $apps = \app\models\Apps::findOne(['created_by' => $user]);

        if (isset($endpoint)) {

            $lastSyncYmLogs = \Yii::$app->db->createCommand("SELECT l.last_synced,YEAR(l.last_synced) y,DATE_FORMAT(l.last_synced,'%Y-%m') ym,DATE_FORMAT(l.last_synced,'%m') m"
                            . " FROM logsym l WHERE l.userid = :userid ORDER BY l.last_synced LIMIT 1;", [':userid' => $user])->queryOne();

            $lastSyncYmLogs = self::arrToObj($lastSyncYmLogs);

            $lastSyncYmdLogs = \Yii::$app->db->createCommand("SELECT l.last_synced,YEAR(l.last_synced) y,DATE_FORMAT(l.last_synced,'%Y-%m') ym,DATE_FORMAT(l.last_synced,'%Y-%m-%d') ymd,DATE_FORMAT(l.last_synced,'%m') m"
                            . " FROM logsym l WHERE l.userid = :userid ORDER BY l.last_synced LIMIT 1;", [':userid' => $user])->queryOne();
            $lastSyncYmdLogs = self::arrToObj($lastSyncYmdLogs);

            $resp = null;

            $arrData = ['appId' => $endpoint->app_id, 'accessKey' => $endpoint->access_key, 'userid' => $user];

//            print_r(($lastSyncYmLogs));
//            print_r(count($lastSyncYmLogs));
//            print_r(empty($lastSyncYmLogs->ym)?"em":"!em");
//            exit;

            $pushRes = \app\assets\AppSettingAsset::pushUserAppConfig($arrData);

            if (!empty($lastSyncYmLogs->ym)) {
                $resp = \app\assets\Misc::curlGet(\Yii::$app->params['FUTURE_YM_LOGS'] . '/' . $lastSyncYmLogs->ym, 'load_stat', $endpoint->access_key);
            } else {
                $resp = \app\assets\Misc::curlGet(\Yii::$app->params['GET_YM_USER_LOGS'], 'load_stat', $endpoint->access_key);
            }

            $successfull = \app\assets\ServiceAsset::isSuccessfull($resp);

            if ($successfull) {
                $obj = \app\assets\ServiceAsset::getResponseObj($resp);
                if (isset($obj)) {
                    self::loadYmData($obj);
                }
            }


            if (!empty($lastSyncYmdLogs->ymd)) {
                $resp = \app\assets\Misc::curlGet(\Yii::$app->params['FUTURE_YMD_LOGS'] . '/' . $lastSyncYmdLogs->ymd, 'load_stat', $endpoint->access_key);
            } else {
                $resp = \app\assets\Misc::curlGet(\Yii::$app->params['GET_YMD_USER_LOGS'], 'load_stat', $endpoint->access_key);
            }
            $successfull = \app\assets\ServiceAsset::isSuccessfull($resp);

            if ($successfull) {
                $obj = \app\assets\ServiceAsset::getResponseObj($resp);
                if (isset($obj)) {
                    self::loadYmdData($obj);
                }
            }
        } else {
            
        }
    }

    public static function loadYmData($obj) {

        foreach ($obj as $value) {

            if (isset($value->userid)) {
                $lym = \app\models\Logsym::findOne([
                            'userid' => $value->userid,
                            'projectid' => $value->projectid,
                            'appid' => $value->appid,
                            'tags' => $value->tags,
                            'ym' => $value->ym,
                ]);


                if (!isset($lym)) {
                    $lym = new \app\models\Logsym();
                    $lym->userid = $value->userid;
                    $lym->projectid = $value->projectid;
                    $lym->appid = $value->appid;
                    $lym->tags = $value->tags;
                    $lym->ym = $value->ym;
                }

                $lym->log_count = $value->log_count;
                $lym->last_synced = date("Y-m-d H:i:s");

                if (!$lym->save()) {
                    print_r($lym->getErrors());
                    exit;
                }
            }
        }
    }

    public static function loadYmdData($obj) {

        foreach ($obj as $value) {
            if (isset($value->userid)) {

                $lym = \app\models\Logsymd::findOne([
                            'userid' => $value->userid,
                            'projectid' => $value->projectid,
                            'appid' => $value->appid,
                            'tags' => $value->tags,
                            'ym' => $value->ym,
                            'ymd' => $value->ymd,
                ]);

                if (!isset($lym)) {
                    $lym = new \app\models\Logsymd();
                    $lym->userid = $value->userid;
                    $lym->projectid = $value->projectid;
                    $lym->appid = $value->appid;
                    $lym->tags = $value->tags;
                    $lym->ym = $value->ym;
                    $lym->ymd = $value->ymd;
                }

                $lym->log_count = $value->log_count;
                $lym->last_synced = date("Y-m-d H:i:s");

                if (!$lym->save()) {
                    print_r($lym->getErrors());
                    exit;
                }
            }
        }
    }

    public static function arrToObj($arr) {
        $arr = json_encode($arr);
        $arr = json_decode($arr);
        return $arr;
    }

    public static function loadAppStat($appId) {

        $endpoint = \app\models\AppsEndpoints::findOne(['app_id' => $appId]);

        $resp = \app\assets\Misc::curlGet(\Yii::$app->params['LOAD_MONTHLY_APP_STAT'] . '/' . date('Y'), 'load_stat', $endpoint->access_key);

        $successfull = \app\assets\ServiceAsset::isSuccessfull($resp);
        $desc = \app\assets\ServiceAsset::getResponseDesc($resp);

        if ($successfull) {
            $obj = \app\assets\ServiceAsset::getResponseObj($resp);
            if (isset($obj)) {
                foreach ($obj as $value) {

                    if (isset($value->userid)) {

                        $lym = \app\models\Logsym::findOne([
                                    'userid' => $value->userid,
                                    'projectid' => $value->projectid,
                                    'appid' => $value->appid,
                                    'tags' => $value->tags,
                                    'ym' => $value->ym,
                        ]);

                        if (!isset($lym)) {
                            $lym = new \app\models\Logsym();
                            $lym->userid = $value->userid;
                            $lym->projectid = $value->projectid;
                            $lym->appid = $value->appid;
                            $lym->tags = $value->tags;
                            $lym->ym = $value->ym;
                        }

                        $lym->log_count = $value->log_count;
                        $lym->last_synced = date("Y-m-d H:i:s");
                        ;

                        if (!$lym->save()) {
                            print_r($lym->getErrors());
                            exit;
                        }

                        //Array ( [0] => stdClass Object ( [userid] => 8 [projectid] => 5 [appid] => 4 [tags] => LOGIN [ym] => 2020-05 [log_count] => 4 ) [1] => stdClass Object ( [userid] => 8 [projectid] => 5 [appid] => 4 [tags] => VIEW PAYMENT PAGE [ym] => 2020-05 [log_count] => 2 ) )
                    }
                }
                print_r($obj);
                exit;
            }
        }

        print_r($resp);
        exit;
    }

}
