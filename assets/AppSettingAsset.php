<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\assets;

/**
 * Description of AppSettingRequest
 *
 * @author uchephilz
 */
class AppSettingAsset {

    public static function pushConfig($accessKey) {
        $resp = \app\assets\Misc::curlGet(\Yii::$app->params['PUSH_CONFIG'].'/'.$accessKey, 'settings');
        return $resp;
    }

    public static function shareConfig($accessKey,$shareEmail) {
        $resp = \app\assets\Misc::curlGet(\Yii::$app->params['SHARE_CONFIG'].'/'.$accessKey.'?shareEmail='.$shareEmail, 'settings');
        return $resp;
    }

    public static function pushUserAppConfig($settings) {
        $resp = \app\assets\Misc::curlPost($settings, \Yii::$app->params['PUSH_USER_APP_CONFIG'], 'settings');
        return $resp;
    }

}
