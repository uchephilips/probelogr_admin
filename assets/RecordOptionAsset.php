<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\assets;

/**
 * Description of RecordOptionAsset
 *
 * @author uchephilz
 */
class RecordOptionAsset {

    public static function getReportTagOptions() {

        $recordOption = \app\models\RecordOption::findOne(['option_key' => 'ReportTag']);
        $returnOPtionsArray =[];
        if (isset($recordOption)) {
            $recordOptionArr = explode(',', $recordOption->option_value);
            foreach ($recordOptionArr as $value) {
                $returnOPtionsArray[$value] = $value;
            }
        }else{
            ErrorAlert::raiseAlert("Couldn't fetch ReportTag");
        }
        return $returnOPtionsArray;
    }

}
