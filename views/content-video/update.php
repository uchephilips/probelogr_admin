<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\content\ContentVideo */

$this->title = 'Update Video Content: ' . $model->video_title;
$this->params['breadcrumbs'][] = ['label' => 'Videos Content', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->video_title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<!--**********************************
          Content body start
      ***********************************-->
<div class="content-body">

    <!-- row -->
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-ttle">Update Video Content</h2>
                        <div class="col text-right">
                            <a href="<?= Yii::getAlias('@web'); ?>/content-video" class="btn btn-lg btn-primary">Back <i class="fa fa-backward"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">


                        <?=
                        $this->render('_form', [
                            'model' => $model,
                        ])
                        ?>


                    </div>
                </div>
            </div>




        </div>
    </div>
    <!-- #/ container -->
</div>
<!--**********************************
    Content body end
***********************************-->
