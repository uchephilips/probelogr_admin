<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\content\ContentVideo */

$this->title = 'Create Video Content';
$this->params['breadcrumbs'][] = ['label' => 'Video Content', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title,'url'=> ['create']];
?>


<!--**********************************
          Content body start
      ***********************************-->
<div class="content-body">

    <!-- row -->
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-ttle">Create Video Content</h2>
                        <div class="col text-right">
                            <a href="<?= Yii::getAlias('@web'); ?>/content-video" class="btn btn-lg btn-primary">Back <i class="fa fa-backward"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">


                        <?=
                        $this->render('_form', [
                            'model' => $model,
                        ])
                        ?>


                    </div>
                </div>
            </div>




        </div>
    </div>
    <!-- #/ container -->
</div>
<!--**********************************
    Content body end
***********************************-->
