<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\content\ContentVideoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $models[] app\models\content\ContentVideo */

$this->title = 'Video Content';
$this->params['breadcrumbs'][] = ['label' => $this->title,'url'=> ['index']];
?>


<!--**********************************
          Content body start
      ***********************************-->
<div class="content-body">
   
    <!-- row -->
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-tile">Video Contents</h2>
                        <div class="col text-right">
                    <a href="<?= Yii::getAlias('@web'); ?>/content-video/create" class="btn btn-lg btn-primary">Video Content <i class="fa fa-plus"></i></a>
                </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <?php foreach ($models as $model) { ?>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-center">
                                <span class="display-5"><i class="icon-control-play gradient-3-text"></i></span>
                                <h4 class="card-widget__title text-dark mt-3"><?= $model->video_title ?></h4>
                                <p class="text-muted"><?= $model->video_description ?></p>
                                <p class="text-muted">
                                    <iframe  src="https://www.youtube-nocookie.com/embed/<?= $model->video_url ?>"
                                            title="YouTube video player"
                                            frameborder="0"
                                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen></iframe>
                                </p>
                                <span class="text-muted"><?= $model->tags ?></span>
                                <span class="text-muted">Published: <?= $model->is_published ?></span>
                            </div>
                        </div>
                        <div class="card-footer border-0 bg-transparent">
                            <div class="row">
                                <div class="col-6 border-right-1 pt-3">
                                    <a class="text-center d-block text-muted" href="<?= Yii::getAlias('@web'); ?>/content-video/update?id=<?= $model->id?>">
                                        <i class="fa fa-edit gradient-3-text"></i>
                                        <p class="">Update</p>
                                    </a>
                                </div>
                                <div class="col-6 pt-3"><a class="text-center d-block text-muted" onclick="return confirm('Are you sure sure you delete this record?');" href="<?= Yii::getAlias('@web'); ?>/content-video/delete?id=<?= $model->id?>">
                                        <i class="fa fa-close gradient-4-text"></i>
                                        <p class="">Delete</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php
            echo \yii\widgets\LinkPager::widget([
                'pagination' => $pages,
            ]);
            ?>

        </div>
    </div>
    <!-- #/ container -->
</div>
<!--**********************************
    Content body end
***********************************-->
