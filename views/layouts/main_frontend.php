<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

$usrData = app\assets\SessionAssets::getUserSession();
?>

<?php $this->beginPage() ?>



<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


        <!-- Favicon -->



        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


        <meta property="og:title" content="Probelogr - Plug into your application and get real-time status of applications or feature of interest. You no longer need to be skilled in software development as Probe-Pingr give you a rich and intuitive interface."/>
        <meta property="og:image" content="<?= Yii::getAlias('@web'); ?>/assets/theme_assets/img/brand/btlogowhite.png"/>
        <meta property="og:url" content="https://www.boldtransaction.com"/>
        <meta name="twitter:card" content="summary_large_image"/>
        <meta property="og:site_name" content="Probelogr - Plug into your application and get real-time status of applications or feature of interest. You no longer need to be skilled in software development as Probe-Pingr give you a rich and intuitive interface.">
        <meta name="twitter:image:alt" content="https://www.boldtransactions.com">
        <meta name="description" content="Plug into your application and get real-time status of applications or feature of interest. You no longer need to be skilled in software development as Probe-Pingr give you a rich and intuitive interface.">

        <?php $this->registerCsrfMetaTags() ?>
        <title>Probelogr | <?= Html::encode($this->title) ?></title>
        <!-- FAVICON -->
        <link rel="icon" href="<?= Yii::getAlias('@web'); ?>/assets/probe_imgs/logo_fav.jpg" type="image/x-icon">
        <link rel="shortcut icon" href="<?= Yii::getAlias('@web'); ?>/assets/probe_imgs/logo_fav.jpg" type="image/x-icon">
        <?php if ((Yii::$app->controller->action->id == 'setup')) {
            ?>
            <link href="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/plugins/jquery-steps/css/jquery.steps.css" rel="stylesheet">
        <?php } ?>

        <?php if ((Yii::$app->controller->id == 'dashboard')) {
            ?>
            <link href="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/plugins/chartist/css/chartist.min.css" rel="stylesheet">
            <link href="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/plugins/chartist-plugin-tooltips/css/chartist-plugin-tooltip.css" rel="stylesheet">

        <?php } ?>

        <?php if ((Yii::$app->controller->action->id == 'logs')) {
            ?>
            <link href="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <?php } ?>

        <link href="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/plugins/toastr/css/toastr.min.css" rel="stylesheet">

        <!-- Custom Stylesheet -->
        <link href="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/css/style.css" rel="stylesheet">




        <script src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/plugins/common/common.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/plugins/toastr/js/toastr.min.js"></script>

        <link href="<?= Yii::getAlias('@web'); ?>/assets/select2/dist/css/select2.min.css" rel="stylesheet" />
        <script src="<?= Yii::getAlias('@web'); ?>/assets/select2/dist/js/select2.min.js"></script>

        <?php if ((Yii::$app->controller->action->id == 'logs')) {
            ?>            
            <script src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/plugins/moment/moment.js"></script>
            <script src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
            <script src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <?php } ?>


        <?php if ((Yii::$app->controller->id == 'dashboard' || Yii::$app->controller->id == 'app-dashboard')) {
            ?>
            <!-- ChartistJS -->

            <script src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/plugins/chart.js/Chart.bundle.min.js"></script>
            <script src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/plugins/chartist/js/chartist.min.js"></script>
            <script src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/plugins/chartist-plugin-tooltips/js/chartist-plugin-tooltip.min.js"></script>

            <script src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/plugins/chart.js/Chart.bundle.min.js"></script>

        <?php } ?>

        <?php
        if ((Yii::$app->controller->id == 'content-blog' || Yii::$app->controller->action->id == 'create' || Yii::$app->controller->action->id == 'update')) {
            ?>

            <link rel="stylesheet" type="text/css" href="<?= Yii::getAlias('@web'); ?>/assets/simditor/site/assets/styles/simditor.css" />


            <script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/assets/simditor/site/assets/scripts/module.js"></script>
            <script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/assets/simditor/site/assets/scripts/hotkeys.js"></script>
            <script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/assets/simditor/site/assets/scripts/uploader.js"></script>
            <script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/assets/simditor/site/assets/scripts/simditor.js"></script>



        <?php } ?>


        <script src="//code.tidio.co/ov7ww7jdcyxkxyyopvjyvlctqwvprion.js" async></script>

        <script>

            window.onload = function () {
                var loadTime = window.performance.timing.domContentLoadedEventEnd - window.performance.timing.navigationStart;
                console.log('Page load time is ' + loadTime);
                timeInMiliseconds = loadTime;
                console.log(convertMillisecToHrMinSec(loadTime));

            }


            var convertMillisecToHrMinSec = (time) => {
                let date = new Date(time);
                let min = date.getMinutes();
                let sec = date.getSeconds();

                min = (min < 10) ? min : min;
                sec = (sec < 10) ? sec : sec;

                return min + ":mins" + sec + " secs";//01:27:55
            }


        </script>

        <style>
            .loader {
                border: 5px solid #f3f3f3; /* Light grey */
                border-top: 5px solid #3498db; /* Blue */
                border-radius: 50%;
                width: 20px;
                height: 20px;
                animation: spin 2s linear infinite;
                display: none;
            }

            @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }
        </style>

    </head>

    <body>

        <!--*******************
            Preloader start
        ********************-->
        <div id="preloader">
            <div class="loader">
                <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
                </svg>
            </div>
        </div>
        <!--*******************
            Preloader end
        ********************-->

        <!--**********************************
        Main wrapper start
    ***********************************-->
        <div id="main-wrapper">

            <!--**********************************
                Nav header start
            ***********************************-->
            <div class="nav-header">
                <img src="<?= Yii::getAlias('@web'); ?>/assets/probe_imgs/probelogrw.png" style="width: 100%;height: 100%;" alt="">
            </div>
            <!--**********************************
                Nav header end
            ***********************************-->

            <!--**********************************
                Header start
            ***********************************-->
            <div class="header">    
                <div class="header-content clearfix">

                    <div class="nav-control">
                        <div class="hamburger">
                            <span class="toggle-icon"><i class="icon-menu"></i></span>
                        </div>
                    </div>
                    <div class="header-left">
                        <!--
                         <div class="input-group icons">
                             <div class="input-group-prepend">
                                 <span class="input-group-text bg-transparent border-0 pr-2 pr-sm-3" id="basic-addon1"><i class="mdi mdi-magnify"></i></span>
                             </div>
                             <input type="search" class="form-control" placeholder="Search Dashboard" aria-label="Search Dashboard">
                             <div class="drop-down animated flipInX d-md-none">
                                 <form action="#">
                                     <input type="text" class="form-control" placeholder="Search">
                                 </form>
                             </div>
                         </div>-->
                    </div>
                    <div class="header-right">
                        <ul class="clearfix">
                            <!--
                            <li class="icons dropdown"><a href="javascript:void(0)" data-toggle="dropdown">
                                    <i class="mdi mdi-email-outline"></i>
                                    <span class="badge badge-pill gradient-1">3</span>
                                </a>
                                <div class="drop-down animated fadeIn dropdown-menu">
                                    <div class="dropdown-content-heading d-flex justify-content-between">
                                        <span class="">3 New Messages</span>  
                                        <a href="javascript:void()" class="d-inline-block">
                                            <span class="badge badge-pill gradient-1">3</span>
                                        </a>
                                    </div>
                                    <div class="dropdown-content-body">
                                        <ul>
                                            <li class="notification-unread">
                                                <a href="javascript:void()">
                                                    <img class="float-left mr-3 avatar-img" src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/images/probelot_trans_sq.png" alt="">
                                                    <div class="notification-content">
                                                        <div class="notification-heading">Probelot Admin</div>
                                                        <div class="notification-timestamp">08 Hours ago</div>
                                                        <div class="notification-text">Your Application Test is ready.</div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li class="notification-unread">
                                                <a href="javascript:void()">
                                                    <img class="float-left mr-3 avatar-img" src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/images/probelot_trans_sq.png" alt="">
                                                    <div class="notification-content">
                                                        <div class="notification-heading">Problogr You are running out of logr space</div>
                                                        <div class="notification-timestamp">08 Hours ago</div>
                                                        <div class="notification-text">Can you do me a favour?</div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void()">
                                                    <img class="float-left mr-3 avatar-img" src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/images/probelot_trans_sq.png" alt="">
                                                    <div class="notification-content">
                                                        <div class="notification-heading">Probelot Admin</div>
                                                        <div class="notification-timestamp">08 Hours ago</div>
                                                        <div class="notification-text">The your automated test has been setup</div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void()">
                                                    <img class="float-left mr-3 avatar-img" src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/images/probelot_trans_sq.png" alt="">
                                                    <div class="notification-content">
                                                        <div class="notification-heading">Probelot Tech</div>
                                                        <div class="notification-timestamp">08 Hours ago</div>
                                                        <div class="notification-text">We found some faulty integration</div>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </li>
                            -->
                            <!--
                            <li class="icons dropdown">
                                <a href="javascript:void(0)" data-toggle="dropdown">
                                    <i class="mdi mdi-bell-outline"></i>
                                    <span class="badge badge-pill gradient-2">3</span>
                                </a>
                                
                                <div class="drop-down animated fadeIn dropdown-menu dropdown-notfication">
                                    <div class="dropdown-content-heading d-flex justify-content-between">
                                        <span class="">2 New Notifications</span>  
                                        <a href="javascript:void()" class="d-inline-block">
                                            <span class="badge badge-pill gradient-2">5</span>
                                        </a>
                                    </div>
                                    <div class="dropdown-content-body">
                                        <ul>
                                            <li>
                                                <a href="javascript:void()">
                                                    <span class="mr-3 avatar-icon bg-success-lighten-2"><i class="icon-present"></i></span>
                                                    <div class="notification-content">
                                                        <h6 class="notification-heading">Events near you</h6>
                                                        <span class="notification-text">Within next 5 days</span> 
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void()">
                                                    <span class="mr-3 avatar-icon bg-danger-lighten-2"><i class="icon-present"></i></span>
                                                    <div class="notification-content">
                                                        <h6 class="notification-heading">Event Started</h6>
                                                        <span class="notification-text">One hour ago</span> 
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void()">
                                                    <span class="mr-3 avatar-icon bg-success-lighten-2"><i class="icon-present"></i></span>
                                                    <div class="notification-content">
                                                        <h6 class="notification-heading">Event Ended Successfully</h6>
                                                        <span class="notification-text">One hour ago</span>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void()">
                                                    <span class="mr-3 avatar-icon bg-danger-lighten-2"><i class="icon-present"></i></span>
                                                    <div class="notification-content">
                                                        <h6 class="notification-heading">Events to Join</h6>
                                                        <span class="notification-text">After two days</span> 
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </li>
                            
                            -->
                            <li class="icons dropdown">
                                <a href="javascript:void(0)" class="log-user"  data-toggle="dropdown">
                                    <span><?= (Yii::$app->session->get('quixlab_theme', 'light') == "light") ? "Light Theme" : "Dark Theme" ?> <i class="fa icon-d"></i></span>  <i class="fa fa-angle-down f-s-14" aria-hidden="true"></i>
                                </a>
                                <div class="drop-down dropdown-language animated fadeIn  dropdown-menu">
                                    <div class="dropdown-content-body">
                                        <ul>
                                            <li class="theme_selector" theme="light"><a href="javascript:void()">Light Theme</a></li>
                                            <li class="theme_selector" theme="dark"><a href="javascript:void()">Dark Theme</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li class="icons dropdown">
                                <div class="user-img c-pointer position-relative"   data-toggle="dropdown">
                                    <span class="activity active"></span>
                                    <img src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/images/user/1.png" height="40" width="40" alt="">
                                </div>
                                <div class="drop-down dropdown-profile animated fadeIn dropdown-menu">
                                    <div class="dropdown-content-body">
                                        <ul>
                                            <!--   <li>
                                                   <a href="app-profile.html"><i class="icon-user"></i> <span>Profile</span></a>
                                               </li>
                                               <li>
                                                   <a href="javascript:void()">
                                                       <i class="icon-envelope-open"></i> <span>Inbox</span> <div class="badge gradient-3 badge-pill gradient-1">3</div>
                                                   </a>
                                               </li>
   
                                               <hr class="my-2">
                                               <li>
                                                   <a href="page-lock.html"><i class="icon-lock"></i> <span>Lock Screen</span></a>
                                               </li>
                                            -->
                                            <li><a href="<?= Yii::getAlias('@web'); ?>/site/logout"><i class="icon-key"></i> <span>Logout</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--**********************************
                Header end ti-comment-alt
            ***********************************-->

            <!--**********************************
                Sidebar start
            ***********************************-->
            <div class="nk-sidebar">           
                <div class="nk-nav-scroll">
                    <ul class="metismenu" id="menu">
                        <li class="<?= (Yii::$app->controller->id == "site") ? 'active' : '' ?>">
                            <a href="<?= Yii::getAlias('@web'); ?>/dashboard" aria-expanded="false">
                                <i class="icon-speedometer menu-icon"></i><span class="nav-text">Dashboard</span>
                            </a>
                        </li>
                        <li class="<?= (Yii::$app->controller->id == "project") ? 'active' : '' ?>">
                            <a href="<?= Yii::getAlias('@web'); ?>/project" aria-expanded="false">
                                <i class="icon-globe-alt menu-icon"></i><span class="nav-text">Project</span>
                            </a>
                        </li>
                        <li class="<?= (Yii::$app->controller->id == "apps") ? 'active' : '' ?>">
                            <a href="<?= Yii::getAlias('@web'); ?>/apps" aria-expanded="false">
                                <i class="icon-screen-tablet menu-icon"></i><span class="nav-text">Apps</span>
                            </a>
                        </li>
                        <li class="<?= (Yii::$app->controller->id == "team") ? 'active' : '' ?>">
                            <a href="<?= Yii::getAlias('@web'); ?>/team" aria-expanded="false">
                                <i class="icon-people menu-icon"></i><span class="nav-text">Team</span>
                            </a>
                        </li>
                        <!-- <li>
                             <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                                 <i class="icon-handbag menu-icon"></i><span class="nav-text">Departments</span>
                             </a>
                             <ul aria-expanded="false">
                                 <li><a href="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/app-profile.html">Account</a></li>
                             </ul>
                         </li>-->

                        <?php if (app\assets\RoleManagement::checkIfUserHasPrivilege('settings', 'index')) { ?>
                            <li>
                                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                                    <i class="icon-settings menu-icon"></i><span class="nav-text">Setting</span>
                                </a>
                                <ul aria-expanded="false">
                                    <li><a href="<?= Yii::getAlias('@web'); ?>/account">Account</a></li>
                                    <li><a href="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/app-calender.html">Preference</a></li>
                                </ul>
                            </li>
                        <?php } ?>

                        <?php if (app\assets\RoleManagement::checkIfUserHasPrivilege('app-users', 'index')) { ?>
                            <li>
                                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                                    <i class="icon-settings menu-icon"></i><span class="nav-text">Admin</span>
                                </a>
                                <ul aria-expanded="false">
                                    <li><a href="<?= Yii::getAlias('@web'); ?>/roles">Roles</a></li>
                                    <li><a href="<?= Yii::getAlias('@web'); ?>/app-users">App users</a></li>
                                </ul>
                            </li>
                        <?php } ?>


                        <?php if (app\assets\RoleManagement::checkIfUserHasPrivilege('content-blog', 'index') || app\assets\RoleManagement::checkIfUserHasPrivilege('content-video', 'index')) {
                            ?>
                            <li>
                                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                                    <i class="icon-settings menu-icon"></i><span class="nav-text">Content</span>
                                </a>
                                <ul aria-expanded="false">
                                    <?php if (app\assets\RoleManagement::checkIfUserHasPrivilege('content-video', 'index')) { ?>
                                        <li><a href="<?= Yii::getAlias('@web'); ?>/content-video">Video Content</a></li>
                                    <?php } ?>
                                    <?php if (app\assets\RoleManagement::checkIfUserHasPrivilege('content-blog', 'index')) { ?>
                                        <li><a href="<?= Yii::getAlias('@web'); ?>/content-blog">Blog Content</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>

                    </ul>
                </div>
            </div>
            <!--**********************************
                Sidebar end
            ***********************************-->


            <div class="row">
                <div class="col-lg-5 col-md-6">
                    <?php if (Yii::$app->session->hasFlash('success')): ?>
                        <script>
    <?php
    $flashSuccess = Yii::$app->session->getFlash('success');
    if (is_array($flashSuccess)) {
        foreach ($flashSuccess as $value) {
            ?>
                                    toastr.success("<?= $value ?>", "Success", {
                                        "closeButton": true,
                                        "timeOut": "60000",
                                        debug: !1,
                                        newestOnTop: !0,
                                        progressBar: !0,
                                        positionClass: "toast-top-center",
                                        preventDuplicates: !0,
                                        onclick: null,
                                        showDuration: "1000",
                                        hideDuration: "1000",
                                        extendedTimeOut: "1000",
                                        showEasing: "swing",
                                        hideEasing: "linear",
                                        showMethod: "fadeIn",
                                        hideMethod: "fadeOut",
                                        tapToDismiss: !1
                                    });
            <?php
        }
    } else {
        ?>
                                toastr.success("<?= Yii::$app->session->getFlash('success') ?>", "Success", {
                                    "closeButton": true,
                                    "timeOut": "60000",
                                    debug: !1,
                                    newestOnTop: !0,
                                    progressBar: !0,
                                    positionClass: "toast-top-center",
                                    preventDuplicates: !0,
                                    onclick: null,
                                    showDuration: "1000",
                                    hideDuration: "1000",
                                    extendedTimeOut: "1000",
                                    showEasing: "swing",
                                    hideEasing: "linear",
                                    showMethod: "fadeIn",
                                    hideMethod: "fadeOut",
                                    tapToDismiss: !1
                                });
    <?php } ?>
                        </script>
                    <?php endif; ?>





                    <?php if (Yii::$app->session->hasFlash('error')): ?>
                        <script>
    <?php
    $flashError = Yii::$app->session->getFlash('error');
    if (is_array($flashError)) {
        foreach ($flashError as $value) {
            ?>
                                    toastr.error("<?= $value ?>", "Error", {
                                        positionClass: "toast-top-center",
                                        "closeButton": true,
                                        "timeOut": "90000",
                                        closeButton: !0,
                                        debug: !1,
                                        newestOnTop: !0,
                                        progressBar: !0,
                                        preventDuplicates: !0,
                                        onclick: null,
                                        showDuration: "500",
                                        hideDuration: "1000",
                                        extendedTimeOut: "1000",
                                        showEasing: "swing",
                                        hideEasing: "linear",
                                        showMethod: "fadeIn",
                                        hideMethod: "fadeOut",
                                        tapToDismiss: !1
                                    })
            <?php
        }
    } else {
        ?>
                                toastr.error("<?= Yii::$app->session->getFlash('error') ?>", "Error", {
                                    positionClass: "toast-top-center",
                                    "closeButton": true,
                                    "timeOut": "90000",
                                    closeButton: !0,
                                    debug: !1,
                                    newestOnTop: !0,
                                    progressBar: !0,
                                    preventDuplicates: !0,
                                    onclick: null,
                                    showDuration: "500",
                                    hideDuration: "1000",
                                    extendedTimeOut: "1000",
                                    showEasing: "swing",
                                    hideEasing: "linear",
                                    showMethod: "fadeIn",
                                    hideMethod: "fadeOut",
                                    tapToDismiss: !1
                                })
    <?php } ?>
                        </script>
                    <?php endif; ?>



                    <?php if (Yii::$app->session->hasFlash('info')): ?>
                        <script>
    <?php
    $flashInfo = Yii::$app->session->getFlash('info');
    if (is_array($flashInfo)) {
        foreach ($flashInfo as $value) {
            ?>
                                    toastr.error("<?= $value ?>", "Info", {
                                        positionClass: "toast-top-center",
                                        "closeButton": true,
                                        "timeOut": "90000",
                                        closeButton: !0,
                                        debug: !1,
                                        newestOnTop: !0,
                                        progressBar: !0,
                                        preventDuplicates: !0,
                                        onclick: null,
                                        showDuration: "500",
                                        hideDuration: "1000",
                                        extendedTimeOut: "1000",
                                        showEasing: "swing",
                                        hideEasing: "linear",
                                        showMethod: "fadeIn",
                                        hideMethod: "fadeOut",
                                        tapToDismiss: !1
                                    })
            <?php
        }
    } else {
        ?>
                                toastr.info("<?= Yii::$app->session->getFlash('info') ?>", "Info", {
                                    positionClass: "toast-top-center",
                                    "closeButton": true,
                                    "timeOut": "90000",
                                    closeButton: !0,
                                    debug: !1,
                                    newestOnTop: !0,
                                    progressBar: !0,
                                    preventDuplicates: !0,
                                    onclick: null,
                                    showDuration: "500",
                                    hideDuration: "1000",
                                    extendedTimeOut: "1000",
                                    showEasing: "swing",
                                    hideEasing: "linear",
                                    showMethod: "fadeIn",
                                    hideMethod: "fadeOut",
                                    tapToDismiss: !1
                                })
    <?php } ?>
                        </script>
                    <?php endif; ?>

                </div>
            </div>

            <div class="content-body">
                <div class="row page-titles mx-0">
                    <div class="col p-md-0">
                        <?=
                        \yii\widgets\Breadcrumbs::widget([
                            'itemTemplate' => "<li class='breadcrumb-item' ><b>{link}</b></li>\n",
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ])
                        ?>

                    </div>
                </div>
                <!-- row -->
                <?= $content ?>

                <!--**********************************
                Footer start
            ***********************************-->
                <div class="footer">
                    <div class="copyright">
                        <p>Copyright &copy; Probelogr & Probelot Developed by <a href="https://www.uchephilz.com">Uchephilz</a> 2020</p>
                    </div>
                </div>
                <!--**********************************
                    Footer end
                ***********************************-->
            </div>
            <!--**********************************
                Main wrapper end
            ***********************************-->

            <!--**********************************
                Scripts
            ***********************************-->

            <script src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/js/custom.min.js"></script>


            <script src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/js/settings.js"></script>
            <script src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/js/gleek_<?= Yii::$app->session->get('quixlab_theme', 'light') ?>.js"></script>
            <script src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/js/styleSwitcher.js"></script>



            <script>

                            $('.theme_selector').on('click', function () {

                                $.get("<?= Yii::getAlias('@web'); ?>/site/set-theme?theme=" + $(this).attr('theme'), function () {
                                    window.location.reload();
                                })


                            });

            </script>

            <?php
            if ((Yii::$app->controller->id == 'content-blog' || Yii::$app->controller->action->id == 'create' || Yii::$app->controller->action->id == 'update')) {
                ?>
                <script>
                    $(document).ready(function () {
                        Simditor.locale = 'en-US';
                        var editor = new Simditor({
                            textarea: $('#contentblog-blog_content'),
                            pasteImage: true,
                            allowedTags:['iframe'],
                            allowedAttributes:{
                                iframe:['width','height','src','title','frameborder','allow','allowfullscreen']
                            },
                            toolbar: [
                                'title',
                                'bold',
                                'italic',
                                'underline',
                                'strikethrough',
                                'fontScale',
                                'color',
                                'ol',
                                'ul',
                                'blockquote',
                                'code',
                                'table',
                                'link',
                                'image',
                                'hr',
                                'indent',
                                'outdent',
                                'alignment'

                            ]
                        });
                    }
                    );
                </script>

            <?php } ?>

            <script>
                $(function () {
                    // Replace the <textarea id="editor1"> with a CKEditor
                    // instance, using default configuration.

                    //bootstrap WYSIHTML5 - text editor
                    // $('#contentblog-blog_content').wysihtml5()
                })



            </script>



    </body>


</html>
