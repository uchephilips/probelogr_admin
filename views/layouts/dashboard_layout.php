<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

$usrData = app\assets\SessionAssets::getUserSession();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>


<!--

=========================================================
* Argon Dashboard - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <?php $this->registerCsrfMetaTags() ?>
        <title>Bold Transaction | <?= Html::encode($this->title) ?></title>
        <!-- Favicon -->
        <link href="<?= Yii::getAlias('@web'); ?>/assets/theme_assets/img/brand/btlogosym.png" rel="icon" type="image/png">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <!-- Icons -->
        <link href="<?= Yii::getAlias('@web'); ?>/assets/theme_assets/js/plugins/nucleo/css/nucleo.css" rel="stylesheet" />
        <link href="<?= Yii::getAlias('@web'); ?>/assets/theme_assets/js/plugins/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet" />
        <!-- CSS Files -->
        <link href="<?= Yii::getAlias('@web'); ?>/assets/theme_assets/css/argon-dashboard.css?v=1.1.0" rel="stylesheet" />

        <link rel="stylesheet" href="<?= Yii::getAlias('@web'); ?>/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

        <script src="<?= Yii::getAlias('@web'); ?>/assets/theme_assets/js/plugins/jquery/dist/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?= Yii::getAlias('@web'); ?>/assets/taginput/css/amsify.suggestags.css">

        <script src="//code.tidio.co/ov7ww7jdcyxkxyyopvjyvlctqwvprion.js" async></script>
        <!-- Global site tag (gtag.js) - Google Ads: 854959494 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-854959494"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'AW-854959494');
        </script>


        <!-- Amsify Plugin -->
        <script type="text/javascript" src="<?= Yii::getAlias('@web'); ?>/assets/taginput/js/jquery.amsify.suggestags.js"></script>



        <style>
            .uploadLogo{
                max-width:180px;
                max-height:80px;
            }
        </style>

    </head>

    <body class="">
        <nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
            <div class="container-fluid">
                <!-- Toggler -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <!-- Brand -->
                <a class="navbar-brand pt-0" href="<?= Yii::getAlias('@web'); ?>/site">
                    <img src="<?= Yii::getAlias('@web'); ?>/assets/theme_assets/img/brand/btlogo.png" class="navbar-brand-img" alt="Bold Transaction Logo">
                </a>
                <!-- User -->
                <ul class="nav align-items-center d-md-none">
                    <li class="nav-item dropdown">
                        <a class="nav-link nav-link-icon" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ni ni-bell-55"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">

                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="media align-items-center">
                                <span class="avatar avatar-sm rounded-circle">
                                    <!--<img alt="Image placeholder" src="<?= Yii::getAlias('@web'); ?>/assets/theme_assets/img/theme/team-1-800x800.jpg">-->
                                </span>
                            </div>
                        </a>
                        <?= $this->render('_dropdownmenu') ?>
                    </li>
                </ul>
                <!-- Collapse -->
                <?= $this->render('_nav_menu') ?>
            </div>
        </nav>
        <div class="main-content">
            <!-- Navbar -->
            <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
                <div class="container-fluid">
                    <!-- Brand -->
                    <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="<?= Yii::getAlias('@web'); ?>/site"><?= $this->title ?></a>
                    <!-- Form -->

                    <!-- User -->
                    <ul class="navbar-nav align-items-center d-none d-md-flex">
                        <li class="nav-item dropdown">
                            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="media align-items-center">
                                    <span class="avatar avatar-sm rounded-circle bg-success">
                                        <!-- <img alt="Image placeholder" src="<?= Yii::getAlias('@web'); ?>/assets/theme_assets/img/theme/team-4-800x800.jpg">-->
                                    </span>
                                    <div class="media-body ml-2 d-none d-lg-block">
                                        <span class="mb-0 text-sm  font-weight-bold"><?= Yii::$app->user->identity->fullname ?></span>
                                    </div>
                                </div>
                            </a>
                            <?= $this->render('_dropdownmenu') ?>
                        </li>
                    </ul>
                </div>
            </nav>



            <?= $content ?>
        </div>
    </div>
    <!--   Core   -->
    <script src="<?= Yii::getAlias('@web'); ?>/assets/theme_assets/js/plugins/jquery/dist/jquery.min.js"></script>
    <script src="<?= Yii::getAlias('@web'); ?>/assets/theme_assets/js/plugins/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

    <script src="//code.tidio.co/cxypichfgtgbdxjq8dgntjkuk0zyduj7.js" async></script>

    <!--   Optional JS   -->
    <script src="<?= Yii::getAlias('@web'); ?>/assets/theme_assets/js/plugins/chart.js/dist/Chart.min.js"></script>
    <script src="<?= Yii::getAlias('@web'); ?>/assets/theme_assets/js/plugins/chart.js/dist/Chart.extension.js"></script>
    <!--   Argon JS   -->
    <script src="<?= Yii::getAlias('@web'); ?>/assets/theme_assets/js/argon-dashboard.js"></script>

    <script src="<?= Yii::getAlias('@web'); ?>/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script src="<?= Yii::getAlias('@web'); ?>/assets/bower_components/ckeditor/ckeditor.js"></script>
    <script>
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1')
        //bootstrap WYSIHTML5 - text editor
        $('.textarea').wysihtml5()
    })



    </script>

    <script>

        $(document).ready(function () {



            $('.cms-ims').fadeIn('slow', function () {
                var imsUrl = $(this).attr('img-data');
                var imsId = $(this).attr('id');
                $.get(imsUrl, function (data) {
                    //console.log(data);
                    if (data != 0) {
                        //alert('not null')
                        document.getElementById(imsId).src = data;

                    } else {
                        document.getElementById(imsId).src = 'https://via.placeholder.com/800x800.png?text=img';
                    }
                });
            });

            $('#sharebtn').click(function () {


                if (navigator.share) {
                    navigator.share({
                        title: 'Web Fundamentals',
                        text: 'Check out Web Fundamentals — it rocks!',
                        url: 'https://developers.google.com/web',
                    })
                            .then(() => console.log('Successful share'))
                            .catch((error) => console.log('Error sharing', error));
                } else {
                    alert("Couldn't share");
                }

            });



        });
    </script>

</body>

</html>
