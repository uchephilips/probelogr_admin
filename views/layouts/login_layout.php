<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
?>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <?php $this->registerCsrfMetaTags() ?>
        <title>Probelogr | <?= Html::encode($this->title) ?></title>
        <!-- FAVICON -->
        <link rel="icon" href="<?= Yii::getAlias('@web'); ?>/assets/probe_imgs/logo_fav.jpg" type="image/x-icon">
        <link rel="shortcut icon" href="<?= Yii::getAlias('@web'); ?>/assets/probe_imgs/logo_fav.jpg" type="image/x-icon">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
        <link href="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/css/style.css" rel="stylesheet">

        <script src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/plugins/common/common.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/plugins/toastr/js/toastr.min.js"></script>
    </head>
    <body class="h-100">

        <!--*******************
            Preloader start
        ********************-->
        <div id="preloader">
            <div class="loader">
                <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
                </svg>
            </div>
        </div>
        <!--*******************
            Preloader end
        ********************-->
        <?= $content ?>



        <!--**********************************
        Scripts
    ***********************************-->



        <script src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/js/custom.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/js/settings.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/js/gleek_<?= Yii::$app->session->get('quixlab_theme', 'light') ?>.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/quixlab_theme/js/styleSwitcher.js"></script>
    </body>
</html>
