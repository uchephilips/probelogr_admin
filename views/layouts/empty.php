<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="<?= Yii::getAlias('@web'); ?>/assets/theme_assets/img/brand/btlogosym.png" rel="icon" type="image/png">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <?php $this->registerCsrfMetaTags() ?>
        <title>Bold Transaction | <?= Html::encode($this->title) ?></title>

        <!-- Icon css link -->
        <link href="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/css/font-awesome.min.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/css/bootstrap.min.css" rel="stylesheet">

        <!-- Rev slider css -->
        <link href="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/vendors/revolution/css/navigation.css" rel="stylesheet">

        <!-- Extra plugin css -->
        <link href="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">

        <link href="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/css/style.css" rel="stylesheet">
        <link href="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/css/responsive.css" rel="stylesheet">

        <!-- Global site tag (gtag.js) - Google Ads: 854959494 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-854959494"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-854959494');
</script>

        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <!--================Header Menu Area =================-->
        <header class="main_menu_area banner_area">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="<?= Yii::getAlias('@web'); ?>"><img src="<?= Yii::getAlias('@web'); ?>/assets/theme_assets/img/brand/btlogo.png" style="width: 150px" alt="Bold Transaction Logo"></a>
               <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item active"><a class="nav-link" href="<?= Yii::getAlias('@web'); ?>">Make Complaint</a></li>
                    </ul>
                </div>-->
            </nav>
        </header>
        <!--================End Header Menu Area =================-->

        <?= $content ?>

        <!--================Footer Area =================-->
        <footer class="footr_area">

            <div class="footer_copyright">
                <div class="container">
                    <div class="float-sm-left">
                        <h5><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> 
                            All rights reserved With <i class="fa fa-heart-o" aria-hidden="true"></i> by <br><a href="javascript:;" target="_blank">Bold Transaction</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></h5>
                    </div>
                </div>
            </div>
        </footer>
        <!--================End Footer Area =================-->




        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/js/jquery-3.2.1.min.js"></script>
        
        <script src="//code.tidio.co/cxypichfgtgbdxjq8dgntjkuk0zyduj7.js" async></script>
        
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/js/popper.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/js/bootstrap.min.js"></script>
        <!-- Rev slider js -->
        <script src="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <!-- Extra plugin css -->
        <script src="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/vendors/counterup/jquery.waypoints.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/vendors/counterup/jquery.counterup.min.js"></script> 
        <script src="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/vendors/counterup/apear.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/vendors/counterup/countto.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/vendors/magnify-popup/jquery.magnific-popup.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/js/smoothscroll.js"></script>

        <script src="<?= Yii::getAlias('@web'); ?>/assets/serria_theme/js/theme.js"></script>


        <script>

                                $(document).ready(function () {
                                    $('.cms-ims').fadeIn('slow', function () {
                                        var imsUrl = $(this).attr('img-data');
                                        var imsId = $(this).attr('id');
                                        $.get(imsUrl, function (data) {
                                            //console.log(data);
                                            if (data != 0) {
                                                //alert('not null')
                                                document.getElementById(imsId).src = data;

                                            } else {
                                                document.getElementById(imsId).src = 'https://via.placeholder.com/800x800.png?text=img';
                                            }
                                        });
                                    });
                                });
        </script>

    </body>
</html>