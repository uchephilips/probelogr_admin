<?php

use yii\helpers\Html;
?>

<!DOCTYPE html>
<html lang="en">


    <!-- Mirrored from validthemes.online/themeforest/anada/index-4.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 04 Oct 2021 18:27:37 GMT -->
    <head>
        <!-- ========== Meta Tags ========== -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Probelogr is the powerful platform for logging and software data analysis">

        <meta property="og:title" content="Probelogr is the powerful platform for logging and software data analysis"/>
        <meta property="og:image" content="<?= Yii::getAlias('@web'); ?>/assets/logo_fav.jpg"/>
        <meta property="og:url" content="https://www.probelogr.com"/>
        <meta name="twitter:card" content="summary_large_image"/>
        <meta property="og:site_name" content="Probelogr - is the powerful platform for logging and software data analysis">
        <meta name="twitter:image:alt" content="https://www.probelogr.com">
        <meta name="description" content="Probelogr is the powerful platform for logging and software data analysis">

        <!-- ========== Page Title ========== -->
        <title>Probelogr</title>

        <!-- ========== Favicon Icon ========== -->
        <link rel="icon" href="<?= Yii::getAlias('@web'); ?>/assets/probe_imgs/logo_fav.jpg" type="image/x-icon">
        <link rel="shortcut icon" href="<?= Yii::getAlias('@web'); ?>/assets/probe_imgs/logo_fav.jpg" type="image/x-icon">

        <!-- ========== Start Stylesheet ========== -->
        <link href="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/css/font-awesome.min.css" rel="stylesheet" />
        <link href="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/css/themify-icons.css" rel="stylesheet" />
        <link href="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/css/flaticon-set.css" rel="stylesheet" />
        <link href="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/css/magnific-popup.css" rel="stylesheet" />
        <link href="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/css/owl.carousel.min.css" rel="stylesheet" />
        <link href="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/css/owl.theme.default.min.css" rel="stylesheet" />
        <link href="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/css/animate.css" rel="stylesheet" />
        <link href="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/css/bootsnav.css" rel="stylesheet" />
        <link href="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/style.css" rel="stylesheet">
        <link href="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/css/responsive.css" rel="stylesheet" />
        <!-- ========== End Stylesheet ========== -->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/js/html5/html5shiv.min.js"></script>
          <script src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/js/html5/respond.min.js"></script>
        <![endif]-->

        <!-- ========== Google Fonts ========== -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;600;700;800&amp;display=swap" rel="stylesheet">

    </head>

    <body>

        <!-- Preloader Start -->
        <div class="se-pre-con"></div>
        <!-- Preloader Ends -->

        <!-- Header 
        ============================================= -->
        <header id="home">

            <!-- Start Navigation -->
            <nav class="navbar navbar-default transparent navbar-fixed navbar-transparent white bootsnav">

                <div class="container-full">

                    <!-- Start Atribute Navigation -->
                    <div class="attr-nav">
                        <ul>
                            <li class="contact">
                                <i class="flaticon-email"></i> info@probelogr.com
                            </li>
                        </ul>
                    </div>        
                    <!-- End Atribute Navigation -->

                    <!-- Start Header Navigation -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand" href="index-2.html">
                            <img src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/img/logo-light.png" class="logo logo-display" alt="Logo">
                            <img src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/img/logo.png" class="logo logo-scrolled" alt="Logo">
                        </a>
                    </div>
                    <!-- End Header Navigation -->

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="navbar-menu">
                        <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
                            <li>
                                <a href="<?= Yii::getAlias('@web'); ?>/site/">Home</a>
                            </li>
                            <li>
                                <a href="<?= Yii::getAlias('@web'); ?>/blogs/">Support Blog</a>
                            </li>
                            <li>
                                <a href="<?= Yii::getAlias('@web'); ?>/videos/">How-To Videos</a>
                            </li>

                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>

            </nav>
            <!-- End Navigation -->

        </header>
        <!-- End Header -->


        <?= $content ?>


        <!-- Star Footer
       ============================================= -->
        <footer>
            <div class="container">
                <div class="f-items default-padding">
                    <div class="row">
                        <div class="equal-height col-lg-4 col-md-6 item">
                            <div class="f-item about">
                                <img src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/img/logo.png" alt="Logo">
                                <p>
                                    Probelogr - is the powerful platform for logging and software data analysis
                                </p>
                            </div>
                        </div>


                        <div class="equal-height col-lg-4 col-md-6 item">
                            <div class="f-item contact">
                                <h4 class="widget-title">Contact Info</h4>
                                <p>
                                    Feel free to contact us via email
                                </p>
                                <div class="address">
                                    <ul>
                                        <li>
                                            <strong>Email:</strong> info@probelogr.com
                                        </li>
                                    </ul>
                                </div>
                                <ul class="social">
                                    <li class="youtube">
                                        <a href="#"><i class="fab fa-youtube"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom">
                    <div class="row">
                        <div class="col-lg-6">
                            <p>&copy; Copyright 2019. All Rights Reserved by <a href="https://www.linkedin.com/in/uchephilz/">Uche Powers</a></p>
                        </div>
                        <div class="col-lg-6 text-right link">
                            <ul>
                                <li>
                                    <a href="#">Terms</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Shape -->
            <div class="footer-shape" style="background-image: url(<?= Yii::getAlias('@web'); ?>/assets/anada_theme/img/shape/1.svg);"></div>
            <!-- End Shape -->
        </footer>
        <!-- End Footer-->

        <!-- jQuery Frameworks
        ============================================= -->
        <script src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/js/jquery-1.12.4.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/js/popper.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/js/bootstrap.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/js/equal-height.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/js/jquery.appear.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/js/jquery.easing.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/js/jquery.magnific-popup.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/js/modernizr.custom.13711.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/js/owl.carousel.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/js/wow.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/js/progress-bar.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/js/isotope.pkgd.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/js/imagesloaded.pkgd.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/js/count-to.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/js/YTPlayer.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/js/circle-progress.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/js/bootsnav.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/js/main.js"></script>

    </body>

    <!-- Mirrored from validthemes.online/themeforest/anada/index-4.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 04 Oct 2021 18:27:56 GMT -->
</html>