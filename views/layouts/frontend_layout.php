<?php

use yii\helpers\Html;
?>

<html lang="en">
    <head>

        <!-- SITE TITTLE -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <meta property="og:title" content="Probelogr Is a Powerful Software insight tool."/>
        <meta property="og:image" content="<?= Yii::getAlias('@web'); ?>/assets/logo_fav.jpg"/>
        <meta property="og:url" content="https://www.probelogr.com"/>
        <meta name="twitter:card" content="summary_large_image"/>
        <meta property="og:site_name" content="Probelogr - Probelogr can be used to log large amount a data per second and analyzes that data to provide insight into your software.">
        <meta name="twitter:image:alt" content="https://www.probelogr.com">
        <meta name="description" content="Probelogr can be used to log large amount a data per second and analyzes that data to provide insight into">

        <?php $this->registerCsrfMetaTags() ?>
        <title>Probelogr | <?= Html::encode($this->title) ?></title>

        <!-- PLUGINS CSS STYLE -->
        <!-- Bootstrap -->
        <link href="<?= Yii::getAlias('@web'); ?>/assets/frontend/plugins/bootstrap/bootstrap.min.css" rel="stylesheet">
        <!-- themify icon -->
        <link rel="stylesheet" href="<?= Yii::getAlias('@web'); ?>/assets/frontend/plugins/themify-icons/themify-icons.css">
        <!-- Slick Carousel -->
        <link href="<?= Yii::getAlias('@web'); ?>/assets/frontend/plugins/slick/slick.css" rel="stylesheet">
        <!-- Slick Carousel Theme -->
        <link href="<?= Yii::getAlias('@web'); ?>/assets/frontend/plugins/slick/slick-theme.css" rel="stylesheet">

        <!-- CUSTOM CSS -->
        <link href="<?= Yii::getAlias('@web'); ?>/assets/frontend/css/style.css" rel="stylesheet">

        <!-- FAVICON -->
        <link rel="icon" href="<?= Yii::getAlias('@web'); ?>/assets/probe_imgs/logo_fav.jpg" type="image/x-icon">
        <link rel="shortcut icon" href="<?= Yii::getAlias('@web'); ?>/assets/probe_imgs/logo_fav.jpg" type="image/x-icon">

        <script src="//code.tidio.co/ov7ww7jdcyxkxyyopvjyvlctqwvprion.js" async></script>
        
        <script>

            window.onload = function () {
                var loadTime = window.performance.timing.domContentLoadedEventEnd - window.performance.timing.navigationStart;
                console.log('Page load time is ' + loadTime);
                console.log(convertMillisecToHrMinSec(loadTime));
            }

            
            var convertMillisecToHrMinSec = (time) => {
                let date = new Date(time);
                let hr = date.getHours();
                let min = date.getMinutes();
                let sec = date.getSeconds();

                hr = (hr < 10) ? "0" + hr : hr;
                min = (min < 10) ? "0" + min : min;
                sec = (sec < 10) ? "0" + sec : sec;

                return hr + 'hr :' + min + ":mins" + sec+" scs";//01:27:55
            }
            

        </script>

    </head>

    <body class="body-wrapper" style="    width: 100%;">


        <nav class="navbar main-nav fixed-top navbar-expand-lg large">
            <div class="container">
                <a class="navbar-brand" href="<?= Yii::getAlias('@web'); ?>/site/index"><img src="<?= Yii::getAlias('@web'); ?>/assets/probe_imgs/probelogrw.png" alt="probelogr logo" height="80px"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="ti-menu text-white"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link scrollTo" href="#home">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link scrollTo" href="#feature">Features</a>
                        </li>
                        <!--
                        <li class="nav-item">
                            <a class="nav-link scrollTo" href="#pricing">Pricing</a>
                        </li>
                        -->

                    </ul>
                </div>
            </div>
        </nav>
        <?= $content ?>
        <!-- JAVASCRIPTS -->


        <script src="<?= Yii::getAlias('@web'); ?>/assets/frontend/plugins/jquery/jquery.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/frontend/plugins/bootstrap/bootstrap.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/frontend/plugins/slick/slick.min.js"></script>
        <script src="<?= Yii::getAlias('@web'); ?>/assets/frontend/js/custom.js"></script>

    </body>

</html>