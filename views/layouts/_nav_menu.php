<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
?>

<div class="collapse navbar-collapse" id="sidenav-collapse-main">
    <!-- Collapse header -->
    <div class="navbar-collapse-header d-md-none">
        <div class="row">
            <div class="col-6 collapse-brand">
                <a href="<?= Yii::getAlias('@web'); ?>">
                    <img src="<?= Yii::getAlias('@web'); ?>/assets/theme_assets/img/brand/bt_logo_blue.png">
                </a>
            </div>
            <div class="col-6 collapse-close">
                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                    <span></span>
                    <span></span>
                </button>
            </div>
        </div>
    </div>
    <!-- Form -->

    <!-- Navigation -->

    <!-- Divider -->
    <hr class="my-3">
    <!-- Heading -->


    <?php
    if (app\assets\RoleManagement::checkIfUserHasPrivilege('dashboard', 'index') && app\assets\RoleManagement::checkIfUserHasPrivilege('bank-details-transaction', 'index') && app\assets\RoleManagement::checkIfUserHasPrivilege('transaction', 'index') && app\assets\RoleManagement::checkIfUserHasPrivilege('profile', 'index')
    ) {
        ?>
        <h6 class="navbar-heading text-muted">Transactions</h6>
    <?php } ?>

    <ul class="navbar-nav">
        <?php if (app\assets\RoleManagement::checkIfUserHasPrivilege('dashboard', 'index')) { ?>
            <li class="nav-item">
                <a class="nav-link " href="<?= Yii::getAlias('@web'); ?>/dashboard">
                    <i class="ni ni-building text-info"></i> Transaction Dashboard
                    
                </a>
            </li>
        <?php } ?>
        <?php if (app\assets\RoleManagement::checkIfUserHasPrivilege('transaction', 'index')) { ?>
            <li class="nav-item">
                <a class="nav-link " href="<?= Yii::getAlias('@web'); ?>/transaction">
                    <i class="ni ni-money-coins text-green"></i> 
                    Transactions
                </a>
            </li>
        <?php } ?>
        <?php if (app\assets\RoleManagement::checkIfUserHasPrivilege('bank-details-transaction', 'index')) { ?>
            <li class="nav-item">
                <a class="nav-link " href="<?= Yii::getAlias('@web'); ?>/bank-details-transaction">
                    <i class="ni ni-planet text-blue"></i> Your Banks
                </a>
            </li>
        <?php } ?>
        
        <?php if (app\assets\RoleManagement::checkIfUserHasPrivilege('profile', 'index')) { ?>
            <li class="nav-item">
                <a class="nav-link " href="<?= Yii::getAlias('@web'); ?>/profile">
                    <i class="ni ni-single-02 text-yellow"></i> Your profile
                </a>
            </li>
        <?php } ?>
    </ul>
    <!-- Divider -->
    <hr class="my-3">
    <!-- Heading -->
    <?php if (app\assets\RoleManagement::checkIfUserHasPrivilege('problem-report', 'index') && app\assets\RoleManagement::checkIfUserHasPrivilege('help', 'index')) { ?>
        <h6 class="navbar-heading text-muted">Support</h6>
    <?php } ?>

    <!-- Navigation -->
    <ul class="navbar-nav mb-md-3">

        <?php if (app\assets\RoleManagement::checkIfUserHasPrivilege('sales-target', 'dashboard')) { ?>
            <li class="nav-item">
                <a class="nav-link" href="#l">
                    <i class="ni ni-spaceship text-blue"></i> Contact Bold Transaction
                </a>
            </li>
        <?php } ?>
        <?php if (app\assets\RoleManagement::checkIfUserHasPrivilege('problem-report', 'index')) { ?>
            <li class="nav-item">
                <a class="nav-link" href="<?= Yii::getAlias('@web'); ?>/problem-report">
                    <i class="ni ni-support-16 text-warning"></i> Report A Problem
                </a>
            </li>
        <?php } ?>
        <?php if (app\assets\RoleManagement::checkIfUserHasPrivilege('help', 'index')) { ?>
            <li class="nav-item">
                <a class="nav-link" href="<?= Yii::getAlias('@web'); ?>/support-blog" target="_blank">
                    <i class="ni ni-ungroup text-primary"></i> Support & How To
                </a>
            </li>
        <?php } ?>
    </ul>
    <!-- Divider -->
    <hr class="my-3">
    <!-- Heading -->
    <?php
    if (app\assets\RoleManagement::checkIfUserHasPrivilege('roles', 'index') && app\assets\RoleManagement::checkIfUserHasPrivilege('app-users', 'index') && app\assets\RoleManagement::checkIfUserHasPrivilege('cms-terms-and-conditions', 'index')) {
        ?>
        <h6  class="navbar-heading text-muted">Administration</h6>
    <?php } ?>

    <!-- Navigation -->
    <ul class="navbar-nav mb-md-3">
        <?php if (app\assets\RoleManagement::checkIfUserHasPrivilege('roles', 'index')) { ?>
            <li class="nav-item">
                <a class="nav-link" href="<?= Yii::getAlias('@web'); ?>/roles">
                    <i class="ni ni-archive-2"></i> Roles
                </a>
            </li>
        <?php } ?>
        <?php if (app\assets\RoleManagement::checkIfUserHasPrivilege('app-users', 'index')) { ?>
            <li class="nav-item">
                <a class="nav-link" href="<?= Yii::getAlias('@web'); ?>/app-users">
                    <i class="ni ni-archive-2"></i> Platform Users
                </a>
            </li>
        <?php } ?>
        <?php if (app\assets\RoleManagement::checkIfUserHasPrivilege('cms-terms-and-conditions', 'index')) { ?>
            <li class="nav-item">
                <a class="nav-link" href="<?= Yii::getAlias('@web'); ?>/cms-terms-and-conditions">
                    <i class="ni ni-archive-2"></i>CMS Terms and Conditions
                </a>
            </li>
        <?php } ?>
        <?php if (app\assets\RoleManagement::checkIfUserHasPrivilege('cms-support-blog', 'index')) { ?>
            <li class="nav-item">
                <a class="nav-link" href="<?= Yii::getAlias('@web'); ?>/cms-support-blog">
                    <i class="ni ni-archive-2"></i>CMS Support & Blog
                </a>
            </li>
        <?php } ?>
        <?php if (app\assets\RoleManagement::checkIfUserHasPrivilege('cms-images', 'index')) { ?>
            <li class="nav-item">
                <a class="nav-link" href="<?= Yii::getAlias('@web'); ?>/cms-images">
                    <i class="ni ni-archive-2"></i>CMS Images
                </a>
            </li>
        <?php } ?>
        <?php if (app\assets\RoleManagement::checkIfUserHasPrivilege('cms-header-slider', 'index')) { ?>
            <li class="nav-item">
                <a class="nav-link" href="<?= Yii::getAlias('@web'); ?>/cms-header-slider">
                    <i class="ni ni-archive-2"></i>CMS Header Slider
                </a>
            </li>
        <?php } ?>
    </ul>
</div>