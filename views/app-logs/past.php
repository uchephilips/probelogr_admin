<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Apps */
/* @var $endpoint app\models\AppsEndpoints */
/* @var $tags app\models\AppsTags[] */

$this->title = $model->name . ' |  Past Logs';
$this->params['breadcrumbs'][] = ['label' => 'Apps', 'url' => ['index'],];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['logs', 'id' => $model->id],];
?>

<script src="<?= Yii::getAlias('@web'); ?>/assets/ws/sockjs.min.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/assets/ws/stomp.min.js"></script>

<style>
    .logPane{
        padding-bottom: 10px;
        padding-top: 10px;
    }
    .logTopBorder{
        border-top: 1px solid #adaeb1;
        font-weight: initial;
    }
</style>

<!--**********************************
          Content body start
      ***********************************-->

<div class="container">
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h2 class="card-ttle"><?php if ($shared) { ?>
                            <i class="icon-screen-desktop gradient-9-text"></i>
                        <?php } else { ?>
                            <i class="icon-screen-desktop gradient-3-text"></i>
                        <?php } ?><?= $this->title ?></h2>
                    <div class="col text-right">
                        <?php if ($shared) { ?>
                            <a href="<?= Yii::getAlias('@web'); ?>/apps/sharedapps" class="btn btn-lg btn-primary">Back <i class="fa fa-backward"></i></a>
                        <?php } else { ?>
                            <a href="<?= Yii::getAlias('@web'); ?>/apps/appinfo?app=<?= $model->id ?>" class="btn btn-lg btn-primary">Back <i class="fa fa-backward"></i></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-12">
            <div class="card"> 
                <div class="card-header" >
                    <div class="form-row align-items-center" style="float: right;">
                        <div class="col-auto">
                            <label class="sr-only">Size</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Size</div>
                                </div>
                                <select required class="form-cntrol" id="pageSize">
                                    <option value="10">10</option>
                                    <option value="100">100</option>
                                    <option value="200">200</option>
                                    <option value="400">400</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-auto">
                            <label class="sr-only">Size</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Detail</div>
                                </div>
                                <select required class="form-cntrol" id="viewDetail">
                                    <option value="full">Full</option>
                                    <option value="simple">Simple</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-auto">
                            <label class="sr-only">View</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">View</div>
                                </div>
                                <select required class="form-cntrol" id="logView">
                                    <option value="grid">GRID</option>
                                    <option value="list">LIST</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-auto">
                            <label class="sr-only">Tags</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Tags</div>
                                </div>
                                <select name="app_tags" required class="form-cntrol" id="apptags">
                                    <?php foreach ($tags as $tag) { ?>
                                        <option><?= $tag->tags ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-auto">
                            <label class="sr-only">Username</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Date</div>
                                </div>
                                <select class="custom-select" name="ymd" id="ymd">
                                    <?php foreach ($ymds as $ymd) { ?>
                                        <option><?= $ymd->ymd ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-auto">
                            <button type="submit" id="search" class="btn btn-dark btn-sm mb-2">Search</button>
                        </div>
                    </div>

                </div>

                <div class="card-body" id="activity">
                    <div id="logs" class="gridviewC"></div>
                    <div id="logs2" style="display: none;" class="listviewC"></div>

                </div>
                <div class="bootstrap-pagination" id="pagin" >
                    <nav>
                        <ul class="pagination justify-content-center">
                            <li class="page-item col-md-12 text-center">
                                <a class="page-link" id="nextlogs" style="display: none;" href="javascript:;">Next</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- #/ container -->
<!--**********************************
    Content body end
***********************************-->

<div style="display: none;">

    <div class="media-body" >

        <blockquote id="newTemp" class="logPane">
            <span class="label btn-rounded fulldetail"  id="tagr"></span>
            <small class="label btn-rounded fulldetail" id="timestamp"></small>
            <div id="tagbody" class="logTopBorder"></div>
        </blockquote>
        <div id="tempG" >
            <strong class="fulldetail" id="tagr2"></strong>
            <strong class="fulldetail" id="timestamp2"></strong>
            <span id="tagbody2"></span>
        </div>

    </div>
</div>

<script>

    var page = 1;
    var size = 0;
    var app = <?= $model->id ?>;
    var totalLogsCount = 0;

    var pagin = $('#pagin');
    var pagin = $('#pageSize');

    function loadLogs(ymd, tag, page) {

        //size = $('#pageSize').val();

        $.getJSON("<?= Yii::getAlias('@web'); ?>/app-logs/search-logs?id=" + app + '&ymd=' + ymd + '&tag=' + tag + '&page=' + page + '&size=' + size + "&<?= $shared ? 'shared' : '' ?>", function (data) {

            if (data.logs.length < size) {
                $('#nextlogs').hide();
            } else {
                $('#nextlogs').show();
            }


            totalLogsCount = data.count;

            $.each(data.logs, function (index, log) {

                var temp = $('#newTemp');
                var temp2 = $('#tempG');
                $clone = temp.clone();
                $clone2 = temp2.clone();

                $clone.find('#tagr').html(log.tags);
                $clone2.find('#tagr2').html(log.tags);

                $clone.find('#timestamp').html(log.ts);
                $clone2.find('#timestamp2').html(log.ts);

                $clone.find('#tagbody').html(log.body);
                $clone2.find('#tagbody2').html(log.body);

                $("#logs").append($clone);
                $("#logs2").append($clone2);



            });
            $("#logs").append("<hr style='border: 2px solid #848491;'>");
            $("#logs2").append("<hr style='border: 2px solid #848491;'>");
        });
    }

    $('#nextlogs').on('click', function () {
        page = parseInt(page) + parseInt(size);
        console.log(page);
        var tag = $('#apptags').val();
        var ymd = $('#ymd').val();

        loadLogs(ymd, tag, page);

    });

    $('#search').on('click', function () {
        page = 1;
        size = $('#pageSize').val();
        $("#logs").html("");
        $("#logs2").html("");

        var tag = $('#apptags').val();
        var ymd = $('#ymd').val();

        loadLogs(ymd, tag, page);


    });



    $('#viewDetail').on('change', function () {
        if ($(this).val() == 'full') {
            $('.fulldetail').show();
        } else {
            $('.fulldetail').hide();
        }
    });


    $('#logView').on('change', function () {
        if ($(this).val() == 'grid') {
            $('#logs').show();
            $('#logs2').hide();
        } else {
            $('#logs').hide();
            $('#logs2').show();
        }
    });

    $('#activity').slimscroll({
        position: "right",
        size: "5px",
        height: "700px",
        color: "#7571f9",
        alwaysVisible: true
    });



</script>