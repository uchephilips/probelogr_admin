<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Apps */
/* @var $endpoint app\models\AppsEndpoints */
/* @var $tag string[] */

$this->title = $model->name . ' |  Real-time Logs';
$this->params['breadcrumbs'][] = ['label' => 'Apps', 'url' => ['index'],];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['logs', 'id' => $model->id],];
?>

<script src="<?= Yii::getAlias('@web'); ?>/assets/ws/sockjs.min.js"></script>
<script src="<?= Yii::getAlias('@web'); ?>/assets/ws/stomp.min.js"></script>

<style>
    .logPane{
        padding-bottom: 10px;
        padding-top: 10px;
    }
    .logTopBorder{
        border-top: 1px solid #adaeb1;
        font-weight: initial;
    }
</style>

<!--**********************************
          Content body start
      ***********************************-->

<div class="container">
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h2 class="card-ttle"><?php if ($shared) { ?>
                            <i class="icon-screen-desktop gradient-9-text"></i>
                        <?php } else { ?>
                            <i class="icon-screen-desktop gradient-3-text"></i>
                        <?php } ?><?= $this->title ?></h2>
                    <div class="col text-right">
                        <a href="<?= Yii::getAlias('@web'); ?>/apps/appinfo?app=<?= $model->id ?>" class="btn btn-lg btn-primary">Back <i class="fa fa-backward"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-12">
            <div class="card"> 
                <div class="card-header" >
                    <select id="logView" style="float:right;">
                        <option value="grid">GRID</option>
                        <option value="list">LIST</option>
                    </select>
                </div>
                <div class="card-body pb-0 d-flex justify-content-between">
                    <div>
                        <h4 class="mb-1">
                            Active Logs <i class="badge badge-pill badge-light" id="live_status" style="font-size: 50%;">loading...</i></h4>
                        <h6 class="m-0" id="liveLogCount"></h6>
                    </div>
                </div>
                <div class="card-body" id="activity">

                    <div id="logs" class="gridviewC">

                    </div>
                    <div id="logs2" style="display: none;" class="listviewC">

                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<!-- #/ container -->
<!--**********************************
    Content body end
***********************************-->

<div style="display: none;">

    <div class="media-body" >

        <blockquote id="newTemp" class="logPane">
            <span class="label btn-rounded" id="tagr"></span>
            <small class="label btn-rounded" id="timestamp"></small>
            <div id="tagbody" class="logTopBorder"></div>
        </blockquote>
        <div id="tempG" >
            <strong class="" id="tagr2"></strong>:
            <strong class="" id="timestamp2"></strong>:
            <span id="tagbody2"></span>
        </div>

    </div>
</div>





<script>



    $('#logView').on('change', function () {
        if ($(this).val() == 'grid') {
            $('#logs').show();
            $('#logs2').hide();
        } else {
            $('#logs').hide();
            $('#logs2').show();
        }
    });

    $('#activity').slimscroll({
        position: "right",
        size: "5px",
        height: "700px",
        color: "#7571f9",
        alwaysVisible: true
    });



    var liveLogCount = 0;

    var live_status = $('#live_status');

    function liveStat(clas, msg) {
        live_status.attr('class', clas)
        live_status.text(msg);
    }

    function connect() {
        var socket = new SockJS('<?= Yii::$app->params['WS_LINK'] ?>');
        ws = Stomp.over(socket);
        liveStat('badge badge-pill badge-light', 'loading...');
        ws.connect({}, function (frame) {
            liveStat('badge badge-pill badge-success', 'connected');
            ws.subscribe("/topic/group_" + '<?= $endpoint->access_key ?>', function (message) {
                var log = JSON.parse(message.body);


                var temp = $('#newTemp');
                var temp2 = $('#tempG');
                $clone = temp.clone();
                $clone2 = temp2.clone();

                console.log(temp2);

                $clone.find('#tagr').html(log.tags);
                $clone2.find('#tagr2').html(log.tags);

                $clone.find('#timestamp').html(log.ts);
                $clone2.find('#timestamp2').html(log.ts);

                $clone.find('#tagbody').text(log.body);
                $clone2.find('#tagbody2').text(log.body);


                $("#logs").append($clone);
                $("#logs2").append($clone2);


                liveLogCount = liveLogCount + 1;
                $('#liveLogCount').text("Activity in view " + liveLogCount);


                var bottomCoord = $('#activity')[0].scrollHeight;
                $('#activity').slimScroll({scrollTo: bottomCoord});
            });

        }, function (error) {
            console.log(error);
            liveStat('badge badge-pill badge-warning', 'disconnected');
            if (confirm("Disconnected, do you want to reconnect")) {
                connect();
            } else {

                toastr.info("You've disconnect from viewing live logs", "Info", {
                    positionClass: "toast-top-center",
                    "closeButton": true,
                    "timeOut": "90000",
                    closeButton: !0,
                    debug: !1,
                    newestOnTop: !0,
                    progressBar: !0,
                    preventDuplicates: !0,
                    onclick: null,
                    showDuration: "500",
                    hideDuration: "1000",
                    extendedTimeOut: "1000",
                    showEasing: "swing",
                    hideEasing: "linear",
                    showMethod: "fadeIn",
                    hideMethod: "fadeOut",
                    tapToDismiss: !1
                })

            }
        });
    }

    function disconnect() {
        if (ws != null) {
            ws.close();
        }
        setConnected(false);
        console.log("Disconnected");
    }

    function sendName() {
        var data = JSON.stringify({
            'name': $("#name").val()
        })
        ws.send("/app/sendMessage", {}, data);
    }

    $(function () {
        connect();
    });

</script>