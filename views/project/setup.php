<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $project app\models\Project */
/* @var $currentForm string */

$this->title = "Setup Project";

?>


<!--**********************************
          Content body start
      ***********************************-->
<div class="content-body">
    <div class="row page-titles mx-0">
        <div class="col p-md-0">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="javascript:void(0)">Home</a></li>
            </ol>
        </div>
    </div>
    <!-- row -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div role="application" class="wizard clearfix" id="steps-uid-0">
                    <div class="steps clearfix">
                        <ul role="tablist">
                            <li role="tab" class="first <?= $currentForm == 'project' ? 'current' : '' ?>">
                                <a id="steps-uid-0-t-0" href="#steps-uid-0-h-0" aria-controls="steps-uid-0-p-0">
                                    <span class="current-info audible">current step: </span>
                                    <span class="number">1.</span> Projects Details
                                </a>
                            </li>
                            <li role="tab" class="done <?= $currentForm == 'app' ? 'current' : '' ?>">
                                <a id="steps-uid-0-t-1" href="#steps-uid-0-h-1" aria-controls="steps-uid-0-p-1">
                                    <span class="number">2.</span> Application Details
                                </a>
                            </li>
                            <li role="tab" class="disabled" aria-disabled="true">
                                <a id="steps-uid-0-t-2" href="#steps-uid-0-h-2" aria-controls="steps-uid-0-p-2">
                                    <span class="number">3.</span> Billing Details</a>
                            </li>
                            <li role="tab" class="disabled last" aria-disabled="true"><a id="steps-uid-0-t-3" href="#steps-uid-0-h-3" aria-controls="steps-uid-0-p-3"><span class="number">4.</span> Confirmation</a></li>
                        </ul>
                    </div>
                    <div class="content clearfix">
                        <h4 id="steps-uid-0-h-0" tabindex="-1" class="title current">Projects Details</h4>
                        <section id="steps-uid-0-p-0" role="tabpanel" aria-labelledby="steps-uid-0-h-0" class="body <?= $currentForm == 'project' ? 'current' : '' ?>" style="left: 0px;" style="<?= $currentForm == 'project' ? 'left: 0px;' : 'left: 1843px; display: none;' ?>">
                            <?php $form = ActiveForm::begin(); ?>
                            <input type="hidden" name="current_form" value="project">
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <?= Html::activeInput('text', $project, 'name', ['class' => 'form-control', 'placeholder' => 'Project name']) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <?= Html::activeTextarea($project, 'description', ['class' => 'form-control', 'placeholder' => 'Project Description']) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="actions clearfix">
                        <ul role="menu" aria-label="Pagination">
                            
                            <li class="disabled" aria-disabled="true"><a href="#previous" role="menuitem">Previous</a></li>
                            <li aria-hidden="false" aria-disabled="false"><?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?></li>
                            <li aria-hidden="true" style="display: none;"><a href="#finish" role="menuitem">Finish</a></li>
                        </ul>
                    </div>
                            <?php ActiveForm::end(); ?>
                        </section>
                        <h4 id="steps-uid-0-h-1" tabindex="-1" class="title">Application Details</h4>
                        <section id="steps-uid-0-p-1" role="tabpanel" aria-labelledby="steps-uid-0-h-1" class="body <?= $currentForm == 'app' ? 'current' : '' ?>"  style="<?= $currentForm == 'app' ? 'left: 0px;' : 'left: 1843px; display: none;' ?>">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" name="firstName" class="form-control" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" name="lastName" class="form-control" placeholder="Last Name">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="text" name="address" class="form-control" placeholder="Address">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" name="city" class="form-control" placeholder="City">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" name="zip" class="form-control" placeholder="ZIP Code">
                                    </div>
                                </div>
                            </div>
                        </section>
                        <h4 id="steps-uid-0-h-2" tabindex="-1" class="title">Billing Details</h4>
                        <section id="steps-uid-0-p-2" role="tabpanel" aria-labelledby="steps-uid-0-h-2" class="body" aria-hidden="true" style="display: none;">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="creditCard" placeholder="Credit Card Number">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="date" placeholder="Expiration Date">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="owner" placeholder="Credit Card Owner">
                                    </div>
                                </div>
                            </div>
                        </section>
                        <h4 id="steps-uid-0-h-3" tabindex="-1" class="title">Confirmation</h4>
                        <section id="steps-uid-0-p-3" role="tabpanel" aria-labelledby="steps-uid-0-h-3" class="body" aria-hidden="true" style="display: none;">
                            <div class="row h-100">
                                <div class="col-12 h-100 d-flex flex-column justify-content-center align-items-center">
                                    <h2>You have submitted form successfully!</h2>
                                    <p>Thank you very much for you information. we will procceed accordingly.</p>
                                </div>
                            </div>
                        </section>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- #/ container -->
</div>
<!--**********************************
    Content body end
***********************************-->


