<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $models[] app\models\Project */

$this->title = 'Projects';
$this->params['breadcrumbs'][] = ['label' => $this->title,'url'=> ['index']];
?>


<!--**********************************
          Content body start
      ***********************************-->
<div class="content-body">
   
    <!-- row -->
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-tile">Projects</h2>
                        <div class="col text-right">
                    <a href="<?= Yii::getAlias('@web'); ?>/project/create" class="btn btn-lg btn-primary">New Project <i class="fa fa-plus"></i></a>
                </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <?php foreach ($models as $model) { ?>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-center">
                                <span class="display-5"><i class="icon-folder-alt gradient-3-text"></i></span>
                                <h4 class="card-widget__title text-dark mt-3"><?= $model->name ?></h4>
                                <p class="text-muted"><?= empty($model->description)?"No Description":$model->description ?></p>
                                <a class="btn gradient-4 btn-lg border-0 btn-rounded px-5" href="<?= Yii::getAlias('@web'); ?>/apps?AppsSearch[project_id]=<?= $model->id?>">View Apps</a>
                            </div>
                        </div>
                        <div class="card-footer border-0 bg-transparent">
                            <div class="row">
                                <div class="col-6 border-right-1 pt-3">
                                    <a class="text-center d-block text-muted" href="<?= Yii::getAlias('@web'); ?>/project/update?id=<?= $model->id?>">
                                        <i class="fa fa-edit gradient-3-text"></i>
                                        <p class="">Update</p>
                                    </a>
                                </div>
                                <div class="col-6 pt-3"><a class="text-center d-block text-muted" onclick="return confirm('Are you sure sure you delete this record?');" href="<?= Yii::getAlias('@web'); ?>/project/delete?id=<?= $model->id?>">
                                        <i class="fa fa-close gradient-4-text"></i>
                                        <p class="">Delete</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php
            echo \yii\widgets\LinkPager::widget([
                'pagination' => $pages,
            ]);
            ?>

        </div>
    </div>
    <!-- #/ container -->
</div>
<!--**********************************
    Content body end
***********************************-->
