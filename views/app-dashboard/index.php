<?php
/* @var $this yii\web\View */
/* @var $model app\models\Apps */


$this->title = $model->name . ' Dashboard';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
?>
<!--
<script>
    function formatBytes(a, b = 2) {
        if (0 === a)
            return"0 Bytes";
        const c = 0 > b ? 0 : b, d = Math.floor(Math.log(a) / Math.log(1024));
        return parseFloat((a / Math.pow(1024, d)).toFixed(c)) + " " + ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"][d]
    }
    console.log(formatBytes(window.performance.memory.totalJSHeapSize));
    console.log(window.PerformanceNavigationTiming.domComplete);
    console.log(window.performance);

</script>-->

<style>
    .ct-label{
        color: black !important;
        font-weight: 500;
    }
</style>

<!--**********************************
          Content body start
      ***********************************-->
<div class="content-body">

    <!-- row -->
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-tile"><?= $model->name . ' Dashboard' ?></h2>
                        <div class="col-md-2" style="    float: right;">
                            <form class="input-group mb-3" method="get" id="form1">
                                <select class="custom-select" name="ym">
                                    <?php foreach ($yms as $ym) { ?>
                                        <option><?= $ym->ym ?></option>
                                    <?php } ?>
                                </select>
                                <button class="input-group-append btn mb-1 btn-outline-dark" type="submit" form="form1" value="Submit">Search</button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card card-widget">
                                <div class="card-body">
                                    <h5 class="text-muted">Logs</h5>
                                    <h2 class="mt-4"><?= $thisMonthTotalLog ?></h2>
                                    <span>Total Logs</span>
                                    <div class="mt-4">
                                        <h4><?= $logSuccess->stat ?></h4>
                                        <h6>Success Activities <span class="pull-right"><?= $logSuccess->statPerc ?>%</span></h6>
                                        <div class="progress mb-3" style="height: 7px">
                                            <div class="progress-bar gradient-1" style="width: <?= $logSuccess->statPerc ?>%;" role="progressbar"><span class="sr-only"><?= $logSuccess->statPerc ?>% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mt-4"> 
                                        <h4><?= $logGeneral->stat ?></h4>
                                        <h6 class="m-t-10 text-muted">General Activities <span class="pull-right"><?= $logGeneral->statPerc ?>%</span></h6>
                                        <div class="progress mb-3" style="height: 7px">
                                            <div class="progress-bar gradient-6" style="width: <?= $logGeneral->statPerc ?>%;" role="progressbar"><span class="sr-only"><?= $logGeneral->statPerc ?>% Complete</span></div>
                                        </div>
                                    </div>
                                    <div class="mt-4">
                                        <h4><?= $logError->stat ?></h4>
                                        <h6 class="m-t-10 text-muted">Error Activities <span class="pull-right"><?= $logError->statPerc ?>%</span></h6>
                                        <div class="progress mb-3" style="height: 7px">
                                            <div class="progress-bar gradient-2" style="width: <?= $logError->statPerc ?>%;" role="progressbar"><span class="sr-only"><?= $logError->statPerc ?>% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="card card-widget">
                                <div class="card-body">
                                    <h5 class="text-muted">Web Activity</h5>
                                    <h2 class="mt-4"><?= $thisMonthTotalWeb ?></h2>
                                    <span>Total Activity</span>
                                    <div class="mt-4">
                                        <h4><?= $httpSuccess->stat ?></h4>
                                        <h6>Success Activities <span class="pull-right"><?= $httpSuccess->statPerc ?>%</span></h6>
                                        <div class="progress mb-3" style="height: 7px">
                                            <div class="progress-bar gradient-1" style="width: <?= $httpSuccess->statPerc ?>%;" role="progressbar"><span class="sr-only"><?= $httpSuccess->statPerc ?>% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mt-4">
                                        <h4><?= $httpClient->stat ?></h4>
                                        <h6 class="m-t-10 text-muted">Client-Side Error<span class="pull-right"><?= $httpClient->statPerc ?>%</span></h6>
                                        <div class="progress mb-3" style="height: 7px">
                                            <div class="progress-bar gradient-3" style="width: <?= $httpClient->statPerc ?>%;" role="progressbar"><span class="sr-only"><?= $httpClient->statPerc ?>% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mt-4">
                                        <h4><?= $httpServer->stat ?></h4>
                                        <h6 class="m-t-10 text-muted">Server-Side Error<span class="pull-right"><?= $httpServer->statPerc ?>%</span></h6>
                                        <div class="progress mb-3" style="height: 7px">
                                            <div class="progress-bar gradient-2" style="width: <?= $httpServer->statPerc ?>%;" role="progressbar"><span class="sr-only"><?= $httpServer->statPerc ?>% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Log Activity chart</h4>
                                    <canvas id="logActivityChart"  width="500" height="500"></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Web Activity chart</h4>
                                    <canvas id="webActivityChart"  width="500" height="500"></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Web Page Visit</h4>
                                    <div id="activty" style="overflow-y: auto;height: 510px">
                                        <?php foreach ($pageVisit as $page) { ?>
                                            <div class="media border-bottom-1 pt-3 pb-3">
                                                <div class="mt-4  col-md-12">
                                                    <h5 class="m-t-10 text-mu" ><i class="fa fa-link"></i> <?= $page->label ?>
                                                        <span class="pull-right"><?= $page->data ?> Visit(s)</span>
                                                    </h5>
                                                    <div class="progress mb-3" style="height: 7px">
                                                        <div class="progress-bar gradient-6" style="width: <?= app\assets\Misc::millisecPercent($page->data, $pageScoreSum) ?>%;" role="progressbar">
                                                            <span class="sr-only"><?= app\assets\Misc::millisecPercent($page->data, $pageScoreSum) ?>% Complete</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Web Page Load Time</h4>
                                   <!-- <div class="col-md-2" style="    float: right;">
                                        <a href="<?= Yii::getAlias('@web'); ?>/app-dashboard/pageloadtime?app=<?= $model->id ?>">View All</a>
                                    </div>-->
                                    <div id="activity" style="overflow-y: auto;height: 510px">
                                        <?php foreach ($pageLoadTime as $page) { ?>
                                            <div class="media border-bottom-1 pt-3 pb-3">
                                                <div class="mt-4  col-md-12">
                                                    <h5 class="m-t-10 text-mu" ><i class="fa fa-link"></i> <?= $page->label ?>
                                                        <span class="pull-right"><i class="fa fa-clock-o"></i><?= app\assets\Misc::formatMilliseconds($page->data) ?></span>
                                                    </h5>
                                                    <div class="progress mb-3" style="height: 7px">
                                                        <div class="progress-bar gradient-2" style="width: <?= app\assets\Misc::millisecPercent($page->data, (1000 * 10)) ?>%;" role="progressbar">
                                                            <span class="sr-only"><?= app\assets\Misc::millisecPercent($page->data, (1000 * 10)) ?>% Complete</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- #/ container -->
</div>
<!--**********************************
    Content body end
***********************************-->

<script>

    (function ($) {
        "use strict"


        var jqxhri = $.getJSON("<?= Yii::getAlias('@web'); ?>/app-dashboard/page-score-chart?app=<?= $model->id ?>", function (data) {

                    var lables = [];
                    var series = [];
                    $.each(data, function (index, item) {
                        lables.push(item.label);
                        series.push(item.data);
                    });
                    console.log(series);
                    var webPageChart = new Chartist.Bar("#web-page-chart", {
                        labels: lables,
                        series: [series]
                    }, {
                        height: 500,
                        seriesBarDistance: 5,
                        reverseData: true,
                        horizontalBars: true,
                        axisY: {
                            offset: 400
                        },
                        plugins: [
                            Chartist.plugins.tooltip()
                        ]
                    });
                }).fail(function () {
                    console.log("error");
                }).always(function () {
                    console.log("complete");
                });
            })(jQuery);
</script>

<script>

    (function ($) {
        "use strict"

        var getHttpsColorCode = function (httpCode) {
            console.log(httpCode);
            if (httpCode >= 200 && httpCode <= 299) {
                return '#124ffd52';
            } else if (httpCode >= 400 && httpCode <= 499) {
                return '#ffbe7a73';
            } else if (httpCode >= 500 && httpCode <= 599) {
                return '#e23c3c82';
            }
        }


        var chartOption = {
            legend: {
                position: 'top'
            },
            scale: {
                ticks: {
                    beginAtZero: true
                }
            }
        };
        $.getJSON("<?= Yii::getAlias('@web'); ?>/app-dashboard/http-status-chart?app=<?= $model->id ?>", function (data) {

                    var lables = [];
                    var series = [];
                    var background = [];
                    $.each(data, function (index, item) {
                        lables.push(item.label);
                        series.push(item.data);
                        background.push(getHttpsColorCode(item.statusCode))
                    });

                    var webActivityChart = document.getElementById("webActivityChart");
                    webActivityChart.height = 300;

                    var myChart = new Chart(webActivityChart, {
                        type: 'polarArea',
                        data: {
                            datasets: [{
                                    data: series,
                                    backgroundColor: background,
                                    hoverBackgroundColor: background
                                }],
                            labels: lables
                        },
                        options: {
                            position: 'top',
                            responsive: true
                        }
                    });
                }).fail(function () {
                    console.log("error");
                }).always(function () {
                    console.log("complete");
                });
            })(jQuery);
</script>

<script>

    //Horizontal bar chart


    //radar chart

    var chartOption = {
        legend: {
            position: 'top'
        },
        scale: {
            ticks: {
                beginAtZero: true
            }
        }
    };


    $.getJSON("<?= Yii::getAlias('@web'); ?>/app-dashboard/logs-ym-chart?app=<?= $model->id ?>", function (data) {


            var lables = [];
            var series = [];
            var background = [];
            $.each(data, function (index, item) {
                lables.push(item.label);
                series.push(item.data);
                background.push(item.color_code + "52")

            });

            var webActivityChart = document.getElementById("logActivityChart");
            webActivityChart.height = 300;

            var myChart = new Chart(webActivityChart, {
                type: 'polarArea',
                data: {
                    datasets: [{
                            data: series,
                            backgroundColor: background,
                            hoverBackgroundColor: background
                        }],
                    labels: lables
                },
                options: {
                    position: 'top',
                    responsive: true
                }
            });

        }).fail(function () {
            console.log("error");
        }).always(function () {
            console.log("complete");
        });
</script>