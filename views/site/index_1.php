<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$this->title = "Probelot "
?>

<!--=====================================
        =            Homepage Banner            =
        ======================================-->

<section class="banner bg-1" id="home" style="min-height:0px">
    <div class="container">
        <div class="row">
            <div class="col-md-12 align-self-center text-center">
                <!-- Contents -->
                <div class="content-block">
                    <h1>Software Insight</h1>
                    <h5>Probelogr technology for is a real-time software activity analysis and reporting system</h5>
                    <!-- App Badge -->
                    <div class="app-badge">
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a href="https://documenter.getpostman.com/view/373796/Szzq4uqo?version=latest" target="_blank" class="btn btn-download"><i class="ti-world "></i>
                                    <div>Get the <span>Rest API</span></div>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="https://github.com/UchePhilz/probelogr" target="_blank" class="btn btn-download"><i class="ti-github "></i>
                                    <div>Get Code<span>GitHub</span></div>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--====  End of Homepage Banner  ====-->


<!--==============================
=            Features            =
===============================-->

<section class="section feature" id="feature">
    <div class="container">

        <div class="row bg-elipse">
            <div class="col-lg-4 align-self-center text-center text-lg-right">
                <!-- Feature Item -->
                <div class="feature-item">
                    <!-- Icon -->
                    <div class="icon">
                        <i class="ti-alarm-clock"></i>
                    </div>
                    <!-- Content -->
                    <div class="content">
                        <h5>Bug Detection</h5>
                        <p>Discover bugs on your platform and never be caught unaware of an error when your software is live</p>
                    </div>
                </div>
                <!-- Feature Item -->
                <div class="feature-item">
                    <!-- Icon -->
                    <div class="icon">
                        <i class="ti-stats-up"></i>
                    </div>
                    <!-- Content -->
                    <div class="content">
                        <h5>Real-Time Alert System</h5>
                        <p>Get notified when a bug is detected, and quickly respond the them before it has a major impact on your users.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center">
                <!-- Feature Item -->
                <div class="feature-item mb-0">
                    <!-- Icon -->
                    <div class="icon">
                        <i class="ti-dashboard"></i>
                    </div>
                    <!-- Content -->
                    <div class="content">
                        <h5>Frontend & Backend Monitoring</h5>
                        <p>Monitor page views, searches and other actions users may take on your application in real-time</p>
                    </div>
                </div>
                <div class="app-screen">
                    <img class="img-fluid" src="<?= Yii::getAlias('@web'); ?>/assets/frontend/images/phones/i-phone-screen.png" alt="app-screen">
                </div>
                <!-- Feature Item -->
                <div class="feature-item">
                    <!-- Icon -->
                    <div class="icon">
                        <i class="ti-support"></i>
                    </div>
                    <!-- Content -->
                    <div class="content">
                        <h5>24/7 support</h5>
                        <p>Our support team are readily available</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center text-lg-left align-self-center">
                <!-- Feature Item -->
                <div class="feature-item">
                    <!-- Icon -->
                    <div class="icon">
                        <i class="ti-signal"></i>
                    </div>
                    <!-- Content -->
                    <div class="content">
                        <h5>View Real-Time & Historic Software Activity</h5>
                        <p>Access your log from your browser, and go back to historical activities of your software with ease</p>
                    </div>
                </div>
                <!-- Feature Item -->
                <div class="feature-item">
                    <!-- Icon -->
                    <div class="icon">
                        <i class="ti-pie-chart"></i>
                    </div>
                    <!-- Content -->
                    <div class="content">
                        <h5>INTELLIGENCE AND DATA AGGREGATION</h5>
                        <p>Get Readable Logs and analytics of the performance of your software.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about section bg-2" id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 align-self-center text-center">
                <!-- Image Content -->
                <div class="image-block">
                    <img class="phone-thumb-md" src="<?= Yii::getAlias('@web'); ?>/assets/frontend/images/phones/iphone-chat.png" alt="iphone-feature" class="img-fluid">
                </div>
            </div>
            <div class="col-lg-6 col-md-10 m-md-auto align-self-center ml-auto">
                <div class="about-block">
                    <!-- About 01 -->
                    <div class="about-item">
                        <div class="icon">
                            <i class="ti-receipt"></i>
                        </div>
                        <div class="content">
                            <h5>High level Log tailing</h5>
                            <p>With Probelogr non technical member of your organization can tail application logs for insight during test and other activites</p>
                        </div>
                    </div>
                    <!-- About 02 -->
                    <div class="about-item active">
                        <div class="icon">
                            <i class="ti-settings"></i>
                        </div>
                        <div class="content">
                            <h5>Powerful Ecosystem</h5>
                            <p>Probelogr backed by Kafka, Cassandra, H2 In-Memory Storage and other technologies to power our ecosystem</p>
                        </div>
                    </div>
                    <!-- About 03 -->
                    <div class="about-item">
                        <div class="icon">
                            <i class="ti-vector"></i>
                        </div>
                        <div class="content">
                            <h5>Best For all scale</h5>
                            <p>Probelogr is designed for enterprise for SAAS Model software, And can be installed in privately dedicated servers</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--====  End of About  ====-->


<footer class="footer-main">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 mr-auto">
                <div class="footer-logo">
                    <img src="<?= Yii::getAlias('@web'); ?>/assets/probe_imgs/probelogrw.png" alt="Probelogr logo" height="80px">
                </div>
                <div class="copyright">
                    <p>Copyright &copy; Probelogr & Probelot, Developed by <a href="https://www.uchephilz.com">Uchephilz</a> 2020</p>
                </div>
            </div>
            <div class="col-lg-6 text-lg-right">
                <!-- Social Icons -->
                <!-- <ul class="social-icons list-inline">
                     <li class="list-inline-item">
                         <a target="_blank" href="https://facebook.com/themefisher"><i class="text-primary ti-facebook"></i></a>
                     </li>
                     <li class="list-inline-item">
                         <a target="_blank" href="https://twitter.com/themefisher"><i class="text-primary ti-twitter-alt"></i></a>
                     </li>
                     <li class="list-inline-item">
                         <a target="_blank" href="https://github.com/themefisher"><i class="text-primary ti-linkedin"></i></a>
                     </li>
                     <li class="list-inline-item">
                         <a target="_blank" href="https://instagram.com/themefisher"><i class="text-primary ti-instagram"></i></a>
                     </li>
                 </ul>-->
                <!-- Footer Links -->

                <ul class="footer-links list-inline">
                    <li class="list-inline-item">
                        <a class="scrollTo" href="#home">Home</a>
                    </li>
                    <li class="list-inline-item">
                        <a class="scrollTo" href="#feature">Features</a>
                    </li>
                    
            </div>
        </div>
    </div>
</footer>

