<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\AppUsers */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Register';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="login-form-bg h-100">
    <div class="container h-100">
        <div class="row justify-content-center">
            <div class="col-xl-6">
                <div class="form-input-content">
                    <div class="card login-form mb-0">
                        <div class="card-body pt-5">

                            <div class="justify-content-center mb-4">
                                <a class="text-center" href="<?= Yii::getAlias('@web'); ?>/site">
                                    <img src="<?= Yii::getAlias('@web'); ?>/assets/probe_imgs/probelogr.png" style="width: 20%;left: 48%;position: sticky;">
                                </a>
                            </div>

                            <?php $form = \yii\widgets\ActiveForm::begin(); ?>
                            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true, 'class' => 'form-control form-control-alternative']) ?>
                            <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true, 'class' => 'form-control form-control-alternative']) ?>
                            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true, 'class' => 'form-control form-control-alternative']) ?>
                            <?= $form->field($model, 'phone_number')->input('number', ['maxlength' => true, 'class' => 'form-control form-control-alternative']) ?>
                            <?= $form->field($model, 'email')->input('email', ['maxlength' => true, 'class' => 'form-control form-control-alternative']) ?>
                            <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'class' => 'form-control form-control-alternative']) ?>
                            <?= $form->field($model, 'confirm_password')->passwordInput(['maxlength' => true, 'class' => 'form-control form-control-alternative']) ?>
                            <div class="text-muted font-italic">
                                <small>It is advised that you should use a secure password</small>
                            </div>
                            <!--                            <div class="row my-4">
                                                            <div class="col-12">
                                                                <div class="custom-control custom-control-alternative custom-checkbox" >
                                                                    <input class="custom-control-input checkIt" id="customCheckRegister" name="AppUsers[rememberMe]"  type="checkbox" >
                                                                    <label class="custom-control-label" for="customCheckRegister">
                                                                        <span class="text-muted">I agree with the <a target="_blank"  href="<?= Yii::getAlias('@web'); ?>/terms-and-conditions">Privacy Policy</a></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>-->
                            <div class="text-center">
                                <input type="submit" class="btn btn-primary mt-4 regis"  value="Create account">
                            </div>
                            <?php \yii\widgets\ActiveForm::end(); ?>
                            <p class="mt-5 login-form__footer">Have account? <a href="<?= Yii::getAlias('@web'); ?>/site/login" class="text-primary">Login </a></p>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





<script>
    $(document).ready(function () {

    });

    $('.checkIt').change(function () {
        //   var atLeastOneIsChecked = $('input[name="chk[]"]:checked').length = 3;

        if ($('input[name="AppUsers[rememberMe]"]:checked').length == 1) {
            $('.regis').show();
        } else {
            $('.regis').hide();
        }
    });

</script>