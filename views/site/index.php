<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* @var $contentBlogs[] app\models\content\ContentBlog */
/* @var $contentVideos[] app\models\content\ContentVideo */

$this->title = "Probelot "
?>

<!-- Start Banner -->
<div class="banner-area text-combo multi-heading bottom-shape top-pad-90 top-pad-35-mobile gradient-bg text-light">
    <div class="item">
        <div class="box-table">
            <div class="box-cell">
                <div class="container">
                    <div class="row">
                        <div class="double-items">
                            <div class="col-lg-7 info">
                                <h2 class="wow fadeInDown" data-wow-duration="1s">Logging & Reporting<span>to ensure the stability of your software</span></h2>
                                <p class="wow fadeInLeft" data-wow-duration="1.5s">
                                    You can rest easy with Probelogr at the core of your system, handling the heavy weight of logging and activity reporting,
                                    to ensure that your tech team stays ahead.
                                </p>
                                <a class="btn circle btn-md btn-transparent border inc-icon wow fadeInUp"
                                   data-wow-duration="1.8s" href="<?= Yii::getAlias('@web'); ?>/site/login">Get Started <i class="fas fa-angle-right"></i></a>

                            </div>
                            <div class="col-lg-5 thumb wow fadeInDown" data-wow-duration="1s">
                                <img src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/img/illustration/5.png" alt="Thumb">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wavesshape">
                    <img src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/img/shape/4.svg" alt="Shape">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Banner -->

<!-- Start About
============================================= -->
<div class="about-area text-center carousel-shadow wavesshape-bottom default-padding-top">
    <div class="container">
        <div class="about-items">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="heading">
                        <h4>About Us</h4>
                        <h2>
                            We provide a solution to monitor logs and activities from the front-end to the back-end of your application

                        </h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <div class="content">
                        <p>
                            Probelogr as a solution to the issue of
                            tailing logs, monitoring the activities that goes on within a software code.
                            With Probelogr, bugs won't go undetected and your customers won't have to report on issues,
                            because you'll be notified when an error has occurred
                            Probelogr's monitoring system works both for front-end, back-end and server administration.
                            We are always improving our improving Probelogr, and we guarantee that our next deployment
                            will always improve how your engineering team works
                        </p>
                    </div>
                </div>
                <div class="feature-box text-left col-lg-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="feature-2-col-carousel owl-carousel owl-theme">
                                <!-- Single Item -->
                                <div class="item">
                                    <div class="info">
                                        <h4>Environment Monitoring</h4>
                                        <p>
                                            With the Probelogr's tools, you can monitor your server system logs, MySQL, Apache, nginx etc.
                                            to detect errors or other activities in realtime.
                                        </p>
                                    </div>
                                    <div class="bottom">
                                        <i class="flaticon-operative-system"></i>
                                        <!--<a class="btn circle btn-sm btn-gradient" href="#">Read more <i class="fas fa-plus"></i></a>-->
                                    </div>
                                </div>
                                <!-- End Single Item -->
                                <!-- Single Item -->
                                <div class="item">
                                    <div class="info">
                                        <h4>Front-end Monitoring</h4>
                                        <p>
                                            Track user's journey on your software, monitor page load-time, detect errors users experiences
                                            while they use your platform, get the console errors your users encounter, and much more
                                        </p>
                                    </div>
                                    <div class="bottom">
                                        <i class="flaticon-analysis-1"></i>
                                        <!--<a class="btn circle btn-sm btn-gradient" href="#">Read more <i class="fas fa-plus"></i></a>-->
                                    </div>
                                </div>
                                <!-- End Single Item -->
                                <!-- Single Item -->
                                <div class="item">
                                    <div class="info">
                                        <h4>Back-end Monitoring</h4>
                                        <p>
                                            Detect all form of error or exception your application may encounter,
                                            detect the number of time a function, method or subroutine is used in your program
                                        </p>
                                    </div>
                                    <div class="bottom">
                                        <i class="flaticon-server"></i>
                                        <!--<a class="btn circle btn-sm btn-gradient" href="#">Read more <i class="fas fa-plus"></i></a>-->
                                    </div>
                                </div>
                                <!-- End Single Item -->
                                <!-- Single Item -->
                                <div class="item">
                                    <div class="info">
                                        <h4>Alert System</h4>
                                        <p>
                                            You can configure email alerts to be sent out when an action take place on your application or
                                            when a certain keyword is dectectd within your log and error messages will be sent as email in real-time.
                                        </p>
                                    </div>
                                    <div class="bottom">
                                        <i class="flaticon-robot"></i>
                                        <!--<a class="btn circle btn-sm btn-gradient" href="#">Read more <i class="fas fa-plus"></i></a>-->
                                    </div>
                                </div>
                                <!-- End Single Item -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="waveshape">
        <img src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/img/shape/2.svg" alt="Shape">
    </div>
</div>
<!-- End About -->

<!-- Star Faq
============================================= -->
<div class="faq-area overflow-hidden rectangular-shape default-padding bg-gray">
    <div class="container">
        <div class="faq-items">
            <div class="row">
                <div class="col-lg-6">
                    <div class="faq-content">
                        <h2>Frequently asked questions</h2>
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h4 class="mb-0" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        How do I get started?
                                    </h4>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>
                                            Create an account with us, create the your project, app and tags, use your access token to connect to our API.
                                            or follow our how to videos or documentation to begin usage
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <h4 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        In Probelogr, what is Project, Apps and Tags?
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel, illum earum nobis dolorum aliquid! Quos pariatur ipsam eum voluptates. Illum provident consequatur non aut labore, voluptates repudiandae maxime cum dolorem.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThree">
                                    <h4 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        How do I get personalized support?
                                    </h4>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum iure accusamus ea, reprehenderit aspernatur deleniti corporis ad perspiciatis. Magnam sit enim animi, esse deleniti nobis quaerat veniam suscipit odit officiis.
                                        </p>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="thumb wow fadeInLeft" data-wow-delay="0.5s">
                        <img src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/img/illustration/undraw_Questions_re_1fy7.svg" alt="Thumb">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Faq -->

<!-- Star Companies
============================================= -->

<!-- Star Companies
<div class="companies-area bg-gray default-padding-bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="site-heading text-center">
                    <h4>Companies</h4>
                    <h2>
                        Trusted by great brands
                    </h2>
                </div>
            </div>
        </div>
        <div class="companies-items">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="companies-carousel owl-carousel owl-theme">
                        <div class="item">
                            <a href="#"><img src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/img/clients/1.png" alt="Thumb"></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/img/clients/2.png" alt="Thumb"></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/img/clients/3.png" alt="Thumb"></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/img/clients/4.png" alt="Thumb"></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/img/clients/5.png" alt="Thumb"></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/img/clients/6.png" alt="Thumb"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
-->
<!-- End Companies -->

<!-- Start Projects Area
============================================= -->
<div class="projects-area overflow-hidden carousel-shadow default-padding-top">
    <!-- Fixed BG -->
    <div class="fixed-bg" style="background-image: url(<?= Yii::getAlias('@web'); ?>/assets/anada_theme/img/shape/2.png);"></div>
    <!-- Fixed BG -->
    <div class="container">
        <div class="heading-left">
            <div class="row">
                <div class="col-lg-5">
                    <h2>
                        Follow our video tutorials
                    </h2>
                </div>
                <div class="col-lg-6 offset-lg-1">
                    <p>
                        We have created numerous video tutorials to support you in your implementation of Probelogr 
                    </p>
                    <a class="btn circle btn-md btn-gradient wow fadeInUp" href="<?= Yii::getAlias('@web'); ?>/videos">View All <i class="fas fa-plus"></i></a>
                </div>
            </div>
        </div>
        <div class="projects-items col-lg-12 wow fadeInUp" data-wow-delay="0.2s">
            <div class="row">
                <div class="projects-carousel owl-carousel owl-theme">

                    <!-- Single Item -->
                    <?php foreach ($contentVideos as $contentVideo) { ?>
                        <div class="single-item">
                            <div class="thumb item-effect">
                                <img src="https://img.youtube.com/vi/<?= $contentVideo->video_url ?>/sddefault.jpg" alt="Thumb">
                                <div class="effect-info">
                                    <a href="https://www.youtube.com/watch?v=<?= $contentVideo->video_url ?>" class="item popup-youtube">
                                        <i class="fas fa-video"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="info">
                                <h5>
                                    <a href="#"><?= $contentVideo->video_title ?></a>
                                </h5>
                                <ul>
                                    <li><?= $contentVideo->tags ?></li>
                                </ul>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <!-- End Single Item -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Projects Area -->

<!-- Start Fun Factor Area
============================================= 
<div class="fun-factor-area default-padding">

    <div class="container">
        <div class="client-items text-center">
            
            <div class="fixed-bg contain" style="background-image: url(<?= Yii::getAlias('@web'); ?>/assets/anada_theme/img/map.svg);"></div>
            
            <div class="row">
                <div class="col-lg-3 col-md-6 item">
                    <div class="fun-fact">
                        <div class="timer" data-to="687" data-speed="5000">687</div>
                        <span class="medium">Total App</span>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 item">
                    <div class="fun-fact">
                        <div class="timer" data-to="2348" data-speed="5000">2348</div>
                        <span class="medium">Total Tags</span>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 item">
                    <div class="fun-fact">
                        <div class="timer" data-to="450" data-speed="5000">450</div>
                        <span class="medium">Alerts Sent</span>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 item">
                    <div class="fun-fact">
                        <div class="timer" data-to="1200" data-speed="5000">1200</div>
                        <span class="medium">Total Logs</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 End Fun Factor Area -->

<!-- Start Pricing Area
============================================= -->

<!-- 
<div class="pricing-area overflow-hidden rectangular-shape shape-margin-right default-padding-bottom bottom-less">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="site-heading text-center">
                    <h4>Our Pricing</h4>
                    <h2>
                        Select Your Choice
                    </h2>
                </div>
            </div>
        </div>
        <div class="pricing-items text-center">
            <div class="row">
                <div class="col-lg-4 col-md-6 single-item">
                    <div class="pricing-item wow fadeInUp" data-wow-delay="400ms">
                        <ul>
                            <li class="pricing-header">
                                <div class="icon">
                                    <div class="shape-top">
                                        <div class="shape"></div>
                                        <div class="shape"></div>
                                        <div class="shape"></div>
                                    </div>
                                    <i class="flaticon-start"></i>
                                    <div class="shape-bottom">
                                        <div class="shape"></div>
                                        <div class="shape"></div>
                                        <div class="shape"></div>
                                    </div>
                                </div>
                                <h5>Trial Version</h5>
                                <h2><sup>$</sup>0.00</h2>
                            </li>
                            <li><i class="ti-close"></i> Demo file</li>
                            <li><i class="ti-check"></i> Update</li>
                            <li><i class="ti-check"></i> Commercial use</li>
                            <li><i class="ti-close"></i> Support</li>
                            <li><i class="ti-check"></i> 2 database</li>
                            <li class="footer">
                                <a class="btn circle btn-sm btn-gray effect" href="#">Try for free</a>
                            </li>
                        </ul>
                    </div>
                </div>
               
                <div class="col-lg-4 col-md-6 single-item">
                    <div class="pricing-item active wow fadeInUp" data-wow-delay="500ms">
                        <ul>
                            <li class="pricing-header">
                                <div class="icon">
                                    <div class="shape-top">
                                        <div class="shape"></div>
                                        <div class="shape"></div>
                                        <div class="shape"></div>
                                    </div>
                                    <i class="flaticon-medal-1"></i>
                                    <div class="shape-bottom">
                                        <div class="shape"></div>
                                        <div class="shape"></div>
                                        <div class="shape"></div>
                                    </div>
                                </div>
                                <h5>Regular</h5>
                                <h2><sup>$</sup>29 <sub>/ M</sub></h2>
                            </li>
                            <li><i class="ti-close"></i> Demo file</li>
                            <li><i class="ti-check"></i> Update</li>
                            <li><i class="ti-close"></i> Commercial use</li>
                            <li><i class="ti-check"></i> Support</li>
                            <li><i class="ti-check"></i> 8 database</li>
                            <li class="footer">
                                <a class="btn circle btn-sm btn-gradient" href="#">Get Started</a>
                            </li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-lg-4 col-md-6 single-item">
                    <div class="pricing-item wow fadeInUp" data-wow-delay="600ms">
                        <ul>
                            <li class="pricing-header">
                                <div class="icon">
                                    <div class="shape-top">
                                        <div class="shape"></div>
                                        <div class="shape"></div>
                                        <div class="shape"></div>
                                    </div>
                                    <i class="flaticon-value"></i>
                                    <div class="shape-bottom">
                                        <div class="shape"></div>
                                        <div class="shape"></div>
                                        <div class="shape"></div>
                                    </div>
                                </div>
                                <h5>Extended</h5>
                                <h2><sup>$</sup>59 <sub>/ Y</sub></h2>
                            </li>
                            <li><i class="ti-check"></i> Demo file</li>
                            <li><i class="ti-check"></i> Update</li>
                            <li><i class="ti-close"></i> Commercial use</li>
                            <li><i class="ti-check"></i> Support</li>
                            <li><i class="ti-check"></i> 8 database</li>
                            <li class="footer">
                                <a class="btn circle btn-sm btn-gray effect" href="#">Get Started</a>
                            </li>
                        </ul>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
-->
<!-- End Pricing Area -->

<!-- Start Blog Area
============================================= -->
<div class="blog-area bg-gray default-padding bottom-less">
    <div class="container">
        <div class="heading-left">
            <div class="row">
                <div class="col-lg-5">
                    <h2>
                        Stay up to date with our new features and how-to tutorials
                    </h2>
                </div>
                <div class="col-lg-6 offset-lg-1">
                    <p>
                        We have put in the effort to provide quality documentation and support to assist with your implementation of Probelogr
                    </p>
                    <a class="btn circle btn-md btn-gradient wow fadeInUp" href="<?= Yii::getAlias('@web'); ?>/blogs">View All <i class="fas fa-plus"></i></a>
                </div>
            </div>
        </div>
        <div class="blog-items">
            <div class="row">
                <?php foreach ($contentBlogs as $contentBlog) { ?>
                    <!-- Single Item -->
                    <div class="col-lg-4 col-md-6 single-item">
                        <div class="item">
                            <div class="thumb">
                                <a href="#">
                                    <img src="<?= $contentBlog->header_image_url ?>" alt="Thumb">
                                    <div class="date">
                                        <?php
                                        $date = strtotime($contentBlog->created_time);
                                        echo date('dS M', $date);
                                        ?>
                                        <strong><?= date('Y', $date); ?></strong>
                                    </div>
                                </a>
                            </div>
                            <div class="info">
                                <div class="meta">
                                    <ul>
                                        <li>
                                            <i class="fas fa-tag"></i>
                                        </li>
                                        <?php
                                        $tags = explode(",", $contentBlog->tags);
                                        foreach ($tags as $tag) {
                                            ?>
                                            <li>
                                                <a href="#"><?= $tag ?></a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <h4>
                                    <a href="#"><?= $contentBlog->content_title ?></a>
                                </h4>
                                <p>
                                    <?= $contentBlog->blog_description ?>
                                </p>
                                <div class="footer-meta">
                                    <ul>
                                        <li>
                                            <a href="#"><i class="fas fa-user"></i> <?= $contentBlog->author ?></a>
                                        </li>
                                        <li>
                                            <a class="btn-simple" href="<?= Yii::getAlias('@web'); ?>/blogs/view?tagline=<?= $contentBlog->tagline ?>"><i class="fas fa-angle-right"></i> Read More</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Item -->
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- End Blog Area -->

    <!-- Start Start With Us Area
    ============================================= -->
    <div class="start-us-area overflow-hidden bg-gradient text-light default-padding">
        <!-- Fixed BG -->
        <div class="fixed-bg" style="background-image: url(<?= Yii::getAlias('@web'); ?>/assets/anada_theme/img/shape/1.png);"></div>
        <!-- Fixed BG -->
        <div class="container">
            <div class="row align-center">
                <div class="col-lg-7">
                    <div class="info wow fadeInLeft">
                        <h2>Wanna build our next version with us?
                        </h2>
                        <p>
                            We are constantly improving the probelogr solution and we are also involved in other projects that you may find interesting,
                            if you wish to volunteer, please send us an email with the detail of how you'd like to volunteer.
                        </p>

                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="thumb wow fadeInUp" data-wow-delay="400ms">
                        <img src="<?= Yii::getAlias('@web'); ?>/assets/anada_theme/img/illustration/undraw_Team_spirit_re_yl1v.svg" alt="Thumb">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Start With Us Area -->

