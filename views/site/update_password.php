<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\AppUsers */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Update Password';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Header -->
<div class="header bg-gradient-primary py-7 py-lg-8">
    <div class="container">
        <div class="header-body text-center mb-7">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-6">
                    <h1 class="text-white">Update Password!</h1>
                    <p class="text-lead text-light">You'll be using this password to login</p>
                </div>
            </div>
        </div>
    </div>
    <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
            <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
        </svg>
    </div>
</div>
<!-- Page content -->
<div class="container mt--8 pb-5">
    <!-- Table -->
    <div class="row justify-content-center">
        <div class="col-lg-6 col-md-8">
            <div class="card bg-secondary shadow border-0">
                <?php if (Yii::$app->session->hasFlash('success')): ?>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?= Yii::$app->session->getFlash('success') ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>


                <?php if (Yii::$app->session->hasFlash('error')): ?>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="alert alert-danger alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>

                                <?= Yii::$app->session->getFlash('error') ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="card-body px-lg-5 py-lg-5">
                    <div class="text-center text-muted mb-4">
                        <small>Enter your Password and Confirm your password</small>
                    </div>
                    <?php $form = \yii\widgets\ActiveForm::begin(); ?>
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'class' => 'form-control form-control-alternative']) ?>
                    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'class' => 'form-control form-control-alternative','placeholder'=>'new password']) ?>
                    <?= $form->field($model, 'confirm_password')->passwordInput(['maxlength' => true, 'class' => 'form-control form-control-alternative']) ?>
                    <div class="text-muted font-italic">
                        <small>You are about to reset your password</small>
                    </div>
                    <div class="row my-4">
                        <div class="col-12">
                            <span class="text-muted">Understand what this means  - <a target="_blank"  href="<?= Yii::getAlias('@web'); ?>/terms-and-conditions">About Update Password</a></span>
                        </div>
                    </div>
                    <div class="text-center">
                        <input type="submit" class="btn btn-primary mt-4" onclick="return confirm('Are you sure you want to update your password?')">
                    </div>
                    <?php \yii\widgets\ActiveForm::end(); ?>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-6">
                    <a href="<?= Yii::getAlias('@web'); ?>/site/reset-password" class="text-light"><small>Reset password again?</small></a>
                </div>
                <div class="col-6 text-right">
                    <a href="<?= Yii::getAlias('@web'); ?>/site/login" class="text-light"><small>Login</small></a>
                </div>
            </div>
        </div>
    </div>
</div>

