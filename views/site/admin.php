<?php
/* @var $this yii\web\View */

$this->title = 'Main Page';
?>
<!-- Event snippet for BT Sign Up conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-854959494/dzixCMyou7gBEIbL1pcD'});
</script>

<!-- Header -->

<div class="row">

    <div class="col-xl-12">
        <div class="card shadow">
            <div class="card-header bg-transparent">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="text-uppercase text-muted ls-1 mb-1">Bold Transaction</h6>
                        <h2 class="mb-0">Getting comfortable <a class="btn btn-sm btn-success" href="javascript:void(0);" onclick="javascript:introJs().start();">Get guide <i class="ni ni-active-40"></i></a></h2>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <!-- Chart -->
                <div class="row text-center mb-">
                    <div class="col-md-12">
                        <div class="card" data-step="1" data-intro="With a transaction you can receive payment" data-position='right' data-scrollTo='tooltip'>
                            <div class="card-body">
                                <h1 class="text-su">Make A Transaction</h1>
                                <p class="text-lead text-">Create the product or service you want to receive payment for</p>
                                <a href="<?= Yii::getAlias('@web'); ?>/transaction/create" class="btn btn-primary text-white">Make A Transaction</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row text-center mb-1">
                    <div class="col-md-12">
                        <div class="card" data-step="2" data-intro="Your profile"  data-position='left'>
                            <div class="card-body">
                                <h1 class="text-su">Update Your Profile</h1>
                                <p class="text-lead text-">You profile contains details about you and your business</p>
                                <a href="<?= Yii::getAlias('@web'); ?>/profile" class="btn btn-primary text-white">Update Profile</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row text-center mb-1">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body" data-step="3" data-intro="Add your payout bank account" data-position='right' data-scrollTo='tooltip'>
                                <h1 class="text-su">Add Bank Account</h1>
                                <p class="text-lead text-">Create the bank account you want to receive money to</p>
                                <a href="<?= Yii::getAlias('@web'); ?>/bank-details-transaction/create" class="btn btn-primary text-white">Add A Bank Account</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
