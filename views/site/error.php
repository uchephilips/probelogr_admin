<?php
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>


<div class="login-form-bg h-100">
    <div class="container h-100">
        <div class="row justify-content-center h-100">
            <div class="col-xl-6">
                <div class="error-content">
                    <div class="card mb-0">
                        <div class="card-body text-center pt-5">
                            <h1 class="error-text text-primary" style="font-size: 2rem;"><?= $name ?></h1>
                            <h4 class="mt-4"><i class="fa fa-thumbs-down text-danger"></i><?= $exception->getMessage() ?></h4>
                            <p><?= $name ?></p>
                            <form class="mt-5 mb-5">

                                <div class="text-center mb-4 mt-4"><a href="<?= Yii::getAlias('@web'); ?>/site" class="btn btn-primary">Go to Homepage</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
