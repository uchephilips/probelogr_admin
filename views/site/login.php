<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="login-form-bg h-100">
    <div class="container h-100">
        <div class="row justify-content-center h-100">
            <div class="col-xl-6">
                <div class="form-input-content">
                    <div class="card login-form mb-0">
                        <div class="card-body pt-5">
                            <a class="text-center" href="<?= Yii::getAlias('@web'); ?>/site">
                                <h4><img src="<?= Yii::getAlias('@web'); ?>/assets/probe_imgs/probelogr.png" style="width: 20%;"></h4>
                            </a>

                            <?php
                            $form = ActiveForm::begin([
                                        'id' => 'login-form',
                            ]);
                            ?>
                            <?php if (Yii::$app->session->hasFlash('success')): ?>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <div class="alert alert-success alert-dismissable">
                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                            <?= Yii::$app->session->getFlash('success') ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if (Yii::$app->session->hasFlash('error')): ?>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <div class="alert alert-danger alert-dismissable">
                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>

                                            <?= Yii::$app->session->getFlash('error') ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="form-group mb-3">
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                    </div>
                                    <input type="email" class="form-control" placeholder="Email" type="email" name="LoginForm[username]">
                                </div>
                                <?= count($model->getErrors("username")) > 0 ? $model->getErrors("username")[0] : "" ?>
                            </div>
                            <div class="form-group">
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                    </div>
                                    <input class="form-control" placeholder="Password" type="password" name="LoginForm[password]">
                                </div>
                                <?= count($model->getErrors("password")) > 0 ? $model->getErrors("password")[0] : "" ?>
                            </div>
                            <div class="custom-control custom-control-alternative custom-checkbox">
                                <input class="custom-control-input" id=" customCheckLogin" name="AppUsers[rememberMe]" type="checkbox">
                                <label class="custom-control-label" for=" customCheckLogin">
                                    <span class="text-muted">Remember me</span>
                                </label>
                            </div>
                            <div class="text-center">
                                <input type="submit" class="btn btn-primary my-4" value="Sign in">
                            </div>
                            <?php ActiveForm::end(); ?>
                            <p class="mt-5 login-form__footer">Do not have account? <a href="<?= Yii::getAlias('@web'); ?>/site/register" class="text-primary">Register</a></p>
                            <p class="mt-3 login-form__footer">Having trouble signing in? <a href="<?= Yii::getAlias('@web'); ?>/site/reset-password" class="text-primary">Reset password</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
