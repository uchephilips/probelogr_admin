<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TeamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $models[] app\models\Team */

$this->title = 'Teams';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];

?>


<!--**********************************
          Content body start
      ***********************************-->
<div class="content-body">

    <!-- row -->
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-tile">Teams</h2>
                        <div class="col text-right">
                            <a href="<?= Yii::getAlias('@web'); ?>/team/create" class="btn btn-lg gradient-9">New Team <i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <?php foreach ($models as $model) { ?>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-center">
                                <span class="display-5"><i class="icon-people gradient-9-text"></i></span>
                                <h4 class="card-widget__title text-dark mt-3"><?= $model->team_name ?></h4>
                                <p class="text-muted"><?= empty($model->description) ? "No Description" : $model->description ?></p>
                                <a class="btn gradient-9 btn-lg border-0 btn-rounded px-5" href="<?= Yii::getAlias('@web'); ?>/team/teammembers?team=<?= $model->id ?>">View Members (<?= $model->member_count?>)</a>
                            </div>
                        </div>
                        <div class="card-footer border-0 bg-transparent">
                            <div class="row">
                                <div class="col-6 border-right-1 pt-3">
                                    <a class="text-center d-block text-muted" href="<?= Yii::getAlias('@web'); ?>/team/update?id=<?= $model->id ?>">
                                        <i class="fa fa-edit gradient-9-text"></i>
                                        <p class="">Update</p>
                                    </a>
                                </div>
                                <div class="col-6 pt-3">
                                    <a class="text-center d-block text-muted" onclick="return confirm('Are you sure sure you delete this record?');" href="<?= Yii::getAlias('@web'); ?>/team/delete?id=<?= $model->id ?>">
                                        <i class="fa fa-close gradient-9-text"></i>
                                        <p class="">Delete</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php
            echo \yii\widgets\LinkPager::widget([
                'pagination' => $pages,
            ]);
            ?>

        </div>
    </div>
    <!-- #/ container -->
</div>
<!--**********************************
    Content body end
***********************************-->

