<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Team */
/* @var $memberModels[] app\models\TeamMembers */

$this->title = 'Team Members for: ' . $model->team_name;
$this->params['breadcrumbs'][] = ['label' => 'Teams', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->team_name, 'url' => ['teammembers', 'team' => $model->id]];
?>


<!--**********************************
          Content body start
      ***********************************-->
<div class="content-body">

    <!-- row -->
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-ttle">Invite Team Members | <?= $model->team_name ?></h2>
                        <div class="col text-left">
                            ...
                        </div>
                        <div class="col text-right">
                            <a href="<?= Yii::getAlias('@web'); ?>/team" class="btn btn-lg gradient-9">Back <i class="fa fa-backward"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">



                        <input type="hidden" name="team_id" value="<?= $model->id ?>">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <label class="control-label" for="appsendpoints-access_key">Team Members</label>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover">
                                               
                                                <?php $form = ActiveForm::begin(); ?>
                                                <tbody>
                                                    <tr>
                                                        <td><input type="email" required placeholder="Email" name="email" class="form-control"></td>
                                                        <td>
                                                            <select name="role" required class="form-control">
                                                                <option>VIEWER</option>
                                                            </select>
                                                        </td>
                                                        
                                                        <td>
                                                            
                                                        </td>
                                                        
                                                        <td>
                                                            <button type="submit" name="form_type" value="Add Member"class="btn gradient-9"> Invite <i class="fa fa-share-alt-square"></i></button>
                                                        </td>
                                                    </tr>
                                                    <?php ActiveForm::end(); ?>
                                                    <?php foreach ($memberModels as $memberModel) { ?>
                                                        <tr>
                                                            <td><strong><?= $memberModel->email ?></strong></td>
                                                            <td><strong><?= $memberModel->role ?></strong></td>
                                                            <td><strong><?= $memberModel->invite_status ?></strong></td>
                                                            <td>
                                                                <a onclick="return confirm('Are you sure you want to delete this record?');" href="<?= Yii::getAlias('@web'); ?>/team/teammembers?team=<?= $model->id ?>&delete_id=<?= $memberModel->id ?>" class="btn btn-sm btn-danger">Delete <i class="fa fa-close"></i></a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>

                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $form = ActiveForm::begin(); ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Html::input('submit', 'form_type', '<< Back', ['class' => 'btn btn-warning text-white']) ?>
                                    <?= Html::input('submit', 'form_type', 'Add Apps >>', ['class' => 'btn btn-success text-white']) ?>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>



                    </div>
                </div>
            </div>




        </div>
    </div>
    <!-- #/ container -->
</div>
<!--**********************************
    Content body end
***********************************-->
<div style="display: none;">
    <li id="newTemp">
        <input type="hidden" name="tagInputs[]" value="">
        <label>
            <i class="icon icon-vector"></i>
            <span >get up</span>
            <a href='javascript:;' class="ti-close"></a>
        </label>
    </li>
</div>

<script>
$(document).ready(function() {
 $('#tagsin').on('keypress', function(e) {
  var regex = new RegExp("^[a-zA-Z ]*$");
  var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
  if (regex.test(str)) {
     return true;
  }
  e.preventDefault();
  return false;
 });
});
</script>  