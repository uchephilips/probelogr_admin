<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'team_name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>


<div class="form-group">

    <?= ($model->isNewRecord) ? "" : Html::input('submit', 'form_type', '<< Save & Go Back', ['class' => 'btn btn-success text-white']) ?>
    <?= Html::input('submit', 'form_type', 'Save Next >>', ['class' => 'btn btn-success text-white']) ?>
</div>

<?php ActiveForm::end(); ?>