<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Team */
/* @var $memberModels[] app\models\TeamMembers */

$this->title = 'Team App Access: ' . $model->team_name;
$this->params['breadcrumbs'][] = ['label' => 'Teams', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->team_name, 'url' => ['teamapps', 'team' => $model->id]];
?>


<!--**********************************
          Content body start
      ***********************************-->
<div class="content-body">

    <!-- row -->
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-ttle">Team App Access | <?= $model->team_name ?></h2>
                        <div class="col text-left">
                            ...
                        </div>
                        <div class="col text-right">
                            <a href="<?= Yii::getAlias('@web'); ?>/team" class="btn btn-lg gradient-9">Back <i class="fa fa-backward"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">



                        <input type="hidden" name="team_id" value="<?= $model->id ?>">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <label class="control-label" for="appsendpoints-access_key">Team Apps</label>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover">

                                                <?php $form = ActiveForm::begin(); ?>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <select name="app_id" required class="form-control select_filter">
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select name="app_tags[]" required class="form-cntrol" id="apptags" multiple="multiple">
                                                            </select>
                                                        </td>

                                                        <td>
                                                            <button type="submit" name="form_type" value="Add App"class="btn gradient-9"> Add <i class="fa fa-plus"></i></button>
                                                        </td>
                                                    </tr>
                                                    <?php ActiveForm::end(); ?>
                                                    <?php foreach ($teamAppModels as $teamAppModel) { ?>
                                                        <tr>
                                                            <td><strong><?= $teamAppModel->name ?></strong></td>
                                                            <td><strong><?= $teamAppModel->tags ?></strong></td>
                                                            <td>
                                                                <a onclick="return confirm('Are you sure you want to delete this record?');"
                                                                   href="<?= Yii::getAlias('@web'); ?>/team/teamapps?team=<?= $model->id ?>&delete_id=<?= $teamAppModel->id ?>"
                                                                   class="btn btn-sm btn-danger">Delete <i class="fa fa-close"></i></a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>


                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $form = ActiveForm::begin(); ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Html::input('submit', 'form_type', '<< Back', ['class' => 'btn btn-warning text-white']) ?>
                                    <?= Html::input('submit', 'form_type', 'Complete', ['class' => 'btn btn-success text-white']) ?>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>



                    </div>
                </div>
            </div>




        </div>
    </div>
    <!-- #/ container -->
</div>
<script>

    $(document).ready(function () {
        $('.select_filter').select2({
            selectOnClose: true,
            width: '100%',
            theme: "classic",
            allowClear: true,
            placeholder: "Select Apps",
            ajax: {
                url: '<?= Yii::getAlias('@web'); ?>/index.php/apps/apps-as-select-option',
                dataType: 'json'
                        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
            }
        });
        $('#apptags').select2({
            selectOnClose: true,
            width: '100%',
            theme: "classic",
            allowClear: true,
            placeholder: "Tags"
        });

        $('.select_filter').on('select2:select', function (e) {

            $("#apptags").val([]).trigger('change')

            $('#apptags').select2({
                width: '100%',
                theme: "classic",
                allowClear: true,
                placeholder: "Tags",
                ajax: {
                    url: '<?= Yii::getAlias('@web'); ?>/index.php/apps/app-tags-as-select-option?term=' + $(this).val(),
                    dataType: 'json'
                            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                }
            });

        });

    });
</script>