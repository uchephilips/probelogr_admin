<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\plt\search\AnswersSearch */
/* @var $models app\models\plt\Avgpageloadtimeymd */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Avgpageloadtimeymds';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
/* @var $this yii\web\View */
/* @var $model app\models\Apps */


$this->title = $model->name . ' Dashboard';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
?>


<!--**********************************
          Content body start
      ***********************************-->
<div class="content-body">

    <!-- row -->
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-tile"><?= $model->name . ' || Page Load Time' ?></h2>
                        <div class="col-md-2" style="float: right;">
                            <form class="input-group mb-3" method="get" id="form1">
                                <select class="custom-select" name="ym">
                                    <?php foreach ($ymds as $ymd) { ?>
                                        <option><?= $ymd->ymd ?></option>
                                    <?php } ?>
                                </select>
                                <button class="input-group-append btn mb-1 btn-outline-dark" type="submit" form="form1" value="Submit">Search</button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="card card-widget">
                    <div class="card-body">
                        <h2 class="font-weight-light text-center text-muted py-3">Bootstrap 4 Timeline</h2>

                        <div class="avgpageloadtimeymd-index">

                            <div class="container py-2">
                                

                                <?php foreach ($models as $model) { ?>
                                    <div class="row">
                                        <div class="col-auto text-center flex-column d-none d-sm-flex">
                                            <div class="row h-50">
                                                <div class="col border-right">&nbsp;</div>
                                                <div class="col">&nbsp;</div>
                                            </div>
                                            <h5 class="m-2">
                                                <span class="badge badge-pill bg-success">&nbsp;</span>
                                            </h5>
                                            <div class="row h-50">
                                                <div class="col border-right">&nbsp;</div>
                                                <div class="col">&nbsp;</div>
                                            </div>
                                        </div>
                                        <div class="col py-2">
                                            <div class="card border-success shadow">
                                                <div class="card-body">
                                                    <div class="float-right text-muted text-monospace"><?= $model->ymd ?></div>
                                                    <h4 class="card-title text-mute text-monospace"><?= $model->currentUrl ?></h4>
                                                    <div class="p-2 text-monospace">
                                                        <div>PLT - <?= app\assets\Misc::formatMilliseconds($model->avgloadtime) ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <!--/row-->
                                <!-- timeline item 3 -->
                                <div class="row">
                                    <div class="col-auto text-center flex-column d-none d-sm-flex">
                                        <div class="row h-50">
                                            <div class="col border-right">&nbsp;</div>
                                            <div class="col">&nbsp;</div>
                                        </div>
                                        <h5 class="m-2">
                                            <span class="badge badge-pill bg-light border">&nbsp;</span>
                                        </h5>
                                        <div class="row h-50">
                                            <div class="col border-right">&nbsp;</div>
                                            <div class="col">&nbsp;</div>
                                        </div>
                                    </div>
                                    <div class="col py-2">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="float-right text-muted">Wed, Jan 11th 2019 8:30 AM</div>
                                                <h4 class="card-title">Day 3 Sessions</h4>
                                                <p>Shoreditch vegan artisan Helvetica. Tattooed Codeply Echo Park Godard kogi, next level irony ennui twee squid fap selvage. Meggings flannel Brooklyn literally small batch, mumblecore PBR try-hard kale chips. Brooklyn vinyl lumbersexual
                                                    bicycle rights, viral fap cronut leggings squid chillwave pickled gentrify mustache. 3 wolf moon hashtag church-key Odd Future. Austin messenger bag normcore, Helvetica Williamsburg sartorial tote bag distillery Portland before
                                                    they sold out gastropub taxidermy Vice.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/row-->
                                <!-- timeline item 4 -->
                                <div class="row">
                                    <div class="col-auto text-center flex-column d-none d-sm-flex">
                                        <div class="row h-50">
                                            <div class="col border-right">&nbsp;</div>
                                            <div class="col">&nbsp;</div>
                                        </div>
                                        <h5 class="m-2">
                                            <span class="badge badge-pill bg-light border">&nbsp;</span>
                                        </h5>
                                        <div class="row h-50">
                                            <div class="col">&nbsp;</div>
                                            <div class="col">&nbsp;</div>
                                        </div>
                                    </div>
                                    <div class="col py-2">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="float-right text-muted">Thu, Jan 12th 2019 11:30 AM</div>
                                                <h4 class="card-title">Day 4 Wrap-up</h4>
                                                <p>Join us for lunch in Bootsy's cafe across from the Campus Center.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/row-->
                            </div>
                            <!--container-->


                        </div>

                    </div>
                </div>
            </div>

        </div>


    </div>
    <!-- #/ container -->
</div>
<!--**********************************
    Content body end
***********************************-->
