<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\plt\search\AnswersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="avgpageloadtimeymd-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'userId') ?>

    <?= $form->field($model, 'appId') ?>

    <?= $form->field($model, 'tag') ?>

    <?= $form->field($model, 'ymd') ?>

    <?php // echo $form->field($model, 'currentUrl') ?>

    <?php // echo $form->field($model, 'avgloadtime') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
