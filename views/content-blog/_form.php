<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\content\ContentBlog */
/* @var $form yii\widgets\ActiveForm */
?>



<div class="content-blog-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'header_image_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'blog_description')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'blog_content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'tags')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tagline')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'author')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_published')->dropDownList(\app\assets\AppAsset::GET_YES_NO(), ['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>

<script>
    $(document).ready(function () {
        $('#contentblog-tagline').on('keypress', function (e) {

            var regexp = /^[a-zA-Z0-9-_]+$/;
            var check = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (!(check.search(regexp) === -1)) {
                return true;
            }

            e.preventDefault();
            return false;
        });
    });
</script>  