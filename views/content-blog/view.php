<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\content\ContentBlog */

$this->title = $model->content_title;
$this->params['breadcrumbs'][] = ['label' => 'Content Blogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>


<!--**********************************
          Content body start
      ***********************************-->
<div class="content-body">

    <!-- row -->
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-ttle"><?= Html::encode($this->title) ?></h2>
                        <div class="col text-right">
                            <a href="<?= Yii::getAlias('@web'); ?>/content-video" class="btn btn-primary">Back <i class="fa fa-backward"></i></a>
                            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                            <?=
                            Html::a('Delete', ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ])
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <?= $model->blog_content ?>
                    </div>
                </div>
            </div>




        </div>
    </div>
    <!-- #/ container -->
</div>
<!--**********************************
    Content body end
***********************************-->

<div class="content-blog-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>



    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'header_image_url:url',
            'content_title',
            'blog_content',
            'tags',
            'tagline',
            'created_by',
            'created_time',
            'update_time',
            'updated_by',
            'is_published',
        ],
    ])
    ?>

</div>
