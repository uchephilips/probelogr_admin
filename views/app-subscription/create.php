<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AppSubscription */

$this->title = 'Create App Subscription';
$this->params['breadcrumbs'][] = ['label' => 'App Subscriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="app-subscription-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
