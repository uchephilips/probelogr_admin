<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\AppSubscriptionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="app-subscription-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'subscription_name') ?>

    <?= $form->field($model, 'subscription_key') ?>

    <?= $form->field($model, 'app_no') ?>

    <?= $form->field($model, 'tag_no') ?>

    <?php // echo $form->field($model, 'user_no') ?>

    <?php // echo $form->field($model, 'storage_no') ?>

    <?php // echo $form->field($model, 'has_support') ?>

    <?php // echo $form->field($model, 'has_dedicated_env') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_time') ?>

    <?php // echo $form->field($model, 'update_time') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'is_active')->checkbox() ?>

    <?php // echo $form->field($model, 'is_suspended')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
