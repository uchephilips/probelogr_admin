<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AppSubscription */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="app-subscription-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'subscription_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'subscription_key')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'app_no')->textInput() ?>

    <?= $form->field($model, 'tag_no')->textInput() ?>

    <?= $form->field($model, 'user_no')->textInput() ?>

    <?= $form->field($model, 'storage_no')->textInput() ?>

    <?= $form->field($model, 'has_support')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'has_dedicated_env')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'created_time')->textInput() ?>

    <?= $form->field($model, 'update_time')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>

    <?= $form->field($model, 'is_suspended')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
