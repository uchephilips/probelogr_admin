<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\AppSubscriptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'App Subscriptions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="app-subscription-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create App Subscription', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'subscription_name',
            'subscription_key',
            'app_no',
            'tag_no',
            //'user_no',
            //'storage_no',
            //'has_support',
            //'has_dedicated_env',
            //'created_by',
            //'created_time',
            //'update_time',
            //'updated_by',
            //'is_active:boolean',
            //'is_suspended:boolean',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
