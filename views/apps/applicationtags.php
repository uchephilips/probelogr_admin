<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Apps */
/* @var $tagsModels[] app\models\AppsTags */

$this->title = 'App Activity Tags: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Apps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['applicationtags', 'app' => $model->id]];
?>


<!--**********************************
          Content body start
      ***********************************-->
<div class="content-body">

    <!-- row -->
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-ttle">Setup Tags | <?= $model->name ?></h2>
                        <div class="col text-left">
                            Your tags will be used to track activities within your application
                        </div>
                        <div class="col text-right">
                            <a href="<?= Yii::getAlias('@web'); ?>/apps" class="btn btn-lg btn-primary">Back <i class="fa fa-backward"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">



                        <input type="hidden" name="app_id" value="<?= $model->id ?>">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <label class="control-label" for="appsendpoints-access_key">Activity Tags</label>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Log&nbsp;Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                        <th>Tag&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                        <th>Email&nbsp;Alert&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                        <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                    </tr>
                                                </thead>
                                                <?php $form = ActiveForm::begin(); ?>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <select name="log_type" class="form-control">
                                                                <option>GENERAL</option>
                                                                <option>SUCCESS</option>
                                                                <option>ERROR</option>
                                                            </select>
                                                        </td>
                                                        <td><input type="input" name="tags" id="tagsin" class="form-control"></td>
                                                        <td>
                                                            <select name="has_email_trigger" class="form-control">
                                                                <option>NO</option>
                                                                <option>YES</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <?= Html::input('submit', 'form_type', 'Add Tag', ['class' => 'btn btn-info']) ?>
                                                        </td>
                                                    </tr>
                                                    <?php ActiveForm::end(); ?>
                                                    <?php foreach ($tagsModels as $tagsModel) { ?>
                                                        <tr>
                                                            <td><strong><?= $tagsModel->log_type ?></strong></td>
                                                            <td><strong><?= $tagsModel->tags ?></strong></td>
                                                            <td><strong><?= $tagsModel->has_email_trigger ?></strong></td>
                                                            <td>
                                                                <a onclick="return confirm('Are you sure you want to delete this record?');" href="<?= Yii::getAlias('@web'); ?>/apps/applicationtags?app=<?= $tagsModel->app_id ?>&delete_id=<?= $tagsModel->id ?>" class="btn btn-sm btn-danger">Delete <i class="fa fa-close"></i></a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>

                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $form = ActiveForm::begin(); ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Html::input('submit', 'form_type', '<< Back', ['class' => 'btn btn-warning']) ?>
                                    <?= Html::input('submit', 'form_type', 'Complete', ['class' => 'btn btn-success']) ?>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>



                    </div>
                </div>
            </div>




        </div>
    </div>
    <!-- #/ container -->
</div>
<!--**********************************
    Content body end
***********************************-->
<div style="display: none;">
    <li id="newTemp">
        <input type="hidden" name="tagInputs[]" value="">
        <label>
            <i class="icon icon-vector"></i>
            <span >get up</span>
            <a href='javascript:;' class="ti-close"></a>
        </label>
    </li>
</div>

<script>
    $(document).ready(function () {
        $('#tagsin').on('keypress', function (e) {

            var regexp = /^[a-zA-Z0-9-_]+$/;
            var check = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (!(check.search(regexp) === -1)) {
                return true;
            }

            e.preventDefault();
            return false;
        });
    });
</script>  