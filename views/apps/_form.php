<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Apps */
/* @var $project app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'project_id')->dropDownList([], ['class' => 'select_filter form-control', 'id' => 'project2', 'style' => 'width:100%;']) ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>



<div class="form-group">

    <?= ($model->isNewRecord) ? "" : Html::input('submit', 'form_type', '<< Save & Go Back', ['class' => 'btn btn-success']) ?>
    <?= Html::input('submit', 'form_type', 'Save Next >>', ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>

<script>

    $(document).ready(function () {
        $('.select_filter').select2({
            selectOnClose: true,
            width: '100%',
            allowClear: true,
            theme: "classic",
            placeholder: "Select Project",
            ajax: {
                url: '<?= Yii::getAlias('@web'); ?>/index.php/project/projects-as-select-option',
                dataType: 'json'
                        // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
            }
        });

<?php if (isset($project)) { ?>

            // Set the value, creating a new option if necessary
            if ($('.select_filter').find("option[value='" + <?= $project->id ?> + "']").length) {
                $('.select_filter').val(<?= $project->id ?>).trigger('change');
            } else {
                // Create a DOM Option and pre-select by default
                var newOption = new Option('<?= $project->name ?>', <?= $project->id ?>, true, true);
                // Append it to the select
                $('.select_filter').append(newOption).trigger('change');
            }
<?php } ?>

    });
</script>