<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Apps */
/* @var $tagsModels[] app\models\AppsTags */
/* @var $endpoint app\models\AppsEndpoints */

$this->title = 'App Info: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Apps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'App info | ' . $model->name, 'url' => ['view', 'id' => $model->id]];
?>


<!--**********************************
          Content body start
      ***********************************-->
<div class="content-body">

    <!-- row -->
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">
                        <h2 class="card-ttle">App info | <?= $model->name ?></h2>
                        <div class="col text-left">
                            View the details of your application.
                        </div>
                        <div class="col text-right">
                            <a href="<?= Yii::getAlias('@web'); ?>/apps" class="btn btn-lg btn-primary">Back <i class="fa fa-backward"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header" >
                        <div class="custom-tab-1">
                            <ul class="nav nav-tabs mb-3">
                                <li class="nav-item ">
                                    <a class="nav-link" href="<?= Yii::getAlias('@web'); ?>/app-dashboard?app=<?= $model->id ?>&<?= $shared ? 'shared' : '' ?>">Dashboard <i class="fa fa-dashboard"></i></a>
                                </li>

                                <li class="nav-item">
                                    <a href="javascript:;" class="nav-link dropdown-toggle" data-toggle="dropdown">View Logs</a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" target="_BLANK" href="<?= Yii::getAlias('@web'); ?>/app-logs/real-time?id=<?= $model->id ?>&<?= $shared ? 'shared' : '' ?>">Real-time Logs</a>
                                        <a class="dropdown-item" target="_BLANK" href="<?= Yii::getAlias('@web'); ?>/app-logs/past?id=<?= $model->id ?>&<?= $shared ? 'shared' : '' ?>">Past Logs</a>
                                    </div>
                                </li>
                                <?php
                                if ($shared) {
                                    if ($model->invite_status == "ADMIN") {
                                        ?>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="modal" data-target="#shareModal" href="<?= Yii::getAlias('@web'); ?>/apps/share?id=<?= $model->id ?>">Share Settings <i class="fa fa-share"></i></a>
                                        </li>

                                        <li class="nav-item ">
                                            <a class="nav-link" href="<?= Yii::getAlias('@web'); ?>/apps/update?id=<?= $model->id ?>">Update <i class="fa fa-edit"></i></a>
                                        </li>

                                        <li class="nav-item">
                                            <a onclick="return confirm('Are you sure sure you delete this record?');" class="nav-link" href="<?= Yii::getAlias('@web'); ?>/apps/delete?id=<?= $model->id ?>">Delete <i class="fa fa-close"></i></a>
                                        </li>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <li class="nav-item">
                                        <a class="nav-link"  data-toggle="modal" data-target="#shareModal" href="<?= Yii::getAlias('@web'); ?>/apps/share?id=<?= $model->id ?>">Share Settings <i class="fa fa-share"></i></a>
                                    </li>

                                    <li class="nav-item ">
                                        <a class="nav-link" href="<?= Yii::getAlias('@web'); ?>/apps/update?id=<?= $model->id ?>">Update <i class="fa fa-edit"></i></a>
                                    </li>

                                    <li class="nav-item">
                                        <a onclick="return confirm('Are you sure sure you delete this record?');" class="nav-link" href="<?= Yii::getAlias('@web'); ?>/apps/delete?id=<?= $model->id ?>">Delete <i class="fa fa-close"></i></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="media align-items-center mb-4">
                            <span class="display-5 mr-3">
                                <?php if ($shared) { ?>
                                    <i class="icon-screen-desktop gradient-9-text"></i>
                                <?php } else { ?>
                                    <i class="icon-screen-desktop gradient-93-text"></i>
                                <?php } ?>
                            </span>
                            <div class="media-body">
                                <h3 class="mb-0"><?= $model->name ?></h3>
                                <p class="text-muted mb-0"><?= $model->project_name ?></p>
                            </div>
                        </div>



                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Access Key&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><?= $endpoint->access_key ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <br>
                        <br>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Log&nbsp;Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                        <th>Tag&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                        <th>Email&nbsp;Alert&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($tagsModels as $tagsModel) { ?>
                                        <tr>
                                            <td><?= $tagsModel->log_type ?></td>
                                            <td><?= $tagsModel->tags ?></td>
                                            <td><?= $tagsModel->has_email_trigger ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php if (isset($teams)) { ?>
                                <br>
                                <br>
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Teams&nbsp;Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                            <th>Members&nbsp;count&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($teams as $team) { ?>
                                            <tr>
                                                <td><?= $team->team_name ?></td>
                                                <td><?= $team->team_members_count ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            <?php } ?>
                        </div>

                        <!-- <ul class="card-profile__info">
                             <li class="mb-1"><strong class="text-dark mr-4">Mobile</strong> <span>01793931609</span></li>
                             <li><strong class="text-dark mr-4">Email</strong> <span>name@domain.com</span></li>
                         </ul>-->
                    </div>
                </div>  
            </div>
        </div>

    </div>
    <!-- #/ container -->
</div>
<!--**********************************
    Content body end
***********************************-->
<div class="modal fade" id="shareModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php $form = ActiveForm::begin(['action' => ['apps/share?app=' . $model->id]]); ?>
            <div class="modal-header">
                <h5 class="modal-title">Share Settings</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="text-muted">Enter the email of the person you want to share this application settings to</p>
                <div class="col-auto">
                    <label class="sr-only">Email</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text">@</div>
                        </div>
                        <input type="text" name="share_email" autocomplete="off" class="form-control" placeholder="Email">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Share</button>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>