<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Apps */
/* @var $endpoint app\models\AppsEndpoints */

$this->title = 'App Access Token: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Apps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['accessendpoint', 'app' => $model->id]];
?>


<!--**********************************
          Content body start
      ***********************************-->
<div class="content-body">

    <!-- row -->
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-ttle">Setup Access Key | <?= $model->name ?></h2>
                        <div class="col text-left">
                            Your Access Key should be used within the API
                        </div>
                        <div class="col text-right">
                            <a href="<?= Yii::getAlias('@web'); ?>/apps" class="btn btn-lg btn-primary">Back <i class="fa fa-backward"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">


                        <?php $form = ActiveForm::begin(); ?>
                        <input type="hidden" name="app_id" value="<?= $model->id ?>">
                        <div class="row">
                            <div class="col-md-6">
                                <label>ACCESS KEY</label>
                                <div class="input-group mb-3">
                                    <input type="text" name="access_key" id="accessKey" placeholder="Access Key" class="form-control" required readonly value="<?= $endpoint->access_key ?>">

                                    <div class="input-group-append">
                                        <button class="btn btn-info" id="genKey" type="button"><?= !isset($endpoint->access_key) ? "GENERATE" : "RESET" ?> <i id="accessKeyLoader" class="loader"></i></button>
                                    </div>

                                </div>

                                <span class="help-block">Click on the Generate or Reset Button to create a new access key.</span>
                                <br>
                                <br>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= Html::input('submit', 'form_type', '<< Save Back', ['class' => 'btn btn-success']) ?>
                                    <?= Html::input('submit', 'form_type', 'Save Next >>', ['class' => 'btn btn-success']) ?>
                                </div>
                            </div>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>




        </div>
    </div>
    <!-- #/ container -->
</div>
<!--**********************************
    Content body end
***********************************-->
<div style="display: none;">
    <li id="newTemp">
        <input type="hidden" name="tagInputs[]" value="">
        <label>
            <i class="icon icon-vector"></i>
            <span >get up</span>
            <a href='javascript:;' class="ti-close"></a>
        </label>
    </li>
</div>

<script>
    $(document).ready(function () {

        var accessKeyLoader = $('#accessKeyLoader');

        $('#genKey').click(function () {
            accessKeyLoader.show();
            $.get("<?= Yii::getAlias('@web'); ?>/apps/gen-access-key", function (data, status) {
                $('#accessKey').val(data);

            }).always(function () {
                accessKeyLoader.hide();
            });


        });
    });
</script>