<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\AppsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $models[] app\models\Apps */

$this->title = 'Apps';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
?>


<!--**********************************
          Content body start
      ***********************************-->
<div class="content-body">

    <!-- row -->
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-tile">Apps</h2>
                        <div class="col text-right">
                            <a href="<?= Yii::getAlias('@web'); ?>/apps/create" class="btn btn-lg btn-primary">New App <i class="fa fa-plus"></i></a>
                        </div>
                        <div class="col text-right">
                            <?php // $this->render('_search',['model'=>$searchModel])?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <div class="text-center">
                            <span class="display-5"><i class="icon-people gradient-9-text"></i></span>
                            <h2 class="mt-3"><?= $sharedAppCount->app_count ?> Shared Apps</h2>                        
                            <p class="text-muted">Apps shared with you.</p>
                            <a href="<?= Yii::getAlias('@web'); ?>/apps/sharedapps" class="btn gradient-9 btn-lg border-0 btn-rounded px-5">
                                View Shared Apps</a>
                        </div>
                        <br>
                        <br>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
            <?php foreach ($models as $model) { ?>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-center">
                                <span class="display-5"><i class="icon-screen-desktop gradient-3-text"></i></span>
                                <h4 class="card-widget__title text-dark mt-3"><?= $model->name ?> | <?= $model->project_name ?></h4>
                                <p class="text-muted"><?= empty($model->description) ? "No Description" : $model->description ?></p>
                                <?php if ($model->has_settings > 0 && $model->has_tags > 0) { ?>
                                    <div role="group" class="btn-group">
                                        <button data-toggle="dropdown" class="btn gradient-4 btn-lg border-0 btn-rounded px-5 dropdown-toggle" type="button" >Logs</button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" target="_BLANK" href="<?= Yii::getAlias('@web'); ?>/app-logs/real-time?id=<?= $model->id ?>">Real-time Logs</a>
                                            <a class="dropdown-item" target="_BLANK" href="<?= Yii::getAlias('@web'); ?>/app-logs/past?id=<?= $model->id ?>">Past Logs</a>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <a class="btn gradient-4 btn-lg border-0 btn-rounded px-5" href="<?= Yii::getAlias('@web'); ?>/apps/<?= ($model->has_settings == 0) ? "accessendpoint" : "applicationtags" ?>?app=<?= $model->id ?>">
                                        <i class="fa fa-cogs gradient-4-text" aria-hidden="true"></i> Finish App Setup</a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="card-footer border-0 bg-transparent">
                            <div class="row">
                                <div class="col-4 border-right-1 pt-3 " >
                                    <a href="javascript:;" class="text-center d-block text-muted "  data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-cogs gradient-2-text " aria-hidden="true"></i>
                                        <p>Settings <i class="fa fa-caret-down"></i></p>
                                    </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="<?= Yii::getAlias('@web'); ?>/apps/appinfo?app=<?= $model->id ?>">App info</a>
                                        <a class="dropdown-item" href="<?= Yii::getAlias('@web'); ?>/apps/accessendpoint?app=<?= $model->id ?>">App Access Key</a>
                                        <a class="dropdown-item" href="<?= Yii::getAlias('@web'); ?>/apps/applicationtags?app=<?= $model->id ?>">Activity Tags</a>
                                    </div>

                                </div>
                                <div class="col-4 border-right-1 pt-3">
                                    <a class="text-center d-block text-muted" href="<?= Yii::getAlias('@web'); ?>/apps/update?id=<?= $model->id ?>">
                                        <i class="fa fa-edit gradient-3-text"></i>
                                        <p class="">Update</p>
                                    </a>
                                </div>
                                <div class="col-4 pt-3"><a onclick="return confirm('Are you sure sure you delete this record?');" class="text-center d-block text-muted" href="<?= Yii::getAlias('@web'); ?>/apps/delete?id=<?= $model->id ?>">
                                        <i class="fa fa-close gradient-1-text"></i>
                                        <p class="">Delete</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php
            echo \yii\widgets\LinkPager::widget([
                'pagination' => $pages,
            ]);
            ?>

        </div>
    </div>
    <!-- #/ container -->
</div>
<!--**********************************
    Content body end
***********************************-->
