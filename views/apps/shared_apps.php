<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\AppsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $models[] app\models\Apps */

$this->title = 'Shared Apps';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
?>


<!--**********************************
          Content body start
      ***********************************-->
<div class="content-body">

    <!-- row -->
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-tile">Shared Apps</h2>
                        <div class="col text-right">
                            <a href="<?= Yii::getAlias('@web'); ?>/apps" class="btn btn-lg btn-primary">Back <i class="fa fa-backward"></i></a>
                        </div>
                        <div class="col text-right">
                            <?php // $this->render('_search',['model'=>$searchModel])?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <?php foreach ($models as $model) { ?>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-center">
                                <span class="display-5">
                                    <i class="icon-screen-desktop gradient-9-text"></i>
                                </span>
                                <h4 class="card-widget__title text-dark mt-3"><?= $model->name ?> | <?= $model->project_name ?></h4>
                                <p class="text-muted"><?= empty($model->description) ? "No Description" : $model->description ?></p>
                                <?php if ($model->has_settings > 0 && $model->has_tags > 0) { ?>
                                    <div role="group" class="btn-group">
                                        <button data-toggle="dropdown" class="btn gradient-4 btn-lg border-0 btn-rounded px-5 dropdown-toggle" type="button" >Logs</button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" target="_BLANK" href="<?= Yii::getAlias('@web'); ?>/app-logs/real-time?id=<?= $model->id ?>&shared">Real-time Logs</a>
                                            <a class="dropdown-item" target="_BLANK" href="<?= Yii::getAlias('@web'); ?>/app-logs/past?id=<?= $model->id ?>&shared">Past Logs</a>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <a class="btn gradient-4 btn-lg border-0 btn-rounded px-5" href="<?= Yii::getAlias('@web'); ?>/apps/<?= ($model->has_settings == 0) ? "accessendpoint" : "applicationtags" ?>?app=<?= $model->id ?>">
                                        <i class="fa fa-cogs gradient-4-text" aria-hidden="true"></i> Finish App Setup</a>
                                <?php } ?>
                                <br>
                                <div class="text-muted mt-1"><strong>Shared by : </strong><?= $model->shared_by ?></div>
                                <div class="text-muted "><strong>Team : </strong><?= $model->team_name ?></div>
                            </div>
                        </div>
                        <div class="card-footer border-0 bg-transparent">
                            <div class="row">
                                <div class="col-12 border-top-1 pt-3 " >
                                    <a href="<?= Yii::getAlias('@web'); ?>/apps/appinfo?app=<?= $model->id ?>&shared" class="text-center d-block text-muted " >
                                        <i class="fa fa-info gradient-2-text " aria-hidden="true"></i>
                                        <p>App info</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php
            echo \yii\widgets\LinkPager::widget([
                'pagination' => $pages,
            ]);
            ?>

        </div>
    </div>
    <!-- #/ container -->
</div>
<!--**********************************
    Content body end
***********************************-->
