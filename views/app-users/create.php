<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AppUsers */

$this->title = 'Create App Users';
$this->params['breadcrumbs'][] = ['label' => 'App Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?=

$this->render('_form', [
    'model' => $model,
])
?>
