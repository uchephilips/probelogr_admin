<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AppUsers */
/* @var $aubp app\models\AppUserBusinessProfile */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="row">
    <div class="col-xl-12">
        <div class="card shadow well">
            <div class="card-header bg-transparent">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="text-uppercase text-muted ls-1 mb-1"><?= $this->title ?></h6>

                    </div>
                    <div class="col text-right">
                        <a href="<?= Yii::getAlias('@web'); ?>/<?= Yii::$app->controller->id ?>" class="btn btn-lg btn-default"><i class="fa fa-arrow-circle-left"></i> Back</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row ">
                    <div class="col-md-12">

                        <?php $form = ActiveForm::begin(); ?>

                        <?= $form->field($model, 'first_name')->textInput(['maxlength' => true, 'required' => 'required', 'autocomplete' => 'off']) ?>

                        <?= $form->field($model, 'last_name')->textInput(['maxlength' => true, 'required' => 'required', 'autocomplete' => 'off']) ?>

                        <?= $form->field($model, 'phone_number')->textInput(['pattern' => '\d*', 'minlength' => 10, 'type' => 'text', 'required' => 'required', 'autocomplete' => 'off']) ?>

                        <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'type' => 'email', 'required' => 'required', 'autocomplete' => 'off']) ?>

                        <?= $form->field($model, 'role_id')->dropDownList(\yii\helpers\ArrayHelper::map(app\models\Roles::find()->orderBy('role_name', SORT_ASC)->all(), 'id', 'role_name'), ['prompt' => 'select role', 'id' => 'select_state_id'], ['required' => 'required']); ?>
                        
                       

                        <div class="form-group">
                            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $('#update_password').change(function () {
        $('#pass_input').toggle();
    });
</script>