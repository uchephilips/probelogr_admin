<?php
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\AppUsersSearch */
/* @var $model app\models\AppUsers */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'App Users';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">

    <div class="col-xl-12">
        <div class="card shadow">
            <div class="card-header bg-transparent">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="text-uppercase text-muted ls-1 mb-1">Probelogr</h6>
                        <h2 class="mb-0">Platform Users</h2>
                    </div>
                    <div class="col text-right">
                        <!--<a href="<?= Yii::getAlias('@web'); ?>/<?= Yii::$app->controller->id ?>/create" class="btn btn-lg btn-primary">Add User <i class="fa fa-plus"></i></a>-->
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="box-body table-responsive no-padding">
                    <div id="dvData" class="box-body table-responsive no-padding">
                        <table  class="table table-hover">
                            <tbody>
                                <tr>
                                    <th>(id)User Name</th>
                                    <th>Profile</th>
                                    <th>Email</th>
                                    <th>Phone Number </th>
                                    <th>Roles</th>
                                    <th>Created Date</th>
                                    <th>Last Login Date</th>
                                    <th>Status </th>

                                </tr>

                                <?php foreach ($models as $model) { ?>

                                    <tr>
                                        <td>(<?= $model->id ?>)<?= $model->first_name . ' ' . $model->last_name ?>
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="icon icon-list"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                    <a class="dropdown-item" href="<?= Yii::getAlias('@web'); ?>/app-users/view?id=<?= $model->id ?>">View</a>
                                                    <?php if (app\assets\RoleManagement::checkIfUserHasPrivilege('app-users', 'update')) { ?>
                                                        <a class="dropdown-item" href="<?= Yii::getAlias('@web'); ?>/app-users/update?id=<?= $model->id ?>">Update</a>
                                                    <?php } ?>
                                                    <?php if (app\assets\RoleManagement::checkIfUserHasPrivilege('app-users', 'suspend')) { ?>
                                                        <a class="dropdown-item" href="<?= Yii::getAlias('@web'); ?>/app-users/suspend?id=<?= $model->id ?>"
                                                           onclick="return confirm('Are you sure you want to make this action ?')"><?= $model->is_active == 1 ? 'Suspend' : "Unsuspend" ?></a>
                                                       <?php } ?>
                                                       <?php if (app\assets\RoleManagement::checkIfUserHasPrivilege('app-users', 'reset-password')) { ?>
                                                        <a class="dropdown-item" href="<?= Yii::getAlias('@web'); ?>/app-users/reset-password?id=<?= $model->id ?>">Reset password</a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </td>
                                        <td><?= $model->company_name ?></td>
                                        <td><?= $model->email ?></td>
                                        <td><?= $model->phone_number ?></td>
                                        <td><?= $model->role_name ?></td>
                                        <td><?= $model->create_time ?> (<?= \app\assets\Misc::time_elapsed_string($model->create_time) ?>)</td>
                                        <td><?= $model->last_login ?> (<?= \app\assets\Misc::time_elapsed_string($model->last_login) ?>)</td>
                                        <td><span class="label label-<?= $model->is_active == 1 ? 'success' : 'warning' ?>"><?= $model->is_active == 1 ? "Active" : "Suspended" ?></span></td>


                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <?php
                        echo \yii\widgets\LinkPager::widget([
                            'pagination' => $pages,
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("input").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            $("form").submit();
        }
    });
</script>

<!--
<a href="<?= Yii::getAlias('@web'); ?>/app-users/export" class="btn btn-success btn-sm" aria-expanded="false">
                            Download &nbsp;<i class="fa fa-download"></i> 
                        </a>
                        <a href="<?= Yii::getAlias('@web'); ?>/app-users/create" class="btn btn-success btn-sm " aria-expanded="false">
                            Add &nbsp;<i class="fa fa-plus"></i> 
                        </a>
-->