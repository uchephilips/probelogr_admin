<?php
use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\VideoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/* @var $models[] app\models\content\ContentVideo */

$this->title = 'Videos';
$this->params['breadcrumbs'][] = $this->title;
?>


<!-- Start Breadcrumb 
============================================= -->
<div class="breadcrumb-area gradient-bg text-light text-center">
    <!-- Fixed BG -->
    <div class="fixed-bg" style="background-image: url(<?= Yii::getAlias('@web'); ?>/assets/anada_theme/img/shape/1.png);"></div>
    <!-- Fixed BG -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h1>How-To Videos</h1>
                <ul class="breadcrumb">
                    <li><a href="<?= Yii::getAlias('@web'); ?>"><i class="fas fa-home"></i>Home</a></li>
                    <li class="active">How-To Videos</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Breadcrumb -->

<!-- Star Projects
============================================= -->
<div class="projects-area overflow-hidden default-padding">
    <!-- Fixed BG -->
    <div class="fixed-bg" style="background-image: url(<?= Yii::getAlias('@web'); ?>/assets/anada_theme/img/shape/2.png);"></div>
    <!-- Fixed BG -->
    <div class="container">
        <div class="project-items-area">
            <div class="row">
                <div class="col-lg-12">
                    <div class="gallery-content">

                        <!-- End Mixitup Nav-->

                        <div class="magnific-mix-gallery masonary">
                            <div id="portfolio-grid" class="gallery-items projects-items colums-2">
                                <?php foreach ($models as $contentVideo) { ?>
                                <!-- Single Item -->
                                <div class="pf-item development capital">
                                    <div class="single-item">
                                        <div class="thumb item-effect">
                                            <img src="https://img.youtube.com/vi/<?= $contentVideo->video_url ?>/maxresdefault.jpg" alt="Thumb">
                                            <div class="effect-info">
                                                <a href="https://www.youtube.com/watch?v=<?= $contentVideo->video_url ?>" class="popup-youtube">
                                                    <i class="fas fa-video"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="info">
                                            <h5>
                                                <a href="#"><?= $contentVideo->video_title ?></a>
                                            </h5>
                                            <ul>
                                                <?php
                                                $tags = explode(",", $contentVideo->tags);
                                                foreach ($tags as $tag) {
                                                ?>
                                                <li>
                                                    <a href="#"><?= $tag ?></a>
                                                </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php
                echo \yii\widgets\LinkPager::widget([
                    'pagination' => $pages,
                ]);
                ?>
            </div>
        </div>
    </div>
</div>
<!-- End Projects -->

<!--
<div class="content-video-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Content Video', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(Html::encode($model->id), ['view', 'id' => $model->id]);
        },
    ])
    ?>
</div>

-->