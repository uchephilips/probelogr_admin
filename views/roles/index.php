<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\RolesSearch */
/* @var $model app\models\Roles */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Roles';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">

    <div class="col-xl-12">
        <div class="card shadow">
            <div class="card-header bg-transparent">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="text-uppercase text-muted ls-1 mb-1">Bold Transaction</h6>
                        <h2 class="mb-0">System Roles</h2>
                    </div>
                    <div class="col text-right">
                        <a href="<?= Yii::getAlias('@web'); ?>/<?= Yii::$app->controller->id ?>/create" class="btn btn-lg btn-primary">Add Role <i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>Role</th>
                                <th>Created By</th>
                                <th>Created Time</th>
                                <th>Status </th>
                                <th> </th>

                            </tr>

                            <?php foreach ($models as $model) { ?>
                                <tr>
                                    <td><?= $model->role_name ?></td>
                                    <td><?= $model->getCreatorName(); ?></td>
                                    <td><?= $model->created_time ?></td>
                                    <td><span class="label label-<?= $model->is_active == 1 ? 'success' : 'warning' ?>"><?= $model->is_active == 1 ? "Active" : "Suspended" ?></span></td>
                                    <td>
                                        <?php if ($model->is_editable) { ?>
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a class="dropdown-item" href="<?= Yii::getAlias('@web'); ?>/<?= Yii::$app->controller->id ?>/delete?id=<?= $model->id ?>"
                                                   onclick="return confirm('Are you sure you want to perform this action?');"
                                                   ><?= $model->is_active == 1 ? 'Suspend' : "Unsuspend" ?></a>
                                                <a class="dropdown-item" href="<?= Yii::getAlias('@web'); ?>/<?= Yii::$app->controller->id ?>/update?id=<?= $model->id ?>">Update</a>
                                                <!--<a class="dropdown-item" href="#">View Transaction</a>-->
                                            </div>
                                        </div>
                                            
                                        <?php } else { ?>
                                            <label class="label label-default">Default</label>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
           </div>
        </div>
    </div>
</div>
