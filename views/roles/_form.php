<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Roles */
/* @var $pages[] app\models\PageActions */
/* @var $actions[] app\models\PageActions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-xl-12">
        <div class="card shadow well">
            <div class="card-header bg-transparent">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="text-uppercase text-muted ls-1 mb-1"><?= $this->title ?></h6>

                    </div>
                    <div class="col text-right">
                        <a href="<?= Yii::getAlias('@web'); ?>/<?= Yii::$app->controller->id?>" class="btn btn-lg btn-default"><i class="fa fa-arrow-circle-left"></i> Back</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row ">
                    <div class="col-md-12">

                        <?php $form = ActiveForm::begin(); ?>

                        <?= $form->field($model, 'role_name')->textInput(['maxlength' => true]) ?>

                        <hr>
                        <?php foreach ($pages as $page) { ?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-3">

                                        <label class="inppt_container"><b><?= $page->controller_name ?></b>
                                            <input type="checkbox" name="page[]" value="<?= $page->controller_id ?>">
                                            <span class="checkmark"></span>
                                        </label>

                                    </div>
                                </div>
                                <div class="row">
                                    <?php foreach ($page->getActions() as $actions) { ?>
                                        <div class="col-md-2">
                                            <label class="inppt_container"><?= $actions->action_name ?>
                                                <input type="checkbox" name="actions[]" <?= $actions->hasAction($model->id) ? "checked" : "" ?> value="<?= $actions->id ?>">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>


                        <div class="form-group">
                            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

