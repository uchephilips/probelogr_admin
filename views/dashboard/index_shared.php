<?php
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\AppsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $models[] app\models\Apps */

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
?>



<!--**********************************
          Content body start
      ***********************************-->
<div class="content-body">

    <!-- row -->
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-tile">Shared Dashboard</h2>
                        <div class="col text-right">
                            <?php // $this->render('_search',['model'=>$searchModel])?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">

                        <?php foreach ($models as $model) { ?>
                            <div class="col-xl-3 col-lg-6 col-sm-6 col-xxl-6">

                                <div class="card">
                                    <div class="chart-wrapper mb-4">
                                        <div class="px-4 pt-4 d-flex justify-content-between">
                                            <div>
                                                <h4><?= $model->name ?> Activities</h4>
                                                <p style="color: #979797">
                                                    <strong>Project:</strong> <?= $model->project_name ?><br>
                                                    <strong>Team:</strong> <?= $model->team_name ?><br>
                                                    <strong>Shared By:</strong> <?= $model->shared_by ?>
                                                </p>
                                                <a class="btn btn-info btn-rounded btn-sm" href="<?= Yii::getAlias('@web'); ?>/app-dashboard?app=<?= $model->id ?>&shared">View</a>
                                            </div>
                                            <div>
                                                <strong>
                                                    AVG</strong>
                                                <h4 class="d-inline-block text-info" id="avg<?= $model->id ?>">0</h4>
                                                <p>
                                                    <i class="" id="trend_path_<?= $model->id ?>"></i>
                                                    <span class="" id="trend_<?= $model->id ?>" ></span>
                                                </p>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="card-body border-top pt-4">

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <canvas id="<?= 'barchart_' . $model->id ?>"></canvas>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <canvas id="<?= 'chart_' . $model->id ?>" height="100px"></canvas>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <ul>
                                                    <li>
                                                        <h5 class="Sub-heading">Highest activity</h5>

                                                        <h6 class="Sub-heading" id="ha_<?= $model->id ?>"></h6>
                                                    </li>
                                                    <li>
                                                        <h5 class="Sub-heading">Lowest activity</h5>
                                                        <h6 class="Sub-heading" id="la_<?= $model->id ?>"></h6>
                                                    </li>
                                                </ul>

                                            </div>
                                            <div class="col-sm-6">
                                                <div>
                                                    <h5>Total Activity</h5>
                                                    <h3 id="tot_act_<?= $model->id ?>">0</h3>
                                                </div>
                                            </div>
                                            <?= $this->render('_app_monthly_report', ['chartId' => 'chart_' . $model->id, 'chartPieId' => 'piechart_' . $model->id, 'model' => $model]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- #/ container -->
</div>
<!--**********************************
    Content body end
***********************************-->

<script>



</script>