<?php
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\AppsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $models[] app\models\Apps */

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
?>



<!--**********************************
          Content body start
      ***********************************-->
<div class="content-body">

    <!-- row -->
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-tile">Dashboard</h2>
                        <div class="col text-right">
                            <?php // $this->render('_search',['model'=>$searchModel])?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <div class="card gradient-1">
                    <div class="card-body">
                        <h3 class="card-title text-white">Projects</h3>
                        <div class="d-inline-block">
                            <h2 class="text-white"><?= $totalProjects ?></h2>
                            <p class="text-white mb-0"></p>
                        </div>
                        <span class="float-right display-5 opacity-5"><i class="icon-folder-alt"></i></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="card gradient-2">
                    <div class="card-body">
                        <h3 class="card-title text-white">Total Apps</h3>
                        <div class="d-inline-block">
                            <h2 class="text-white"><?= $totalApps ?></h2>
                            <p class="text-white mb-0"></p>
                        </div>
                        <span class="float-right display-5 opacity-5"><i class="icon-screen-desktop"></i></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="card gradient-3">
                    <div class="card-body">
                        <h3 class="card-title text-white">Total Activities</h3>
                        <div class="d-inline-block">
                            <h2 class="text-white"><?= $totalTags ?></h2>
                            <p class="text-white mb-0"></p>
                        </div>
                        <span class="float-right display-5 opacity-5"><i class="icon icon-vector"></i></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="text-center">
                                        <span class="display-5"><i class="icon-people gradient-9-text"></i></span>
                                        <h2 class="mt-3"><?= $sharedAppCount->app_count ?> Shared Apps</h2>                        
                                        <p class="text-muted">Apps shared with you.</p>
                                        <a href="<?= Yii::getAlias('@web'); ?>/dashboard/shared-dashboard" class="btn gradient-9 btn-lg border-0 btn-rounded px-5">
                                            View Dashboard</a>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </div>
                        <?php foreach ($models as $model) { ?>
                            <div class="col-xl-3 col-lg-6 col-sm-6 col-xxl-6">

                                <div class="card">
                                    <div class="chart-wrapper mb-4">
                                        <div class="px-4 pt-4 d-flex justify-content-between">
                                            <div>
                                                <h4><?= $model->name ?> Activities</h4>
                                                <p><?= $model->project_name ?> <a class="btn btn-info btn-rounded btn-sm" href="<?= Yii::getAlias('@web'); ?>/app-dashboard?app=<?= $model->id ?>">View</a></p>
                                            </div>
                                            <div>
                                                <strong>
                                                    AVG</strong>
                                                <h4 class="d-inline-block text-info" id="avg<?= $model->id ?>">0</h4>
                                                <p>
                                                    <i class="" id="trend_path_<?= $model->id ?>"></i>
                                                    <span class="" id="trend_<?= $model->id ?>" ></span>
                                                </p>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="card-body border-top pt-4">

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <canvas id="<?= 'barchart_' . $model->id ?>"></canvas>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <canvas id="<?= 'chart_' . $model->id ?>" height="100px"></canvas>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <ul>
                                                    <li>
                                                        <h5 class="Sub-heading">Highest activity</h5>

                                                        <h6 class="Sub-heading" id="ha_<?= $model->id ?>"></h6>
                                                    </li>
                                                    <li>
                                                        <h5 class="Sub-heading">Lowest activity</h5>
                                                        <h6 class="Sub-heading" id="la_<?= $model->id ?>"></h6>
                                                    </li>
                                                </ul>

                                            </div>
                                            <div class="col-sm-6">
                                                <div>
                                                    <h5>Total Activity</h5>
                                                    <h3 id="tot_act_<?= $model->id ?>">0</h3>
                                                </div>
                                            </div>
                                            <?= $this->render('_app_monthly_report_shared', ['chartId' => 'chart_' . $model->id, 'chartPieId' => 'piechart_' . $model->id, 'model' => $model]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- #/ container -->
</div>
<!--**********************************
    Content body end
***********************************-->

<script>



</script>