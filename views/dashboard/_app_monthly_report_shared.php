<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>


<script>
    (function ($) {
        "use strict"

        let ctx = document.getElementById("<?= $chartId ?>");
        //ctx.height = 130;
        var chart_<?= $model->id ?> = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [],
                type: 'line',
                title: 'line',
                defaultFontFamily: 'Montserrat',
                datasets: [{
                        data: [],
                        label: "",
                        backgroundColor: 'transparent',
                        borderColor: '#847DFA',
                        borderWidth: 2,
                        pointStyle: 'circle',
                        pointRadius: 5,
                        pointBorderColor: '#847DFA',
                        pointBackgroundColor: '#fff',
                    }]
            },
            options: {
                responsive: !0,
                maintainAspectRatio: true,
                tooltips: {
                    mode: 'index',
                    titleFontSize: 12,
                    titleFontColor: '#fff',
                    bodyFontColor: '#fff',
                    backgroundColor: '#000',
                    titleFontFamily: 'Montserrat',
                    bodyFontFamily: 'Montserrat',
                    cornerRadius: 3,
                    intersect: false,
                },
                legend: {
                    display: false,
                    position: 'top',
                    labels: {
                        usePointStyle: true,
                        fontFamily: 'Montserrat',
                    },

                },
                scales: {
                    xAxes: [{
                            display: false,
                            gridLines: {
                                display: false,
                                drawBorder: false
                            },
                            scaleLabel: {
                                display: false,
                                labelString: 'Month'
                            }
                        }],
                    yAxes: [{
                            display: false,
                            gridLines: {
                                display: false,
                                drawBorder: false
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'Value'
                            }
                        }]
                },
                title: {
                    display: true,
                }
            }
        });


        var jqxhr = $.getJSON("<?= Yii::getAlias('@web'); ?>/dashboard/app-monthly-report?appId=<?= $model->id ?>", function (data) {

                    chart_<?= $model->id ?>.data.datasets.forEach((dataset) => {

                        $.each(data, function (index, item) {
                            chart_<?= $model->id ?>.data.labels.push([item.label]);
                            dataset.data.push([item.data]);
                        });


                    });
                    chart_<?= $model->id ?>.update();
                }).fail(function () {
                    console.log("error");
                }).always(function () {
                    console.log("complete");
                });



            })(jQuery);


            /*******************
             Chart Chartist
             *******************/
            (function ($) {
                "use strict"


                var jqxhri = $.getJSON("<?= Yii::getAlias('@web'); ?>/dashboard/app-tag-monthly-report?appId=<?= $model->id ?>", function (data) {

                            var lables = [];
                            var series = [];

                            $.each(data, function (index, item) {
                                lables.push(item.label);
                                series.push(item.data);

                            });
                            var <?= $chartPieId ?>_pie = new Chartist.Pie("#<?= $chartPieId ?>", {
                                labels: lables,
                                series: series
                            }, {
                                donut: !0,
                                donutWidth: 10,
                                startAngle: 0,
                                showLabel: true
                            });
                        }).fail(function () {
                            console.log("error");
                        }).always(function () {
                            console.log("complete");
                        });


                        var jqxhri = $.getJSON("<?= Yii::getAlias('@web'); ?>/dashboard/app-stat-info?appId=<?= $model->id ?>", function (data) {

                                    var y = data.y;
                                    var lastMonthSum = data.lastMonthSum;
                                    var lastMonthcount = data.lastMonthcount;
                                    var monthSum = data.monthSum;
                                    var monthcount = data.monthcount;
                                    var monAvg = data.monAvg;
                                    var lowestTags = data.lowestTags;
                                    var highestTags = data.highestTags;
                                    var trend = data.trend;

                                    if (highestTags)
                                        $('#ha_<?= $model->id ?>').html(highestTags.tags + " (" + highestTags.ma + ") ");
                                    else
                                        $('#ha_<?= $model->id ?>').html("No Activity");
                                    if (lowestTags)
                                        $('#la_<?= $model->id ?>').html(lowestTags.tags + " (" + lowestTags.mi + ") ");
                                    else
                                        $('#la_<?= $model->id ?>').html("No Activity");

                                    $('#tot_act_<?= $model->id ?>').html(monthSum);

                                    $('#avg<?= $model->id ?>').html(monAvg);
                                    $('#trend_<?= $model->id ?>').html(trend);
                                    if (trend > 0) {
                                        $('#trend_path_<?= $model->id ?>').attr('class', 'fa fa-caret-up text-success');
                                        $('#trend_<?= $model->id ?>').attr('class', 'text-success');
                                    } else {
                                        $('#trend_path_<?= $model->id ?>').attr('class', 'fa fa-caret-up text-danger');
                                        $('#trend_<?= $model->id ?>').attr('class', 'text-danger');
                                    }


                                }).fail(function () {
                                    console.log("error");
                                }).always(function () {
                                    console.log("complete");
                                });

                            })(jQuery);

                            (function ($) {
                                "use strict";




                                var barchart_<?= $model->id ?> = document.getElementById("barchart_<?= $model->id ?>");
                                barchart_<?= $model->id ?>.height = 150;
                                var mybarchart_<?= $model->id ?> = new Chart(barchart_<?= $model->id ?>, {
                                    type: 'bar',
                                    data: {
                                        labels: [],
                                        datasets: [
                                            {
                                                label: "Actvities",
                                                data: [],
                                                borderColor: "rgba(117, 113, 249, 0.9)",
                                                borderWidth: "0",
                                                backgroundColor: "rgba(117, 113, 249, 0.5)"
                                            }
                                        ]
                                    },
                                    options: {
                                        scales: {
                                            yAxes: [{
                                                    ticks: {
                                                        beginAtZero: true
                                                    }
                                                }]
                                        }
                                    }
                                });

                                var jqxhri = $.getJSON("<?= Yii::getAlias('@web'); ?>/dashboard/app-tag-count?appId=<?= $model->id ?>", function (data) {

                                            var lables = [];
                                            var series = [];

                                            $.each(data, function (index, item) {
                                                lables.push(item.label);
                                                series.push(item.data);
                                                mybarchart_<?= $model->id ?>.data.labels.push(item.label);
                                                
                                                mybarchart_<?= $model->id ?>.data.datasets[0].data.push(item.data);
                                            });
                                            console.log(series);
                                           

                                            mybarchart_<?= $model->id ?>.data.datasets.forEach((dataset) => {
                                                dataset.data.push([104, 9]);
                                            });

                                            mybarchart_<?= $model->id ?>.update();
                                        }).fail(function () {
                                            console.log("error");
                                        }).always(function () {
                                            console.log("complete");
                                        });

                                    })(jQuery);


</script>