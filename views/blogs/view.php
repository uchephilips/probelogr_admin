<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $model app\models\content\ContentBlog */

$this->title = 'Blog';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Start Breadcrumb 
============================================= -->
<div class="breadcrumb-area gradient-bg text-light text-center">
    <!-- Fixed BG -->
    <div class="fixed-bg" style="background-image: url(<?= Yii::getAlias('@web'); ?>/assets/anada_theme/img/shape/1.png);"></div>
    <!-- Fixed BG -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h1><?= $model->content_title ?></h1>
                <ul class="breadcrumb">
                    <li><a href="<?= Yii::getAlias('@web'); ?>"><i class="fas fa-home"></i> Home</a></li>
                    <li><a href="<?= Yii::getAlias('@web'); ?>/blogs">Blog</a></li>
                    <li class="active"><?= $model->content_title ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Breadcrumb -->

<!-- Start Blog
============================================= -->
<div class="blog-area single full-blog full-blog default-padding">
    <div class="container">
        <div class="blog-items">
            <div class="row">
                <div class="blog-content col-lg-10 offset-lg-1 col-md-12">
                    <div class="item">

                        <div class="blog-item-box">
                            <!-- Start Post Thumb -->
                            <div class="thumb">
                                <a href="#">
                                    <img src="<?= $model->header_image_url ?>" alt="Thumb">
                                    <div class="date">
                                        <?php
                                        $date = strtotime($model->created_time);
                                        echo date('dS M', $date);
                                        ?>
                                        <strong><?= date('Y', $date); ?></strong>
                                    </div>
                                </a>
                            </div>
                            <!-- Start Post Thumb -->

                            <div class="info">
                                <div class="meta"> 
                                    <ul>
                                        <li>
                                            <i class="fas fa-tag"></i>
                                        </li>
                                        <?php
                                        $tags = explode(",", $model->tags);
                                        foreach ($tags as $tag) {
                                            ?>
                                            <li>
                                                <a href="#"><?= $tag ?></a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>

                                <h3><?= $model->content_title ?></h3>
                                <p>
                                    <?= $model->blog_content ?>
                                </p>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Blog -->