<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\VideoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/* @var $models[] app\models\content\ContentBlog */

$this->title = 'Blogs';
$this->params['breadcrumbs'][] = $this->title;
?>


<!-- Start Breadcrumb 
============================================= -->
<div class="breadcrumb-area gradient-bg text-light text-center">
    <!-- Fixed BG -->
    <div class="fixed-bg" style="background-image: url(<?= Yii::getAlias('@web'); ?>/assets/anada_theme/img/shape/1.png);"></div>
    <!-- Fixed BG -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h1>Support Blog</h1>
                <ul class="breadcrumb">
                    <li><a href="<?= Yii::getAlias('@web'); ?>"><i class="fas fa-home"></i> Home</a></li>
                    <li class="active">Support Blog</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Breadcrumb -->

<!-- Start Blog
============================================= -->
<div class="blog-area full-blog right-sidebar full-blog default-padding">
    <div class="container">
        <div class="blog-items">
            <div class="row">
                <!-- Start Sidebar 
                <div class="sidebar col-md-12">
                    <aside>
                        <div class="sidebar-item search">
                            <div class="sidebar-info">
                                <form>
                                    <input type="text" name="text" class="form-control">
                                    <button type="submit"><i class="fas fa-search"></i></button>
                                </form>
                            </div>
                        </div>
                    </aside>
                </div>
                 End Start Sidebar -->
            </div>
            <br>
            <br>
            <div class="row">

                <div class="blog-content col-md-12">
                    <div class="blog-item-box">
                        <?php foreach ($models as $contentBlog) { ?>
                            <!-- Single Item -->
                            <div class="single-item">
                                <div class="item">
                                    <div class="thumb">
                                        <a href="#">
                                            <img src="<?= $contentBlog->header_image_url ?>" alt="Thumb">
                                            <div class="date">
                                                <?php
                                                $date = strtotime($contentBlog->created_time);
                                                echo date('dS M', $date);
                                                ?>
                                                <strong><?= date('Y', $date); ?></strong>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="info">
                                        <div class="meta">
                                            <ul>
                                                <li>
                                                    <i class="fas fa-tag"></i>
                                                </li>
                                                <?php
                                                $tags = explode(",", $contentBlog->tags);
                                                foreach ($tags as $tag) {
                                                    ?>
                                                    <li>
                                                        <a href="#"><?= $tag ?></a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                        <h4>
                                            <a href="#"><?= $contentBlog->content_title ?></a>
                                        </h4>
                                        <p>
                                            <?= $contentBlog->blog_description ?>
                                        </p>
                                        <a class="btn-simple" href="<?= Yii::getAlias('@web'); ?>/blogs/view?tagline=<?= $contentBlog->tagline ?>"><i class="fas fa-angle-right"></i> Read More</a>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single Item -->
                        <?php } ?>

                    </div>

                    <!-- Pagination -->
                    <div class="row">
                        <div class="col-md-12 pagi-area text-center">
                            <?php
                            echo \yii\widgets\LinkPager::widget([
                                'pagination' => $pages,
                            ]);
                            ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- End Blog -->

